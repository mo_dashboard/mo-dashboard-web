import { NgModule, ApplicationModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JawabanSurveyComponent } from './jawaban-survey-view/jawaban-survey-view.component';
import { JawabanSurveyAdminComponent } from './jawaban-survey-admin/jawaban-survey-admin.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';// import { IndexComponent } from './index/index.component';
import { routing } from './jawaban-survey.routing';
// import { NgxEchartsModule } from 'ngx-echarts';
// import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// // import { FullCalendarModule } from 'ng-fullcalendar';

// import { ApplicationsModule } from '../applications/applications.module';
// import { ChartsModule } from '../charts/charts.module';
// import { FileManagerModule } from '../file-manager/file-manager.module';
// import { PagesModule } from '../pages/pages.module';
// import { RouterModule } from '@angular/router';
// import { CommonElementsModule } from '../common-elements/common-elements.module';
// import { ListModule } from '../list/list.module';
// import { UiElementsModule } from '../ui-elements/ui-elements.module';
// import { LayoutModule } from '../layout/layout.module';
// import { FormModule } from '../form/form.module';
// import { BlogsModule } from '../blogs/blogs.module';
// import { WidgetsModule } from '../widgets/widgets.module';
// import { MapsModule } from '../maps/maps.module';

import { LazyLoadImageModule } from 'ng-lazyload-image';
// import { VconModule } from '../vcon/vcon.module';
// import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
	imports: [ 
        // LazyLoadImageModule,
		CommonModule,
		routing,
        FormsModule,
        ReactiveFormsModule
		// NgxEchartsModule,
		// LayoutModule,
		// RichTextEditorAllModule,
		// NgbModule,
		// FullCalendarModule,
		// ApplicationsModule,
		// ChartsModule,
		// FileManagerModule,
		// PagesModule,
		// RouterModule,
		// CommonElementsModule,
		// ListModule,
		// UiElementsModule,
		// FormModule,
		// BlogsModule,
        // WidgetsModule,
		// MapsModule,
		// NgxSpinnerModule,
		// VconModule
	],
	declarations: [
		JawabanSurveyComponent,
		JawabanSurveyAdminComponent
		// urlParams
	],
	// schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class JawabanSurveyModule { }