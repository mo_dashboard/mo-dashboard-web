import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JawabanSurveyAdminComponent } from './jawaban-survey-admin.component';

describe('JawabanSurveyAdminComponent', () => {
  let component: JawabanSurveyAdminComponent;
  let fixture: ComponentFixture<JawabanSurveyAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JawabanSurveyAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JawabanSurveyAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});