import { Component, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SidebarService } from '../../services/sidebar.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, UrlSerializer, DefaultUrlSerializer } from '@angular/router';
import { API_BASE_URL } from '../../constants';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
	selector: 'app-jawaban-survey-admin',
	templateUrl: './jawaban-survey-admin.component.html',
	styleUrls: ['./jawaban-survey-admin.component.css']
})
export class JawabanSurveyAdminComponent implements OnInit {

	public dataSurvey: any = [];
	public isValidFormSubmitted: boolean = null;
	public useForm: FormGroup;
	public groupPertanyaan: any = [];
	public jawaban: any = [];
	public saran: string = '';
	public timerInterval: any;
	public id: any;
	public id2: any;
	public dataUser: any = [];
	public nama_user: string;
	public nama_jabatan: string;
	public survey_length: any = [];
	public id_pertanyaan: string;
	public jawaban_event: string;
	public checked: boolean;

	constructor(
		private http: HttpClient,
		private toastr: ToastrService,
		private router: Router,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private serializer: UrlSerializer,
		private location: Location
	) {
		;

		this.useForm = new FormGroup({
			survey: new FormControl()
		});

		const queryParams = { foo: 'a', bar: 42 };
		const tree = router.createUrlTree([], { queryParams });
	}

	ngOnInit() {
		this.id = window.location.href.split('/')[6]
		this.id2 = window.location.href.split('/')[7]
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		this.http.get(API_BASE_URL + 'user-module/getUserById/' + this.id2, { headers }).toPromise().then((res: any) => {
			this.dataSurvey = res.result;
			res.result.map((item: any) => {
				this.nama_user = item.nama_user,
					this.nama_jabatan = item.nama_jabatan
			})
		});

		this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + this.id, { headers }).toPromise().then((res: any) => {
			this.dataSurvey = res.result;
			if (res.result !== 'no data found') {
				this.dataSurvey = res.result;
				var groups = new Set(this.dataSurvey.map(item => item.nama_group_pertanyaan))
				groups.forEach(survey =>
					this.groupPertanyaan.push({
						pertanyaan: survey,
						values: this.dataSurvey.filter(i => i.nama_group_pertanyaan === survey)
					}
					))
				this.groupPertanyaan.map((item: any) => {
					item.values.map((data: any) => {
						this.survey_length.push({
							surveyLength: data.pertanyaan
						})
					})
				})

			}

		});
	}

	groupBy(list: any, keyGetter: any) {
		const map = new Map();
		list.forEach((item) => {
			const key = keyGetter(item);
			const collection = map.get(key);
			if (!collection) {
				map.set(key, [item]);
			} else {
				collection.push(item);
			}
		});
		return map;
	}

	onItemChange(event) {
		this.id_pertanyaan = event.target.id
		this.jawaban_event = event.target.value
		this.checked = event.target.checked

		this.jawaban.push({
			id_pertanyaan: event.target.id,
			jawaban: event.target.value,
			checked: event.target.checked
		})
	}

	login() {
		this.router.navigate(['/authentication/page-login']);
	}

	SubmitSurvey() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		const body = {
			id_master_survey: this.id,
			id_user: this.id2,
			saran: this.saran,
			jawaban_survey: this.jawaban
		}
		this.http.post(API_BASE_URL + 'survey-module/jawabSurvey', body, { headers }).toPromise().then((res: any) => {
			if (res.response_code === "200") {

				Swal.fire({
					width: '25rem',
					text: "Anda Berhasil Menjawab Survey!",
					showCancelButton: false,
				})
			}

		});

	}

}