import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JawabanSurveyComponent } from './jawaban-survey-view.component';

describe('JawabanSurveyComponent', () => {
  let component: JawabanSurveyComponent;
  let fixture: ComponentFixture<JawabanSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JawabanSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JawabanSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});