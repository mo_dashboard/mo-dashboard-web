import { Routes, RouterModule, Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { JawabanSurveyComponent } from './jawaban-survey-view/jawaban-survey-view.component';
import { JawabanSurveyAdminComponent } from './jawaban-survey-admin/jawaban-survey-admin.component';

const routes: Routes = [   
    {
        path: 'survey',
        children: [
            { path: 'kuisioner/:id/:id2', component: JawabanSurveyComponent, data: { 
                
               title : 'e-kabinet' 
            }},
            { path: 'kuisioner/:id', component: JawabanSurveyAdminComponent, data: { 
                
               title : 'e-kabinet' 
            }},


        ]
    },
];

export const routing = RouterModule.forChild(routes);