import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTambahRespondenRisalahComponent } from './list-tambah-responden-risalah.component';

describe(' ListTambahRespondenRisalahComponent ', () => {
  let component:  ListTambahRespondenRisalahComponent ;
  let fixture: ComponentFixture< ListTambahRespondenRisalahComponent >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  ListTambahRespondenRisalahComponent  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( ListTambahRespondenRisalahComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
