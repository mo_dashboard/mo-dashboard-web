import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUbahRisalahComponent } from './list-ubah-risalah.component';

describe('ListUbahRisalahComponent', () => {
  let component: ListUbahRisalahComponent;
  let fixture: ComponentFixture<ListUbahRisalahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUbahRisalahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUbahRisalahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
