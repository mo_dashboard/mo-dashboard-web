import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPenggunaComponent } from './list-pengguna.component';

describe('ListPenggunaComponent', () => {
  let component: ListPenggunaComponent;
  let fixture: ComponentFixture<ListPenggunaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPenggunaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
