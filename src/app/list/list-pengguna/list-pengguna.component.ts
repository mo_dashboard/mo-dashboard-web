import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASE_URL } from '../../constants';

var moment = require('moment')
@Component({
  selector: 'app-list-pengguna',
  templateUrl: './list-pengguna.component.html',
  styleUrls: ['./list-pengguna.component.css']
})

export class ListPenggunaComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUser : any = [];
  public lengthDataUser: number = 0;
  public pembagi:number = 5;
  public jumlahPage : number = 0;
  public sliceDataUser: any = [];
  public awalan: boolean = true;
  public isLoading : boolean = true;
  public isSearching : boolean = false;
  public p:number=1;

  constructor(private sidebarService: SidebarService, private cdr: ChangeDetectorRef,private http:HttpClient) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }

  ngOnInit() {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('Access-Control-Allow-Headers', 'Content-Type')
    .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.http.get(API_BASE_URL+'user-module/getAllUser',{headers}).toPromise().then((res:any)=>{
      var userData = res.result.map((item: any, index: number) => {
        return {
          ...item,
          format_tanggal: moment(item.create_date).format('DD-MM-YYYY HH:mm')
        }
      })
      this.dataUser = userData.sort((a, b) => {
        return Number(a.id_user) > Number(b.id_user)
          ? 1
          : Number(a.id_user) < Number(b.id_user) ? -1 : 0;
      });
      this.lengthDataUser = this.dataUser.length;
      this.jumlahPage = Math.ceil(this.lengthDataUser / this.pembagi);
      this.sliceDataUser = this.dataUser.slice(0, this.pembagi);
      this.isLoading = false;
    })
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  gantiPage(n: number){
    this.sliceDataUser = this.dataUser.slice (this.pembagi*n, this.pembagi*(n+1))
    this.awalan = false
  }

  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  onSearchUser(event) {
		this.p = 1;
	}
  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}