import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { data } from 'jquery';
import { Router } from '@angular/router';
import { API_BASE_URL } from '../../constants'

@Component({
  selector: 'app-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.css']
})
export class ListRoleComponent implements OnInit {
  [x: string]: any;

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataRole: any = [];
  public lengthDataRole: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceRole: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;

  public id_role: string;
  public nama_role: string = '';
  public roleById: Array<any>;
  public user_create: string = localStorage.getItem('id_user');
  public user_update: string = localStorage.getItem('id_user');
  public p: number = 1;

  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }

  ngOnInit() {
    const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Access-Control-Allow-Headers', 'Content-Type')

    this.http.get(API_BASE_URL + 'master-module/getAllRole', { headers }).toPromise().then((res: any) => {
      this.dataRole = res.result
      this.lengthDataRole = this.dataRole.length
      this.jumlahPage = Math.ceil(this.lengthDataRole / this.pembagi)
      this.sliceRole = this.dataRole.slice(0, this.pembagi)
      this.isLoading = false;
    })
  }

  open(id: number, content) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Access-Control-Allow-Headers', 'Content-Type')
    this.id_role = id.toString()
    this.modalService.open(content, { size: 'lg' })
    this.http.get(API_BASE_URL + 'master-module/getRoleById/' + id, { headers }).toPromise().then((res: any) => {
      this.roleById = res.result
      this.roleById.map(res => this.nama_role = res.nama_role)
    })
  }

  tambahModal(content) {
    this.modalService.open(content, { size: 'lg' })
  }

  tambahRole(content) {
    this.modalService.open(content, { size: 'lg' })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type');
    const body = {
      nama_role: this.nama_role,
      user_create: this.user_create
    }
    this.http
      .post<any>(API_BASE_URL + 'master-module/createRole', body, { headers })
      .subscribe(
        (data: any) => {
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Role');
            this.nama_role = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-role']);
            this.ngOnInit()
          } else {
            this.nama_role = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Role');
          }
        }
      )
  }

  tutupModal() {
    this.nama_role = "";
    this.modalService.dismissAll();
  }

  ubahRole() {
    if (this.nama_role !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type');
      const body = {
        id_role: this.id_role,
        nama_role: this.nama_role,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateRole/', body, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Role');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-role']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Role');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Role ');
    }
  }

  deleteRole(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteRole/' + id).subscribe(
      (data) => {
        this.showToastr('Berhasil Menghapus Peran');
        this.ngOnInit()
      })
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  updateQuery(e: any) {
    if (e !== "") {
      this.isSearching = true;
    } else {
      this.isSearching = false;
    }
  }

  gantiPage(n: number) {
    this.sliceRole = this.dataRole.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  onFileSelected(event) {
    this.nama_role = event.target.files;
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }
}