import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { data } from 'jquery';
import { Router } from '@angular/router';
import { API_BASE_URL } from '../../constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
@Component({
	selector: 'app-list-survey',
	templateUrl: './list-survey.component.html',
	styleUrls: ['./list-survey.component.css']
})
export class ListSurveyComponent implements OnInit {

	[x: string]: any;

	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dataMasterSurvey: any = [];
	public lengthDataMasterSurvey: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceMasterSurvey: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isSearching: boolean = false;
	public closeResult: string;

	public id_master_survey: string;
	public nama_master_survey: string = '';
	public masterSurveyById: Array<any>;
	public masterSurveyById2: any = [];
	public user_update: string = localStorage.getItem('id_user');
	public p: number = 1;
	public id: number;
	public content: any;
	public surveyForm: FormGroup;
	public surveyMap: any = [];
	public routerLinkSurvey: SafeUrl;
	public pertanyaan: string = '';
	public nama_pertanyaan: string = '';
	public id_pertanyaan: string;
	public id_group_pertanyaan: string = '1';
	public nama_group_pertanyaan: string;
	public groupPertanyaan: any = [];

	constructor(
		private router: Router,
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService,
		private fb: FormBuilder,
		private domSanitizer: DomSanitizer
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
		this.dropdownList = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.selectedItems = [];
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'buatSelect',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari'
		};

		this.surveyForm = this.fb.group({
			pertanyaan: this.fb.array([]),
		});
	}

	pertanyaanSurvey(): FormArray {
		return this.surveyForm.get("pertanyaan") as FormArray
	}

	pertanyaanBaru(): FormGroup {
		return this.fb.group({
			nama_pertanyaan: '',
			id_group_pertanyaan: '1',
		})
	}

	hapusPertanyaan(i: number) {
		this.pertanyaanSurvey().removeAt(i);
	}

	tambahRow() {
		this.pertanyaanSurvey().push(this.pertanyaanBaru());
	}

	onSubmit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

		var sesuatu = true;

		this.surveyMap = this.surveyForm.value.pertanyaan.map((data: any) => {
			return {
				id_group_pertanyaan: data.id_group_pertanyaan,
				nama_pertanyaan: data.nama_pertanyaan
			}
		})

		this.surveyMap.map((item: any, index) => {
			if (item.id_group_pertanyaan === '' || item.nama_pertanyaan === '') {
				sesuatu = false
			}
		})

		if (sesuatu === true) {
			this.user_create = localStorage.getItem('id_user')

			const body = {
				id_master_survey: this.id_master_survey,
				user_create: this.user_create,
				pertanyaan: this.surveyMap
			}
			this.http
				.post<any>(API_BASE_URL +
					'survey-module/createSurvey', body, { headers })
				.subscribe(
					(data: any) => {
						if (data.message === "insert berhasil") {
							this.showToastrBerhasil('Berhasil Menambah Pertanyaan');
							this.pertanyaan = "";
							this.modalService.dismissAll()
						}
					}, error => {
						this.showToastr('Data Tidak Lengkap');
					}
				);

		} else {
			this.showToastr('Data Tidak Lengkap');
		}

	}

	directLink(item) {
		this.user_create = localStorage.getItem('id_user')
		this.routeLinkSurvey = '/#/survey/kuisioner/' + item.id_master_survey
	}

	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		this.http.get(API_BASE_URL + 'survey-module/getAllMastersurvey', { headers }).toPromise().then((res: any) => {
			this.dataMasterSurvey = res.result
			this.lengthDataMasterSurvey = this.dataMasterSurvey.length
			this.jumlahPage = Math.ceil(this.lengthDataMasterSurvey / this.pembagi)
			this.sliceMasterSurvey = this.dataMasterSurvey.slice(0, this.pembagi)
			this.isLoading = false;
		});
		this.http.get(API_BASE_URL +
			'survey-module/getAllGroupPertanyaan', { headers }).toPromise().then((res: any) => {
				this.groupPertanyaan = res.result;
			})
	}

	updatePertanyaan() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + this.id_master_survey, { headers }).toPromise().then((res: any) => {
			this.masterSurveyById2 = res.result;
			if (res.result !== "no data found") {
				this.masterSurveyById2 = res.result;
			}
		})
	}

	open(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		this.id_master_survey = id.toString()
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
		this.http.get(API_BASE_URL + 'survey-module/getMastersurveyById/' + id, { headers }).toPromise().then((res: any) => {
			this.masterSurveyById = res.result
			this.masterSurveyById.map(res => this.nama_master_survey = res.nama_master_survey)
		})
		this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + id, { headers }).toPromise().then((res: any) => {
			this.masterSurveyById2 = res.result;

			if (res.result !== "no data found") {
				this.masterSurveyById2 = res.result;

			}
		})

		this.http.get(API_BASE_URL + 'survey-module/getRespondenBelumTerimaSurvey/' + id, { headers }).toPromise().then((res: any) => {
			if (res.result !== "no data found") {
				this.dataUser = res.result;
				this.idUser = res.result[0].id_user;
				this.dataUser = this.dataUser.map((item: any, index: number) => {
					return {
						...item,
						buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
					}
				})
			}
		})
	}

	editPertanyaan(item: any, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
		this.content = content

		this.pertanyaan = item.pertanyaan
		this.nama_group_pertanyaan = item.nama_group_pertanyaan
		this.id_group_pertanyaan = item.id_group_pertanyaan
		this.id_master_survey = item.id_master_survey
		this.id_pertanyaan = item.id_pertanyaan
		this.http.get(API_BASE_URL +
			'survey-module/getAllGroupPertanyaan', { headers }).toPromise().then((res: any) => {
				this.groupPertanyaan = res.result;
			})
	}

	ubahPertanyaan(modal) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

		const body = {
			id_pertanyaan: this.id_pertanyaan,
			id_master_survey: this.id_master_survey,
			id_group_pertanyaan: this.id_group_pertanyaan,
			pertanyaan: this.pertanyaan,
			user_update: localStorage.getItem('id_user')
		}
		this.http.post(API_BASE_URL +
			'survey-module/updateSurvey', body, { headers }).toPromise().then((res: any) => {
				if (res.response_code === '200') {
					this.showToastrBerhasil('Pertanyaan Berhasil Diperbarui');
					this.updatePertanyaan()
				}
			})
	}

	alertConfirmation(id: string) {
		Swal.fire({
			text: ("Apakah Anda Yakin Ingin Menghapus Survey?"),
			type: "warning",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: "OK",
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.isConfirmed) {
				this.deleteMasterSurvey(id)
				this.showToastrBerhasil('Berhasil Menghapus Survey');
			}
		})
	}

	tambahModal(content) {
		let ngbModalOptions: NgbModalOptions = {
			backdrop: 'static',
			keyboard: false,
			size: "lg"
		};
		this.nama_master_survey = "";
		this.modalService.open(content, ngbModalOptions)
	}

	tambahMasterSurvey(content) {

		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

		this.user_create = localStorage.getItem('id_user')

		const body = {
			nama_master_survey: this.nama_master_survey,
			user_create: this.user_create
		}


		this.http
			.post<any>(API_BASE_URL + 'survey-module/createMastersurvey', body, { headers })
			.subscribe(
				(data: any) => {
					if (data.message === "insert berhasil") {
						this.showToastrBerhasil('Berhasil Menambah Survey');
						this.nama_master_survey = "";
						this.modalService.dismissAll()
						this.ngOnInit();
					} else {
						this.nama_master_survey = "";
						this.showToastr('Terjadi Kesalahan Saat Menambah Survey');
					}
				}
			)
	}

	tutupModal() {
		this.nama_master_survey = "";
		this.modalService.dismissAll();
	}

	ubahMasterSurvey() {
		if (this.nama_master_survey !== '') {
			const headers = new HttpHeaders()
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('Access-Control-Allow-Headers', 'Content-Type')
				.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
			const body = {
				id_master_survey: this.id_master_survey,
				nama_master_survey: this.nama_master_survey,
				user_update: this.user_update
			}

			this.http
				.post<any>(API_BASE_URL + 'survey-module/updateMastersurvey', body, { headers })
				.subscribe(
					(data: any) => {
						if (data.message === "data berhasil diupdate") {
							this.showToastrBerhasil('Berhasil Mengubah Survey');
							this.modalService.dismissAll()
							this.router.navigate(['/admin/list/list-survey']);
							this.ngOnInit()
						} else {
							this.showToastr('Terjadi Kesalahan Saat Mengubah Survey');
						}
					}
				)
		} else {
			this.showToastr('Terjadi Kesalahan Saat Mengubah Survey ');
		}
	}

	deleteMasterSurvey(id: string) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		this.http.get<any>(API_BASE_URL + 'survey-module/deleteMastersurvey/' + id, { headers }).subscribe(
			(data) => {
				this.ngOnInit()
			})
	}

	tambahSurvey() {
		if (
			this.pertanyaan !== ''
		) {
			const headers = new HttpHeaders()
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('Access-Control-Allow-Headers', 'Content-Type')
				.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

			this.user_create = localStorage.getItem('id_user')

			const body = {
				id_master_survey: this.id_master_survey,
				id_group_pertanyaan: this.id_group_pertanyaan,
				pertanyaan: this.pertanyaan,
				user_create: this.user_create
			}

			this.http
				.post<any>(API_BASE_URL +
					'survey-module/createSurvey', body, { headers })
				.subscribe(
					(data: any) => {
						if (data.message === "insert berhasil") {
							this.showToastrBerhasil('Berhasil Menambah Pertanyaan');
							this.pertanyaan = "";
							this.modalService.dismissAll()
						}
					}
				);
		}
	}
	arrayOne(n: number): any[] {
		return Array(n);
	}

	onSearchMasterSurvey(event) {
		this.p = 1;
	}
	gantiPage(n: number) {
		this.sliceMasterSurvey = this.dataMasterSurvey.slice(this.pembagi * n, this.pembagi * (n + 1))
		this.awalan = false
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}
	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach(element => {
			xAxisData.push("");
		});

		return chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false,
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [{
				data: data,
				type: 'line',
				showSymbol: false,
				symbolSize: 1,
				lineStyle: {
					color: color,
					width: 1
				}
			}]
		};
	}
}
