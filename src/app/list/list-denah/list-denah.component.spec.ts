import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDenahComponent } from './list-denah.component';

describe('ListDenahComponent', () => {
  let component: ListDenahComponent;
  let fixture: ComponentFixture<ListDenahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDenahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDenahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
