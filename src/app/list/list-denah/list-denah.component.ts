
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { API_BASE_URL } from '../../constants';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
	selector: 'app-list-denah',
	templateUrl: './list-denah.component.html',
	styleUrls: ['./list-denah.component.css']
})
export class ListDenahComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dataUndangan: any = [];
	public lengthDataUndangan: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceDataUndangan: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isSearching: boolean = false;
	public closeResult: string;
	public undanganById: number;
	public notifUndanganById: any;
	public denahUndangan: any = [];
	public path_denahUndangan: string = '';
	public id_undangan: string;
	public p: number = 1;
	public isLoadingDenah: boolean = false;

	constructor(
		private router: Router,
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
	}

	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		this.http.get(API_BASE_URL + 'undangan-module/getAllUndangan', { headers }).toPromise().then((res: any) => {

			this.dataUndangan = res.result.sort((a, b) => {
				return Number(a.id_undangan) < Number(b.id_undangan)
					? 1
					: Number(a.id_undangan) > Number(b.id_undangan) ? -1 : 0;
			});
			if (this.dataUndangan[0].denah === null) {
				let element = document.getElementsByClassName('denahKosong');

			}
			this.dataUndangan = res.result;
			this.lengthDataUndangan = this.dataUndangan.length;
			this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi);

			this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi);
			this.isLoading = false;
		});
	}

	arrayOne(n: number): any[] {
		return Array(n);
	}

	onSearchDenahUndangan(event) {

		this.p = 1;
	}



	gantiPage(n: number) {
		this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1));
		this.awalan = false;
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	open(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false });
		this.http
			.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.undanganById = res.result;
			});
		this.id_undangan = id.toString();
		this.http
			.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.notifUndanganById = res.result;
			});
	}
	unggahDenah() {
		const headers = new HttpHeaders()
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		const user_create = localStorage.getItem('id_user')
		var formdata = new FormData();
		formdata.append('denah', this.denahUndangan[0], '/' + this.path_denahUndangan.replace(/\\/g, '/'));
		formdata.append('id_undangan', this.id_undangan);
		formdata.append('user_create', user_create);
		var body_upload = formdata;
		this.spinner.show();
		this.http.post<any>(API_BASE_URL + 'undangan-module/uploadDenah/', body_upload, { headers }).subscribe(
			(data) => {

				if (data.message === 'upload denah berhasil' || data.message === 'data berhasil diupdate') {
					setTimeout(() => {
						this.showToastrBerhasil('Berhasil Menambah Denah');
						this.modalService.dismissAll();
						this.ngOnInit();
						this.router.navigate(['/admin/list/list-denah']);
						this.spinner.hide();
					}, 5000);
				} else if (data.result === "ekstensi file salah") {
					this.showToastr('Ekstensi File Salah');
					this.spinner.hide();
					this.modalService.dismissAll();
				} else {
					this.showToastr(data.result);
					this.spinner.hide();
				}
			},
			(error) => {
				setTimeout(() => {
					this.showToastr('Terjadi Kesalahan Saat Menambah Denah')
					this.spinner.hide();
				}, 5000);
			}
		);

	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	onFileSelected(event) {

		this.denahUndangan = event.target.files;
		if (this.denahUndangan[0]) {
			let file: File = this.denahUndangan[0];
			var idxDot = file.name.lastIndexOf('.') + 1;
			var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
			var sizeFile = file.size;
			var validationExt = this.checkExt(extFile);

			if (validationExt == false) {
				var validationSize = this.checkSize(sizeFile);
			}
		}

	}

	checkExt(extFile) {
		if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'png') {
			this.isLoadingDenah = false;
			return false;
		} else {
			this.isLoadingDenah = true;
			this.showToastr('Unggah Gambar Dengan Ekstensi jpg,jpeg atau png');
			return true;
		}
	}
	checkSize(sizeFile) {
		if (sizeFile <= 5242880) {
			this.isLoadingDenah = false;
			return false;
		} else {
			this.isLoadingDenah = true;
			this.showToastr('Gambar Yang Anda Unggah Lebih Dari 5MB');
			return true;
		}
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}

