import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_BASE_URL, GEN } from '../../constants';
import { NgxSpinnerService } from 'ngx-spinner';
// tambahan
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import * as moment from 'moment'
import * as CryptoJS from 'crypto-js';
// end

@Component({
	selector: 'app-list-undangan',
	templateUrl: './list-undangan.component.html',
	styleUrls: ['./list-undangan.component.css']
})
export class ListUndanganComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dataUndangan: any = [];
	public lengthDataUndangan: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceDataUndangan: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isLoadingPdf: boolean = true;
	public isSearching: boolean = false;
	public closeResult: string;
	public undanganById: number;
	public notifUndanganById: any;
	public searchTerm: string;
	public p: number = 1;
	public undanganById2: any;
	public link: string;
	public pdfSrc: SafeUrl;
	public id_undangan: string;
	// tambahan vcon
	public idRole: string = localStorage.getItem('id_role');
	public btnTxtVcon: string;
	public returnURL: string;

	// end vcon

	constructor(
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private searchService: SearchService,
		private domSanitizer: DomSanitizer,
		private spinner: NgxSpinnerService,
		// tambahan vcon
		private router: Router,
		// end vcon
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
	}
	ngOnInit() {
		if (this.idRole == '1' || this.idRole == '2') {
			this.btnTxtVcon = 'Buat Room Sidang'
		} else {
			this.btnTxtVcon = 'Masuk Room Sidang'
		}

		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http
			.get(API_BASE_URL + 'undangan-module/getAllUndangan', { headers })
			.toPromise()
			.then((res: any) => {
				this.dataUndangan = res.result.sort((a, b) => {
					return Number(a.id_undangan) < Number(b.id_undangan)
						? 1
						: Number(a.id_undangan) > Number(b.id_undangan) ? -1 : 0;
				});
				this.lengthDataUndangan = this.dataUndangan.length;
				this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi);
				this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi);
				this.isLoading = false;
			});
	}


	onSearchUndangan(event) {
		this.p = 1;
	}

	arrayOne(n: number): any[] {
		return Array(n);
	}
	gantiPage(n: number) {
		this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1));
		this.awalan = false;
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	open(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false });

		this.http
			.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.undanganById = res.result;
				this.id_undangan = id.toString();
			});

		this.http
			.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.notifUndanganById = res.result;
			});
	}
	openFile(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false });
		this.http
			.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.undanganById2 = res.result;
				var sesuatu = this;
				var link = res.result[0].link_undangan;
				sesuatu.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
					API_BASE_URL + 'pdf_tmp/' + res.result[0].link_undangan + "#toolbar=0&navpanes=0&scrollbar=0"
				);
				sesuatu.isLoadingPdf = false;
			});
	}
	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}

	// tambahan vcon
	async createRoom(id) {
		// this.spinner.show();
		let key = CryptoJS.enc.Utf8.parse(GEN);
		let iv = CryptoJS.enc.Utf8.parse(GEN);
		let returnString;
		returnString = await CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(id), key, {
			keySize: 256,
			iv: iv,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		})
		this.returnURL = returnString.toString()
		this.router.navigate(['/vcon/meeting', { return: this.returnURL }])
		// this.getURL = await this.decrypt(returnString)
		// console.log("decr: " + this.getURL)
	}
	//end Tambahan vcon

}