import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUndanganComponent } from './list-undangan.component';

describe('ListUndanganComponent', () => {
  let component: ListUndanganComponent;
  let fixture: ComponentFixture<ListUndanganComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUndanganComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUndanganComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
