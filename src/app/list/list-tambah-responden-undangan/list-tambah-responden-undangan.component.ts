import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { API_BASE_URL } from '../../constants'


@Component({
  selector: 'app-list-tambah-responden-undangan',
  templateUrl: './list-tambah-responden-undangan.component.html',
  styleUrls: ['./list-tambah-responden-undangan.component.css']
})
export class ListTambahRespondenUndanganComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public undanganById: any = [];
  public notifUndanganById: any = [];
  public selectedItems: Array<any>;
  public dropdownList: Array<any>;
  public dropdownSettings: any;
  public selectNama: string;
  public user_create: string = '1';
  public dataUser: any = [];
  public idUser: string = '';
  public undanganId: string;
  public isTampil: boolean = false;
  public p: number = 1;



  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    this.dropdownList = [
      { item_id: 1, item_text: 'Cheese' },
      { item_id: 2, item_text: 'Tomatoes' },
      { item_id: 3, item_text: 'Mozzarella' },
      { item_id: 4, item_text: 'Mushrooms' },
      { item_id: 5, item_text: 'Pepperoni' },
      { item_id: 6, item_text: 'Onions' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id_user',
      textField: 'buatSelect',
      selectAllText: 'Pilih Semua',
      unSelectAllText: 'Tidak ada yang Dipilih',
      allowSearchFilter: true,
      searchPlaceholderText: 'Cari'
    };
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    this.http.get(API_BASE_URL + 'undangan-module/getAllUndangan', { headers }).toPromise().then((res: any) => {
      this.dataUndangan = res.result.sort((a, b) => {
        return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
      })
      this.dataUndangan = res.result
      this.lengthDataUndangan = this.dataUndangan.length
      this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
      this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
      this.isLoading = false;

    });
  }


  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchRespondenUndangan(event) {
    this.p = 1;
  }
  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.undanganById = res.result;
        this.undanganId = res.result[0].id_undangan;
      }
    })
    this.http.get(API_BASE_URL + 'undangan-module/getRespondenBelumTerimaUndangan/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.dataUser = res.result;
        this.idUser = res.result[0].id_user;

        this.dataUser = this.dataUser.map((item: any, index: number) => {

          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }
        })
      }
    })

    this.http.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.notifUndanganById = res.result || []
      }

    })
  }


  tambahResponden() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    var body_responden = this.selectedItems.map((item: any, index: number) => {

      return {

        ...item,
        id_undangan: this.undanganId,
        id_user: item.id_user,
        user_create: this.user_create
      }
    })
    this.http.post<any>(API_BASE_URL + 'undangan-module/tambahRespondenUndangan', body_responden, { headers }).subscribe(data => {
      if (data.message === 'tambah responden berhasil') {
        this.showToastrBerhasil('Berhasil Menambahkan Responden');
        this.modalService.dismissAll()
        this.router.navigate(['/admin/list/list-undangan']);
        this.selectedItems = []
      } else {
        this.showToastr('Terjadi Kesalahan Saat Menambahkan Responden');
        this.selectedItems = []
      }

    })
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}