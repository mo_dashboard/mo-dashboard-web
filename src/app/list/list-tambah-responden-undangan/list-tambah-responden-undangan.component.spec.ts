import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTambahRespondenUndanganComponent } from './list-tambah-responden-undangan.component';

describe(' ListTambahRespondenUndanganComponent ', () => {
  let component:  ListTambahRespondenUndanganComponent ;
  let fixture: ComponentFixture< ListTambahRespondenUndanganComponent >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  ListTambahRespondenUndanganComponent  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( ListTambahRespondenUndanganComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
