import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListJenisSidangComponent } from './list-jenis-sidang.component';

describe('ListJenisSidangComponent', () => {
  let component: ListJenisSidangComponent;
  let fixture: ComponentFixture<ListJenisSidangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListJenisSidangComponent
   ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListJenisSidangComponent
  );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
