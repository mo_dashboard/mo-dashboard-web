
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { API_BASE_URL } from '../../constants'

var moment = require('moment')
@Component({
  selector: 'app-list-unggah-materi',
  templateUrl: './list-unggah-materi.component.html',
  styleUrls: ['./list-unggah-materi.component.css']
})
export class ListUnggahMateriComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingMateri: boolean = false;
  public isSearching: boolean = false;
  public closeResult: string;
  public undanganById: number;
  public notifUndanganById: any;
  public materiUndangan: any = [];
  public pathMateriUndangan: any = [];
  public user_create: string;
  public id_undangan: string;
  public p: number = 1;
  public lengthFileAdd: number;
  public lengthFileRemove: number;
  public lengthFile: any = [];
  public materiUndanganById: any = [];
  public id_sidang: any = [];
  public materiUser: any = [];
  public materiName: any = [];
  public userName: any = [];
  public userCreate: string;
  public userCreate2: string;
  public idUser: string;
  public idUser2: string;
  public userUpload: any = [];
  public userUpload2: any;
  public sifat_materi: string = 'umum';
  public dataUserKhusus: any = [];
  public dataUserRahasia: any = [];
  public dataUserUmum: any = [];
  public selectedItems: any = [];
  public dropdownSettings: any;
  public penerimaMateri: any = [];
  public waktu_sekarang: any;
  public buttonCondition: boolean = true;
  public materiRahasia: boolean = true;
  public disableButton: boolean = true;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private cdr: ChangeDetectorRef,
    private http: HttpClient,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {

    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");

    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'penerima',
      textField: 'buatSelect',
      selectAllText: 'Pilih Semua',
      unSelectAllText: 'Tidak ada yang Dipilih',
      allowSearchFilter: true,
      searchPlaceholderText: 'Cari'
    };
  }


  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    this.waktu_sekarang = moment(new Date()).format('YYYY-MM-DD hh:mm:ss')

    const id = localStorage.getItem('id_user')
    const nama_role = localStorage.getItem('nama_role')
    if (nama_role === 'Super Admin' || nama_role === 'Admin Materi') {
      this.http.get(API_BASE_URL + 'undangan-module/getAllUndanganByMateri', { headers }).toPromise().then((res: any) => {
        this.dataUndangan = res.result.sort((a, b) => {
          return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
        })
        this.lengthDataUndangan = this.dataUndangan.length
        this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
        this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
        this.isLoading = false;
        if (this.dataUndangan[0].materi === null) {
          let element = document.getElementsByClassName('materiKosong');
        }
      })
    } else {
      this.http.get(API_BASE_URL + 'materi-module/getUndanganMateriById/' + id, { headers }).toPromise().then((res: any) => {
        this.dataUndangan = res.result
        this.lengthDataUndangan = this.dataUndangan.length
        this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
        this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
        this.isLoading = false;
      })
    }
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchUnggahMateri(event) {
    this.p = 1;
  }
  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.files = []
    this.materiUser = []

    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers }).toPromise().then((res: any) => {
      this.undanganById = res.result
    })

    this.id_undangan = id.toString()
    this.http.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.idUser = res.result[0].penerima
        this.dataUserUmum = res.result
        this.dataUserKhusus = res.result.map((item: any, index: number) => {
          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }

        })
        this.dataUserRahasia = res.result.map((item: any, index: number) => {

          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }
        })
      }
    })

    this.http.get(API_BASE_URL + 'materi-module/getMateriByIdUndangan/' + id, { headers }).toPromise().then((res: any) => {
      this.materiUndanganById = res.result;
      for (let i = 0; i < this.materiUndanganById.length; i++) {
        this.idUser = localStorage.getItem('id_user')
        this.userCreate = this.materiUndanganById[i].user_create
        if (res.result !== "no data found") {
          if (this.userCreate === this.idUser) {
            this.idUser2 = localStorage.getItem('id_user')
            this.userCreate2 = this.materiUndanganById[i].user_create
            this.materiUser.push(this.materiUndanganById[i])
          }
        }
      }
    })

  }

  // upload materi

  files: File[] = [];

  onSelect(event: any) {
    if (event.rejectedFiles.length > 0) {
      this.isLoadingMateri = true
      this.disableButton = true
      this.showToastr('Unggah Materi Dengan Ekstensi Berikut png,jpeg,jpg,pdf dan mp4');
    } else {
      this.disableButton = false
      this.isLoadingMateri = false
      this.lengthFileAdd = this.files.push(...event.addedFiles);
    }
  }

  onRemove(event) {
    this.lengthFileRemove = this.files.splice(this.files.indexOf(event), 1).length;
    this.disableButton = true

  }

  unggahMateri() {
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.user_create = localStorage.getItem("id_user")

    if (this.sifat_materi === 'khusus') {
      this.penerimaMateri = this.selectedItems.map((item: any, index: number) => {
        return {
          id_user: item.penerima
        }
      })
    } else if (this.sifat_materi === 'rahasia') {
      this.penerimaMateri = this.selectedItems.map((item: any, index: number) => {
        return {
          id_user: item.penerima
        }
      })
    } else if (this.sifat_materi === 'umum') {
      this.penerimaMateri = this.dataUserUmum.map((item: any, index: number) => {
        return {
          id_user: item.penerima
        }
      })
    }
    var formdata = new FormData();
    for (let i = 0; i < this.files.length; i++) {
      formdata.append(
        'materi[]',
        this.files[i]
      );
    }
    formdata.append('user_create', this.user_create);
    formdata.append('id_undangan', this.id_undangan);
    formdata.append('sifat_materi', this.sifat_materi);
    formdata.append('penerima', JSON.stringify(this.penerimaMateri));
    var body_upload = formdata;
    this.spinner.show();
    this.http.post<any>(API_BASE_URL + 'materi-module/createMateri', body_upload, { headers })
      .subscribe(
        (data) => {
          if (data.message === 'insert berhasil') {
            setTimeout(() => {
              this.showToastrBerhasil('Berhasil Menambahkan Materi');
              this.modalService.dismissAll()
              this.ngOnInit();
              this.files = []
              this.selectedItems = []
              /** spinner ends after 5 seconds */
              this.spinner.hide();
            }, 5000);
          } else if (data.message === 'ekstensi file salah') {
            /** spinner ends after 5 seconds */
            this.showToastr("Ekstensi file salah");
            this.files = []
            this.spinner.hide();
          } else {
            this.showToastr('Pilih Berkas Terlebih Dahulu!');
            /** spinner ends after 5 seconds */
            this.spinner.hide();
            this.files = []
            this.selectedItems = []
          }

        }, (error) => {
          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
            this.files = []
            this.selectedItems = []
            this.showToastr('Terjadi Kesalahan Saat Unggah Berkas')
          }, 5000);
        }
      );
  }

  checkValue(data) {
    if (data === "rahasia") {
      this.materiRahasia = false
    } else {
      this.materiRahasia = true
    }
  }

  tutupModal() {
    var formdata = new FormData();
    formdata.append('materi[]', "");
    this.modalService.dismissAll()
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}

