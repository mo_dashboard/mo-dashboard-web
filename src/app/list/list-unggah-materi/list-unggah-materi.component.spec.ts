import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUnggahMateriComponent } from './list-unggah-materi.component';

describe('ListUnggahMateriComponent', () => {
  let component: ListUnggahMateriComponent;
  let fixture: ComponentFixture<ListUnggahMateriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUnggahMateriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUnggahMateriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
