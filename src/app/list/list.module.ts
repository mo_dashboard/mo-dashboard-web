import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ListPenggunaComponent } from './list-pengguna/list-pengguna.component';
import { ListUndanganComponent } from './list-undangan/list-undangan.component';
import { ListUndanganUserComponent } from './list-undangan-user/list-undangan-user.component';
import { ListRisalahComponent } from './list-risalah/list-risalah.component';
import { ListMateriComponent } from './list-materi/list-materi.component';
import { ListDenahComponent } from './list-denah/list-denah.component';
import { ListUbahDataPenggunaComponent } from './list-ubah-data-pengguna/list-ubah-data-pengguna.component';
import { ListTambahRespondenUndanganComponent } from './list-tambah-responden-undangan/list-tambah-responden-undangan.component';
import { ListKirimMateriComponent } from './list-kirim-materi/list-kirim-materi.component';
import { ListUnggahMateriComponent } from './list-unggah-materi/list-unggah-materi.component';
import { ListDaftarMateriComponent } from './list-daftar-materi/list-daftar-materi.component';
import { ListMasterDataComponent } from './list-master-data/list-master-data.component';
import { ListJenisSidangComponent } from './list-jenis-sidang/list-jenis-sidang.component';
import { ListJabatanComponent } from './list-jabatan/list-jabatan.component';
import { ListRoleComponent } from './list-role/list-role.component';
import { ListLokasiComponent } from './list-lokasi/list-lokasi.component';
import { ListGroupComponent } from './list-group/list-group.component';
import { ListUnggahKirimRisalahComponent } from './list-unggah-kirim-risalah/list-unggah-kirim-risalah.component';
import { ListTambahRespondenRisalahComponent } from './list-tambah-responden-risalah/list-tambah-responden-risalah.component';
import { ListUbahRisalahComponent } from './list-ubah-risalah/list-ubah-risalah.component';
import { ListLaporanRisalahComponent } from './list-laporan-risalah/list-laporan-risalah.component';
import { ListRisalahUserComponent } from './list-risalah-user/list-risalah-user.component';
import { ListSurveyComponent } from './list-survey/list-survey.component';
import { ListKirimSurveyComponent } from './list-kirim-survey/list-kirim-survey.component';
import { ListLaporanSurveyComponent } from './list-laporan-survey/list-laporan-survey.component';
import { ListDownloadAplikasiComponent } from './list-download-aplikasi/list-download-aplikasi.component';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { SearchPipe } from './SearchPipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelect2Module } from 'ng-select2';
import { NgxPaginationModule } from 'ngx-pagination';
// import { UiSwitchModule } from 'ngx-toggle-switch';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ExcelService } from '../services/excel.service';

@NgModule({

	declarations: [ListPenggunaComponent,
		ListUbahDataPenggunaComponent,
		ListUndanganComponent,
		ListUndanganUserComponent,
		ListRisalahComponent,
		ListMateriComponent,
		ListDenahComponent,
		ListTambahRespondenUndanganComponent,
		ListKirimMateriComponent,
		ListUnggahMateriComponent,
		ListJenisSidangComponent,
		ListJabatanComponent,
		ListRoleComponent,
		ListLokasiComponent,
		ListGroupComponent,
		ListMasterDataComponent,
		ListUnggahKirimRisalahComponent,
		ListTambahRespondenRisalahComponent,
		ListUbahRisalahComponent,
		ListLaporanRisalahComponent,
		ListRisalahUserComponent,
		ListDaftarMateriComponent,
		ListSurveyComponent,
		ListKirimSurveyComponent,
		ListLaporanSurveyComponent,
		ListDownloadAplikasiComponent,
		SearchPipe
	],


	imports: [LazyLoadImageModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		FormsModule,
		ReactiveFormsModule ,
		NgMultiSelectDropDownModule,
		NgbModule,
		NgSelect2Module,
		NgxPaginationModule,
		// UiSwitchModule,
		PdfViewerModule,
		NgxDropzoneModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSpinnerModule,
		Ng2SearchPipeModule,
		ReactiveFormsModule
	],
	providers:[SearchPipe,ExcelService, {provide: OWL_DATE_TIME_LOCALE, useValue: 'id'},]
})
export class ListModule { }
