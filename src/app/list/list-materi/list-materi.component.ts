import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_BASE_URL } from '../../constants'
import { ToastrService } from 'ngx-toastr';
import { DownloadService } from '../../services/download.service'
import * as fileSaver from 'file-saver';
@Component({
  selector: 'app-list-materi',
  templateUrl: './list-materi.component.html',
  styleUrls: ['./list-materi.component.css']
})
export class ListMateriComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public undanganById: number;
  public notifUndanganById: any;
  public searchTerm: string;
  public p: number = 1;
  public materiUndanganById: any;
  public link: string;
  public pdfSrc: SafeUrl;
  public videoShow: SafeUrl;
  public id_undangan: string;
  public materiByIdUndangan: any = [];
  public isShowMateri: boolean = true;
  public isShowAction: boolean = true;
  public isShowMp4: boolean = false;
  public isNotMp4: boolean = true;
  public isShowDownload: boolean = false;

  constructor(
    private sidebarService: SidebarService,
    private cdr: ChangeDetectorRef,
    private http: HttpClient,
    private modalService: NgbModal,
    private searchService: SearchService,
    private domSanitizer: DomSanitizer,
    private toastr: ToastrService,
    private renderer: Renderer2,
    private downloadService: DownloadService
  ) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }

  ngOnInit() {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    const id = localStorage.getItem('id_user')
    this.http.get(API_BASE_URL + 'undangan-module/getAllUndanganByMateri', { headers }).toPromise().then((res: any) => {
      this.dataUndangan = res.result.sort((a, b) => {
        return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
      })
      this.lengthDataUndangan = this.dataUndangan.length
      this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
      this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
      this.isLoading = false;
      if (this.dataUndangan[0].materi === null) {
        let element = document.getElementsByClassName('materiKosong');
      }
    })
  }
  onSearchTermChange(): void {
    this.searchService.search(this.searchTerm).subscribe();
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchMateriUndangan(event) {
    this.p = 1;
  }
  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }
  downloadMateri2(item) {
    const headers = new HttpHeaders()
      .set('Access-Control-Allow-Headers', 'Content-Type')
    this.downloadService.downloadFile(item.materi).subscribe(response => {
      let blob: any = new Blob([response], { type: response.type });
      const url = window.URL.createObjectURL(blob);
      fileSaver.saveAs(blob, item.file_name);
    })
  }

  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    // console.log(id)
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers }).toPromise().then((res: any) => {
      this.undanganById = res.result
      this.id_undangan = id.toString()
    })
    //CHECK DEVICE
    let check = false;
    this.isShowMateri = true;
    this.isShowMateri = true;
    let window: any;
    (function (a) {
      if (
        /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
          a
        ) ||
        /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
          a.substr(0, 4)
        )
      )
        check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    if (check) {
      this.showToastr('Materi Tidak Tersedia. Gunakan aplikasi E-Kabinet Mobile Anda');
      this.isShowMateri = false;
      this.isShowAction = false;
    }
    //END CHECK DEVICE
    this.id_undangan = id.toString()
    this.http.get(API_BASE_URL + 'materi-module/getMateriByIdUndangan/' + id, { headers }).toPromise().then((res: any) => {
      this.materiByIdUndangan = res.result
    })
    let namaRole = localStorage.getItem('nama_role')
    if (namaRole === 'Super Admin' || 'Admin Materi') {
      this.isShowDownload = true;
    }
  }

  openFile(id: number, content, pdfMateri, id_bahan_sidang: number, fileType) {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http.get(API_BASE_URL + 'materi-module/getPenerimaByIdBahanSidang/' + id_bahan_sidang, { headers }).toPromise().then((res: any) => {
      this.notifUndanganById = res.result
    })
    if (fileType === "video/mp4") {
      this.videoShow = this.domSanitizer.bypassSecurityTrustResourceUrl(pdfMateri + "#toolbar=0&navpanes=0&scrollbar=0");
      this.isShowMp4 = true;
      this.isNotMp4 = false;
    } else {
      this.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(pdfMateri + "#toolbar=0&navpanes=0&scrollbar=0");
      this.isNotMp4 = true;
      this.isShowMp4 = false;
    }

  }
  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}
