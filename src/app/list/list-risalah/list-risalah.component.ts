
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { API_BASE_URL } from '../../constants'
import { Router } from '@angular/router';
import * as _ from 'lodash';

var moment = require('moment')

@Component({
  selector: 'app-list-risalah',
  templateUrl: './list-risalah.component.html',
  styleUrls: ['./list-risalah.component.css']
})
export class ListRisalahComponent implements OnInit {
  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public notifRisalahById: any;
  public searchTerm: string;
  public p: number = 1;
  public risalahById: number;
  public risalahById2: any;
  public link: string;
  public pdfSrc: SafeUrl;
  public pdfPrint: SafeUrl;
  public id_risalah: string;
  public idNotifRisalah: string;
  public idUser: any;
  public timerInterval: any;
  public undanganById: number;
  public dataUser: any = [];
  public selectedItems: Array<any>;
  public dropdownList: Array<any>;
  public dropdownSettings: any;
  public undanganId: string;
  public id_undangan: string;
  public user_create: string = '1';
  public isLoadingRisalah: boolean = false;
  public detailRisalahOff: any = [];
  public risalahOfflineById: any = [];
  public checkedTrue: any = [];
  public tanggal_kirim_pos: any;
  public tanggal_kirim_email: any;
  public tanggal_kirim_langsung: any;
  public alamat_pos: any;
  public alamat_email: any;
  public alamat_langsung: any;
  public keterangan: any;
  public isChecked: boolean = false;
  public detailTracking: any;
  public filterTracking: any;
  public alamat_pengiriman: any;
  public tanggal_email: any;
  public grouped: any = [];


  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private searchService: SearchService, private domSanitizer: DomSanitizer, private spinner: NgxSpinnerService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    this.dropdownList = [
      { item_id: 1, item_text: 'Cheese' },
      { item_id: 2, item_text: 'Tomatoes' },
      { item_id: 3, item_text: 'Mozzarella' },
      { item_id: 4, item_text: 'Mushrooms' },
      { item_id: 5, item_text: 'Pepperoni' },
      { item_id: 6, item_text: 'Onions' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id_user',
      textField: 'buatSelect',
      selectAllText: 'Pilih Semua',
      unSelectAllText: 'Tidak ada yang Dipilih',
      allowSearchFilter: true,
      searchPlaceholderText: 'Cari'
    };
  }



  ngOnInit() {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.http.get(API_BASE_URL + 'undangan-module/getAllUndanganByRisalah', { headers }).toPromise().then((res: any) => {
      this.dataUndangan = res.result.sort((a, b) => {
        return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
      })
      if (this.dataUndangan[0].nomor_risalah === null) {
        let element = document.getElementsByClassName('risalahKosong');
      }
      if (this.dataUndangan[0].status_risalah === '0') {
        let element = document.getElementsByClassName('tidakDistribusi');
      }
      this.lengthDataUndangan = this.dataUndangan.length
      this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
      this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
      this.isLoading = false;
    })
  }
  onSearchTermChange(): void {
    this.searchService.search(this.searchTerm).subscribe();
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchRisalah(event) {
    this.p = 1;
  }
  onChangePengiriman(event, item, kirim) {

    if (kirim === 'pos') {
      item.checked_pos = event.target.checked
    }
    if (kirim === 'email') {
      item.checked_email = event.target.checked
    }
    if (kirim === 'langsung') {
      item.checked_langsung = event.target.checked
    }
  }
  //End tracking
  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }

  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  async open(id: number, content) {
    this.id_undangan = id.toString();
    this.checkedTrue = []
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    await this.http.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndangan/' + id, { headers }).toPromise().then((res: any) => {
      this.risalahById = res.result
      this.id_risalah = this.risalahById[0].id_risalah
      if (res.result == 'no data found') {
        this.showToastr('Detail Risalah Belum Tersedia');
      } else { this.modalService.open(content, { size: <any>'xl', backdrop: 'static', keyboard: false }) }
    })

    await this.http.get(API_BASE_URL + 'risalah-module/getDetailTrackingRisalah/' + id, { headers }).toPromise().then((res: any): void => {
      this.detailTracking = res.result
      this.detailTracking.map(item => {

        if (item.tanggal_pengiriman !== null) {
          var dt = item.tanggal_pengiriman.split(/\-|\s/);
        }
        item.id_user = item.penerima //
        item.tanggal_kirim_email = item.metode_pengiriman === 'EMAIL' ? new Date(dt.slice(0, 3).join('-') + ' ' + dt[3]) : ''
        item.tanggal_kirim_pos = item.metode_pengiriman === 'POS' ? new Date(dt.slice(0, 3).join('-') + ' ' + dt[3]) : ''
        item.tanggal_kirim_langsung = item.metode_pengiriman === 'LANGSUNG' ? new Date(dt.slice(0, 3).join('-') + ' ' + dt[3]) : ''
        item.metode_pengiriman_email = item.metode_pengiriman === 'EMAIL' ? true : false
        item.metode_pengiriman_pos = item.metode_pengiriman === 'POS' ? true : false
        item.metode_pengiriman_langsung = item.metode_pengiriman === 'LANGSUNG' ? true : false
        item.alamat_pos = item.metode_pengiriman === 'POS' ? item.alamat_pengiriman : item.alamat_pos
        item.alamat_email = item.metode_pengiriman === 'EMAIL' ? item.alamat_pengiriman : item.email
        item.alamat_langsung = item.metode_pengiriman === 'LANGSUNG' ? item.alamat_pengiriman : item.alamat_langsung
        item.keterangan = item.keterangan
        item.id_undangan = id //
        item.alamat_pengiriman_pos = item.alamat_pos === "null" || item.alamat_pos === null ? '' : item.alamat_pos
        item.alamat_pengiriman_email = item.email
        item.alamat_pengiriman_langsung = item.alamat_langsung === "null" || item.alamat_langsung === null ? '' : item.alamat_langsung
        item.id_transaksi = item.id_transaksi //
        item.metode_pengiriman_pos2 = item.metode_pengiriman === 'POS' ? 'POS' : ''
        item.metode_pengiriman_email2 = item.metode_pengiriman === 'EMAIL' ? 'EMAIL' : ''
        item.metode_pengiriman_langsung2 = item.metode_pengiriman === 'LANGSUNG' ? 'LANGSUNG' : ''
        item.tanggal_pengiriman = item.tanggal_pengiriman
      })
      this.grouped = Object.entries(_.mapValues(_.groupBy(this.detailTracking, 'id_user'),
        clist => clist.map(item => _.omit(item, 'id_user'))))
      this.grouped.map((item: any, index: number) => {
        item.checked_pos = false
        item.checked_email = false
        item.checked_langsung = false

        item.tanggal_pos = ''
        item.tanggal_email = ''
        item.tanggal_langsung = ''

        item.alamat_pos = ''
        item.alamat_email = ''
        item.alamat_langsung = ''

        item.id_undangan = ''
        item.penerima = item[0]
        item.keterangan = ''

        item[1].map(item1 => {

          if (item1.tanggal_pengiriman !== null) {
            var dt = item1.tanggal_pengiriman.split(/\-|\s/);
          }
          item.alamat_pos = item1.alamat_pengiriman_pos
          item.alamat_email = item1.alamat_pengiriman_email
          item.alamat_langsung = item1.alamat_pengiriman_langsung

          if (item1.metode_pengiriman_pos === true) {
            item.checked_pos = true
            item.tanggal_pos = item1.tanggal_kirim_pos
            item.id_undangan = item1.id_undangan
            item.penerima = item1.penerima
            item.keterangan = item1.keterangan
          }
          if (item1.metode_pengiriman_email === true) {
            item.checked_email = true
            item.tanggal_email = item1.tanggal_kirim_email
            item.id_undangan = item1.id_undangan
            item.penerima = item1.penerima
            item.keterangan = item1.keterangan
          }
          if (item1.metode_pengiriman_langsung === true) {
            item.checked_langsung = true
            item.tanggal_langsung = item1.tanggal_kirim_langsung
            item.id_undangan = item1.id_undangan
            item.penerima = item1.penerima
            item.keterangan = item1.keterangan
          }
        })

      })
      this.filterTracking = Array.from(this.detailTracking.reduce((m, t) => m.set(t.penerima, t), new Map()).values());
    })
  }
  //traking risalah
  simpanTracking() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    var notifPengiriman: any = [];

    this.grouped.map((item, index) => {

      if (item.checked_pos === true) {
        notifPengiriman.push({
          "id_undangan": this.id_undangan,
          "id_risalah": this.id_risalah,
          "id_user": item.penerima,
          "metode_pengiriman": "POS",
          "tanggal_pengiriman": moment(item.tanggal_pos).format('YYYY-MM-DD HH:mm'),
          "alamat_pengiriman": item.alamat_pos,
          "keterangan": item.keterangan,
          "user_create": localStorage.getItem('id_user'),
        })
      }
      if (item.checked_langsung === true) {
        notifPengiriman.push({
          "id_undangan": this.id_undangan,
          "id_risalah": this.id_risalah,
          "id_user": item.penerima,
          "metode_pengiriman": "LANGSUNG",
          "tanggal_pengiriman": moment(item.tanggal_langsung).format('YYYY-MM-DD HH:mm'),
          "alamat_pengiriman": item.alamat_langsung,
          "keterangan": item.keterangan,
          "user_create": localStorage.getItem('id_user'),
        })
        // console.log('if langsung',notifPengiriman);
      }
      if (item.checked_email === true) {
        notifPengiriman.push({
          "id_undangan": this.id_undangan,
          "id_risalah": this.id_risalah,
          "id_user": item.penerima,
          "metode_pengiriman": "EMAIL",
          "tanggal_pengiriman": moment(item.tanggal_email).format('YYYY-MM-DD HH:mm'),
          "alamat_pengiriman": item.alamat_email,
          "keterangan": item.keterangan,
          "user_create": localStorage.getItem('id_user'),
        })
      }
    })

    if (notifPengiriman.length === 0) {
      this.showToastr('Isi Data Distribusi Risalah Dengan Lengkap');

    } else {

      this.http
        .post<any>(API_BASE_URL + 'risalah-module/createTrackingRisalah', notifPengiriman, { headers })
        .subscribe(
          (data: any) => {
            if (data.response_code === "IF") {
              this.showToastr("Data Tidak Lengkap");
            }
            else if (data.response_code === "200") {
              this.spinner.show();
              setTimeout(() => {
                /** spinner ends after 5 seconds */
                this.spinner.hide();
                this.modalService.dismissAll()
                this.showToastrBerhasil("Tracking Distribusi Risalah Berhasil");
              }, 5000);
            } else {
              this.showToastr('Terjadi Kesalahan Saat Menambah Distribusi Risalah');
            }
          }
        )
    }
  }
  //end tracking risalah
  alertConfirmation(id: number, content) {
    Swal.fire({
      text: ("Berkas Yang Ingin Anda Lihat Bersifat Rahasia"),
      type: "warning",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK",
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          text: 'Mohon Tunggu',
          timer: 1000,
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            this.timerInterval = setInterval(() => {
              const content = Swal.getContent()
            }, 100)
          },
          onClose: () => {
            clearInterval(this.timerInterval)
          }
        })
        this.openFile(id, content)
      }
    })
  }

  openFile(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    let userId = localStorage.getItem('id_user')
    this.http.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndanganIdUser/' + id + '/' + userId, { headers }).toPromise().then((res: any) => {
      this.risalahById2 = res.result
      if (res.result !== 'no data found' && this.risalahById2 !== undefined) {
        this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
        var showFile = this;
        var link = res.result[0].link_risalah;
        showFile.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah' + link + "#toolbar=0&navpanes=0&scrollbar=0"
        );
        showFile.pdfPrint = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah' + link
        );
        showFile.isLoadingPdf = false;
      } else {
        this.showToastr('Pengantar Presiden Tidak Tersedia Untuk User Anda');
      }
    });
    this.http
      .get(API_BASE_URL + 'risalah-module/getNotifRisalahByIdUser/' + userId, { headers })
      .toPromise()
      .then((res: any) => {
        this.idNotifRisalah = res.result[0].id_notifikasi_risalah;
      });
  }

  counterDownload() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterDownloadRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }
  counterPrint() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterPrintRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }

  detailRisalahOffline(item: any, id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.dataUser = []
    this.selectedItems = []
    this.http.get(API_BASE_URL + 'risalah-module/getDetailRisalahOffline/' + id, { headers }).toPromise().then((res: any) => {
      this.detailRisalahOff = res.result;
      if (this.detailRisalahOff !== 'no data found') {
        this.detailRisalahOff = res.result

      }
    });
  }

  downloadRisalah(link) {
    this.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
      API_BASE_URL + 'pdf_file/risalah' + link
    );
  }

  openRisalahOffline(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.dataUser = []
    this.selectedItems = []

    this.http.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers }).toPromise().then((res: any) => {
      this.undanganById = res.result;
      this.undanganId = res.result[0].id_undangan;
    });

    this.id_undangan = id.toString();

    this.http.get(API_BASE_URL + 'user-module/getAllUser', { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.dataUser = res.result;
        this.idUser = res.result[0].id_user;
        this.dataUser = this.dataUser.map((item: any, index: number) => {
          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }
        })
      }
    })
  }

  createRisalahOffline() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.user_create = localStorage.getItem('id_user')
    var body_responden = this.selectedItems.map((item: any, index: number) => {
      return {
        id_undangan: this.undanganId,
        id_user: item.id_user,
        user_create: this.user_create
      }
    })
    this.http.post<any>(API_BASE_URL + 'risalah-module/createRisalahOffline', body_responden, { headers }).subscribe(data => {
      if (data.response_code === '200') {
        this.showToastrBerhasil('Berhasil Membuat Pengantar Presiden Offline');
        this.modalService.dismissAll()
        this.router.navigate(['/admin/list/list-risalah']);
        this.selectedItems = []
        this.ngOnInit();
      } else {
        this.showToastr('Terjadi Kesalahan Saat Membuat Pengantar Presiden Offline');
        this.selectedItems = []
      }
    })
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}

