import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRisalahComponent } from './list-risalah.component';

describe('ListRisalahComponent', () => {
  let component: ListRisalahComponent;
  let fixture: ComponentFixture<ListRisalahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRisalahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRisalahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
