import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { data } from 'jquery';
import { Router } from '@angular/router';
import { API_BASE_URL } from '../../constants'

@Component({
  selector: 'app-list-master-data',
  templateUrl: './list-master-data.component.html',
  styleUrls: ['./list-master-data.component.css']
})
export class ListMasterDataComponent implements OnInit {
  [x: string]: any;

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public lengthDataJenisSidang: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceJenisSidang: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;

  public id_jenis_sidang: string;
  public nama_sidang: string = '';
  public jenisSidangById: Array<any>;
  public dataJenisSidang: any = [];

  public id_lokasi: string;
  public nama_lokasi: string = '';
  public lokasiById: Array<any>;
  public dataLokasi: any = [];

  public id_group: string;
  public nama_group: string = '';
  public groupById: Array<any>;
  public dataGroup: any = [];

  public id_jabatan: string;
  public nama_jabatan: string;
  public jabatanById: any[];
  public dataJabatan: any = [];

  public user_create: string = localStorage.getItem('id_user');
  public user_update: string = localStorage.getItem('id_user');
  public p: number = 1;


  showTable = {
    jenisSidang: true,
    lokasiSidang: false,
    jabatan: false,
    group: false
  }

  onButtonGroupClick($event) {
    let clickedElement = $event.target || $event.srcElement;

    if (clickedElement.nodeName === "BUTTON") {

      let isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector(".active");
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove("active");
      }

      clickedElement.className += " active";
    }
  }

  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }
  ngOnInit() {
    this.reloadData()
  }
  reloadData() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get(API_BASE_URL + 'master-module/getAllJenisSidang', { headers }).toPromise().then((res: any) => {
      this.dataJenisSidang = res.result
      this.lengthDataJenisSidang = this.dataJenisSidang.length
      this.jumlahPage = Math.ceil(this.lengthDataJenisSidang / this.pembagi)
      this.sliceJenisSidang = this.dataJenisSidang.slice(0, this.pembagi)
      this.isLoading = false;
    })

    this.http.get(API_BASE_URL + 'master-module/getAllLokasi', { headers }).toPromise().then((res: any) => {
      this.dataLokasi = res.result
      this.lengthDataLokasi = this.dataLokasi.length
      this.jumlahPage = Math.ceil(this.lengthDataLokasi / this.pembagi)
      this.sliceLokasi = this.dataLokasi.slice(0, this.pembagi)
      this.isLoading = false;
    })

    this.http.get(API_BASE_URL + 'master-module/getAllJabatan', { headers }).toPromise().then((res: any) => {
      this.dataJabatan = res.result
      this.lengthDataJabatan = this.dataJabatan.length
      this.jumlahPage = Math.ceil(this.lengthDataJabatan / this.pembagi)
      this.sliceJabatan = this.dataJabatan.slice(0, this.pembagi)
      this.isLoading = false;
    })

    this.http.get(API_BASE_URL + 'master-module/getAllGroup', { headers }).toPromise().then((res: any) => {
      this.dataGroup = res.result
      this.lengthDataGroup = this.dataGroup.length
      this.jumlahPage = Math.ceil(this.lengthDataGroup / this.pembagi)
      this.sliceGroup = this.dataGroup.slice(0, this.pembagi)
      this.isLoading = false;
    })
  }

  openSidang(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.id_jenis_sidang = id.toString()
    this.modalService.open(content, { size: 'lg' })
    this.http.get(API_BASE_URL + 'master-module/getJenisSidangById/' + id, { headers }).toPromise().then((res: any) => {
      this.jenisSidangById = res.result
      this.jenisSidangById.map(res => this.nama_sidang = res.nama_sidang)
    })
  }

  openModal(content) {
    this.nama_sidang = "";
    this.nama_lokasi = "";
    this.nama_group = "";
    this.nama_jabatan = "";
    this.modalService.open(content, { size: 'lg' })
  }

  tutupModal() {
    this.nama_sidang = "";
    this.nama_lokasi = "";
    this.nama_group = "";
    this.nama_jabatan = "";
    this.modalService.dismissAll();
  }

  tambahSidang(content) {

    this.modalService.open(content, { size: 'lg' })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    const body = {
      nama_sidang: this.nama_sidang,
      user_create: this.user_create
    }

    this.http
      .post<any>(API_BASE_URL + 'master-module/createJenisSidang', body, { headers })
      .subscribe(
        (data: any) => {
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Jenis Sidang');
            this.nama_sidang = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-master-data']);
            this.ngOnInit()
          } else {
            this.nama_sidang = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Jenis Sidang');
          }
        }
      )
  }

  ubahSidang() {
    if (this.nama_sidang !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      const body = {
        id_jenis_sidang: this.id_jenis_sidang,
        nama_sidang: this.nama_sidang,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateJenisSidang/', body, { headers })
        .subscribe(
          (data: any) => {
            // console.log('ubah sidang api', data);
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Jenis Sidang');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-master-data']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Jenis Sidang');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Jenis Sidang ');
    }
  }

  deleteSidang(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteJenisSidang/' + id).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }

  // Lokasi Sidang
  openLokasi(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.id_lokasi = id.toString()
    this.modalService.open(content, { size: 'lg' })
    this.http.get(API_BASE_URL + 'master-module/getLokasiById/' + id, { headers }).toPromise().then((res: any) => {
      this.lokasiById = res.result
      this.lokasiById.map(res => this.nama_lokasi = res.nama_lokasi)
    })
  }

  tambahLokasi(content) {

    this.modalService.open(content, { size: 'lg' })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    const body = {
      nama_lokasi: this.nama_lokasi,
      user_create: this.user_create
    }
    this.http
      .post<any>(API_BASE_URL + 'master-module/createLokasi', body, { headers })
      .subscribe(
        (data: any) => {
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Lokasi');
            this.nama_lokasi = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-master-data']);
            this.ngOnInit()
          } else {
            this.nama_lokasi = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Lokasi');
          }
        }
      )
  }

  ubahLokasi() {
    if (this.nama_lokasi !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      const body = {
        id_lokasi: this.id_lokasi,
        nama_lokasi: this.nama_lokasi,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateLokasi/', body, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Lokasi');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-master-data']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Lokasi');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Lokasi ');
    }
  }

  deleteLokasi(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteLokasi/' + id).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }

  //Jabatan
  openJabatan(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    // console.log(id)
    this.id_jabatan = id.toString()
    this.modalService.open(content, { size: 'lg' })
    this.http.get(API_BASE_URL + 'master-module/getJabatanById/' + id, { headers }).toPromise().then((res: any) => {
      this.jabatanById = res.result
      this.jabatanById.map(res => this.nama_jabatan = res.nama_jabatan)
    })
  }

  tambahJabatan(content) {

    this.modalService.open(content, { size: 'lg' })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    const body = {
      nama_jabatan: this.nama_jabatan,
      user_create: this.user_create
    }
    this.http
      .post<any>(API_BASE_URL + 'master-module/createJabatan', body, { headers })
      .subscribe(
        (data: any) => {
          // console.log('tester', data);
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Jabatan');
            this.nama_jabatan = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-master-data']);
            this.ngOnInit()
          } else {
            this.nama_jabatan = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Jabatan');
          }
        }
      )
  }

  ubahJabatan() {
    if (this.nama_jabatan !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      const body = {
        id_jabatan: this.id_jabatan,
        nama_jabatan: this.nama_jabatan,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateJabatan/', body, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Jabatan');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-master-data']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Jabatan');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Jabatan ');
    }
  }

  deleteJabatan(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteJabatan/' + id).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }

  //Grup
  openGroup(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.id_group = id.toString()
    this.modalService.open(content, { size: 'lg' })
    this.http.get(API_BASE_URL + 'master-module/getGroupById/' + id, { headers }).toPromise().then((res: any) => {
      this.groupById = res.result
      this.groupById.map(res => this.nama_group = res.nama_group)
    })
  }

  tambahGroup(content) {

    this.modalService.open(content, { size: 'lg' })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    const body = {
      nama_group: this.nama_group,
      user_create: this.user_create
    }
    this.http
      .post<any>(API_BASE_URL + 'master-module/createGroup', body, { headers })
      .subscribe(
        (data: any) => {
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Group');
            this.nama_group = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-master-data']);
            this.ngOnInit()
          } else {
            this.nama_group = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Group');
          }
        }
      )
  }

  ubahGroup() {
    if (this.nama_group !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      const body = {
        id_group: this.id_group,
        nama_group: this.nama_group,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateGroup/', body, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Group');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-master-data']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Group');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Group ');
    }
  }

  deleteGroup(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteGroup/' + id).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }


  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchMasterData(event) {
    this.p = 1;
  }
  gantiPage(n: number) {
    this.sliceJenisSidang = this.dataJenisSidang.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }
  showToastr(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  onFileSelected(event) {
    this.nama_sidang = event.target.files;
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }
}