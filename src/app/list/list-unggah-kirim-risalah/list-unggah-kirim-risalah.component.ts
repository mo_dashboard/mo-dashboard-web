import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Item } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { API_BASE_URL } from '../../constants'

var moment = require('moment')

declare var $: any;

@Component({
  selector: 'app-list-unggah-kirim-risalah',
  templateUrl: './list-unggah-kirim-risalah.component.html',
  styleUrls: ['./list-unggah-kirim-risalah.component.css']
})
export class ListUnggahKirimRisalahComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dropdownList: Array<any>;
  public selectedItems: Array<any>;
  public dropdownSettings: any;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public undanganById: number;
  public notifUndanganById: any;
  public risalah: any = [];
  public path_risalah: string = '';
  public user_create: string = '1';
  public id_undangan: string;
  public p: number = 1;
  public isLoadingRisalah: boolean = false;
  public risalahById: any;
  public link: string;
  public pdfSrc: SafeUrl;
  public dataUser: any = [];
  public idUser: string = '';
  public jabatanPimpinan: any;
  public jam_mulai_sidang: string = '';
  public nomor_risalah: string = '';
  public status_risalah: string = '1';
  public zona_waktu: string = '';
  public selectedDataStatus: string = '';
  public tgl_ttd_risalah: string;
  public penyimpanan_arsip: string;
  public dataPenerima: any;
  public WIB: string = "WIB";


  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private searchService: SearchService, private domSanitizer: DomSanitizer, private spinner: NgxSpinnerService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    this.dropdownList = [
      { item_id: 1, item_text: 'Cheese' },
      { item_id: 2, item_text: 'Tomatoes' },
      { item_id: 3, item_text: 'Mozzarella' },
      { item_id: 4, item_text: 'Mushrooms' },
      { item_id: 5, item_text: 'Pepperoni' },
      { item_id: 6, item_text: 'Onions' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id_user',
      textField: 'buatSelect',
      selectAllText: 'Pilih Semua',
      unSelectAllText: 'Tidak ada yang Dipilih',
      allowSearchFilter: true,
      searchPlaceholderText: 'Cari'
    };
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.http.get(API_BASE_URL + 'undangan-module/getAllUndanganByRisalah', { headers }).toPromise().then((res: any) => {
      this.dataUndangan = res.result.sort((a, b) => {
        return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
      })
      if (this.dataUndangan[0].nomor_risalah === null) {
        let element = document.getElementsByClassName('risalahKosong');
      }
      this.dataUndangan = res.result
      this.lengthDataUndangan = this.dataUndangan.length
      this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
      this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
      this.isLoading = false;
    });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchUnggahRisalah(event) {
    this.p = 1;
  }

  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }


  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.dataUser = []
    this.selectedItems = []

    this.jam_mulai_sidang = ''
    this.nomor_risalah = ''
    this.zona_waktu = 'WIB'
    this.status_risalah = '1'
    this.tgl_ttd_risalah = ''
    this.penyimpanan_arsip = ''
    this.risalah = []

    this.http.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers }).toPromise().then((res: any) => {
      this.undanganById = res.result;
    });

    this.id_undangan = id.toString();

    this.http.get(API_BASE_URL + 'risalah-module/getRespondenBelumTerimaRisalah/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.dataUser = res.result;
        this.idUser = res.result[0].id_user;

        this.dataUser = this.dataUser.map((item: any, index: number) => {

          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }
        })
      }
    })
    this.http
      .get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers })
      .toPromise()
      .then((res: any) => {
        // console.log('penerima risalah', res.result);
        this.dataPenerima = res.result
        // console.log('data user', this.dataPenerima);
        this.selectedItems = res.result;
        this.selectedItems = this.selectedItems.map((item: any, index: number) => {
          return {
            ...item,
            id_user: item.penerima,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          };
        });
      });
  }

  changeZona(event) {
    this.zona_waktu = event.target.value
  }

  unggahRisalah() {
    if (
      this.jam_mulai_sidang !== '' &&
      this.nomor_risalah !== '' &&
      this.zona_waktu !== '' &&
      this.status_risalah !== '' &&
      this.tgl_ttd_risalah !== '' &&
      this.penyimpanan_arsip !== '' &&
      this.risalah !== [] &&
      this.risalah[0] !== undefined
    ) {
      const headers = new HttpHeaders()
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      this.user_create = localStorage.getItem("id_user")

      var body_responden = this.selectedItems.map((item: any, index: number) => {
        return {
          ...item,
          id_user: item.id_user,
        }
      })

      var formdata = new FormData();
      formdata.append(
        'risalah',
        this.risalah[0]
      );
      formdata.append('jam_mulai_sidang', moment(this.jam_mulai_sidang).format('HH:mm'));
      formdata.append('zona_waktu', this.zona_waktu);
      formdata.append('nomor_risalah', this.nomor_risalah);
      formdata.append('status_risalah', this.status_risalah);
      formdata.append('tgl_ttd_risalah', moment(this.tgl_ttd_risalah).format('YYYY-MM-DD'));
      formdata.append('penyimpanan_arsip', this.penyimpanan_arsip);
      formdata.append('user_create', this.user_create);
      formdata.append('id_undangan', this.id_undangan);
      formdata.append('penerima', JSON.stringify(body_responden));

      var body_upload = formdata;
      this.spinner.show();
      this.http
        .post<any>(API_BASE_URL + 'risalah-module/sendRisalah', body_upload, { headers })
        .subscribe(
          (data) => {
            if (data.response_code === '200') {
              setTimeout(() => {
                this.showToastrBerhasil('Berhasil Mengirim Pengantar Presiden');
                this.modalService.dismissAll()
                this.ngOnInit();
                this.router.navigate(['/admin/list/list-unggah-kirim-risalah']);
                /** spinner ends after 5 seconds */
                this.spinner.hide();
              }, 5000);
            } else if (data.result == "ekstensi file salah") {
              this.showToastr("Ektstensi File Salah");
              this.spinner.hide();
              this.modalService.dismissAll()
            } else {
              this.showToastr(data.result);
              this.spinner.hide();
            }
          },
          (error) => this.showToastr('Terjadi Kesalahan Saat Membuat Pengantar Presiden')
        );
    } else {
      this.showToastr('Data Tidak Lengkap');
    }
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  onFileSelected(event) {
    this.risalah = event.target.files;
    if (this.risalah[0]) {
      let file: File = this.risalah[0];
      var idxDot = file.name.lastIndexOf('.') + 1;
      var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
      var sizeFile = file.size;
      var validationExt = this.checkExt(extFile);
      if (validationExt == false) {
        var validationSize = this.checkSize(sizeFile);
      }
    }
  }

  checkExt(extFile) {
    if (extFile == 'pdf') {
      this.isLoadingRisalah = false;
      return false;
    } else {
      this.isLoadingRisalah = true;
      this.showToastr('Unggah file dengan ekstensi pdf');
      this.path_risalah = '';
      return true;
    }
  }
  checkSize(sizeFile) {
    if (sizeFile <= 5242880) {
      this.isLoadingRisalah = false;
      return false;
    } else {
      this.isLoadingRisalah = true;
      this.showToastr('File yang anda unggah lebih dari 5MB');
      this.path_risalah = '';
      return true;
    }
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}
