import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUnggahKirimRisalahComponent } from './list-unggah-kirim-risalah.component';

describe('ListUnggahKirimRisalahComponent', () => {
  let component: ListUnggahKirimRisalahComponent;
  let fixture: ComponentFixture<ListUnggahKirimRisalahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUnggahKirimRisalahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUnggahKirimRisalahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
