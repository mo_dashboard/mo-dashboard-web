import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUbahDataPenggunaComponent } from './list-ubah-data-pengguna.component';

describe('ListUbahDataPenggunaComponent', () => {
  let component: ListUbahDataPenggunaComponent;
  let fixture: ComponentFixture<ListUbahDataPenggunaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUbahDataPenggunaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUbahDataPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
