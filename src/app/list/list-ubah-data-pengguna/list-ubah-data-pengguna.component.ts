import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { query } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import { Select2OptionData } from 'ng-select2';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_BASE_URL } from '../../constants'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

var moment = require('moment')
@Component({
  selector: 'app-list-ubah-data-pengguna',
  templateUrl: './list-ubah-data-pengguna.component.html',
  styleUrls: ['./list-ubah-data-pengguna.component.css']
})
export class ListUbahDataPenggunaComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public userId: string;
  public dataUser: any = [];
  public lengthDataUser: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUser: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingGambar: boolean = true;
  public isSearching: boolean = false;
  public dataUserById: any = [];
  public username: string;
  public result: any = [];
  public id_user: string;
  public id_role: Array<Select2OptionData>;
  public id_group: string;
  public id_jabatan: string;
  public nama_jabatan: string;
  public nama_user: string;
  public alamat_pos: string;
  public alamat_langsung: string;
  public msisdn: string;
  public imei: string;
  public email: string;
  public user_update: string = localStorage.getItem('id_user')
  public isiModal: any = {};
  public dataJabatan: Array<Select2OptionData>;
  public dataRole: any;
  public dataGroup: any;
  public selectedDataRole: string = '';
  public selectedDataJabatan: string = '';
  public selectedDataGroup: string = '';
  public foto: any = [];
  public path_foto: string = '';
  public link_foto: string;
  public p: number = 1;
  public password_lama: string;
  public password_baru: string;
  public real_password: string;
  public fieldTextType: boolean;
  public fieldTextType2: boolean;
  public isLoadingForm: boolean = false;
  public passForm: FormGroup;

  constructor(
    private sidebarService: SidebarService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private http: HttpClient,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService

  ) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get(API_BASE_URL + 'user-module/getAllUser/', { headers }).toPromise().then((res: any) => {
      var userData = res.result.map((item: any, index: number) => {
        return {
          ...item,
          format_tanggal: moment(item.create_date).format('DD-MM-YYYY HH:mm')
        }
      })

      this.dataUser = userData.sort((a, b) => {
        return Number(a.id_user) > Number(b.id_user)
          ? 1
          : Number(a.id_user) < Number(b.id_user) ? -1 : 0;
      });
      this.lengthDataUser = this.dataUser.length;
      this.jumlahPage = Math.ceil(this.lengthDataUser / this.pembagi);
      this.sliceDataUser = this.dataUser.slice(0, this.pembagi);
      this.isLoading = false;
    })

  }
  async open(id: number, content, item) {
    this.isLoadingGambar = await true
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    await this.http.get(API_BASE_URL + 'user-module/getUserById/' + id, { headers }).toPromise().then(async (res: any) => {
      this.isiModal = await item;
      this.isiModal = await res.result[0];
      this.dataUserById = await res.result.map((item: any, index: number) => {
        return {
          ...item,
          alamat_pos2: item.alamat_pos === null || item.alamat_pos === 'null' ? '-' : item.alamat_pos,
          alamat_langsung2: item.alamat_langsung === null || item.alamat_langsung === 'null' ? '-' : item.alamat_langsung
        }
      })
      this.isLoadingGambar = await false
    })

    await this.http.get(API_BASE_URL + 'master-module/getAllJabatan', { headers }).toPromise().then((res: any) => {
      this.dataJabatan = res.result
      this.dataJabatan = this.dataJabatan.map((item: any, index: number) => {
        return {
          ...item,
          id: item.id_jabatan,
          text: item.nama_jabatan
        }
      })
    })
    await this.http.get(API_BASE_URL + 'master-module/getAllRole', { headers }).toPromise().then((res: any) => {
      this.dataRole = res.result
      this.dataRole = this.dataRole.map((item: any, index: number) => {
        return {
          ...item,
          id: item.id_role,
          text: item.nama_role
        }
      })
    })

    await this.http.get(API_BASE_URL + 'master-module/getAllGroup', { headers }).toPromise().then((res: any) => {
      this.dataGroup = res.result
      this.dataGroup = this.dataGroup.map((item: any, index: number) => {
        return {
          ...item,
          id: item.id_group,
          text: item.nama_group
        }
      })
    })
    await this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
  }

  ubahDataPengguna() {
    if (
      this.isiModal.id_user !== '' && this.isiModal.id_role !== '' && this.isiModal.nama_jabatan !== '' && this.isiModal.id_jabatan !== '' && this.isiModal.id_group !== '' && this.isiModal.nama_user !== '' && this.isiModal.email !== '' && this.isiModal.user_update !== ''
    ) {
      this.spinner.show();
      const headers = new HttpHeaders()
        .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
      var formData = new FormData();

      if (this.foto.length !== 0) {
        formData.append(
          'foto',
          this.foto[0],
          '/' + this.path_foto.replace(/\\/g, '/')
        );
      }
      this.dataUserById.map(data => {
        formData.append('id_user', data.id_user);
        formData.append('id_role', data.id_role);
        formData.append('id_jabatan', data.id_jabatan);
        formData.append('id_group', data.id_group);
        formData.append('nama_user', data.nama_user);
        formData.append('email', data.email);
        formData.append('imei', data.imei);
        formData.append('msisdn', data.msisdn);
        formData.append('alamat_pos', data.alamat_pos2);
        formData.append('alamat_langsung', data.alamat_langsung2);
        formData.append('user_update', data.user_update);
      })

      var body_update = formData;
      this.http
        .post<any>(API_BASE_URL + 'user-module/updateUser', body_update, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Data Pengguna');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-ubah-data-pengguna']);
              this.ngOnInit()
              this.spinner.hide();
            } else if (data.result === 'ekstensi file salah') {
              this.showToastr('Ekstensi File Salah');
              this.modalService.dismissAll()
              this.spinner.hide();
            }
            else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Data Pengguna');
              this.spinner.hide();
            }
          }
        )
    } else {
      this.showToastr('Data Tidak Lengkap');
    }
  }

  deleteUser(id: string) {
    const headers = new HttpHeaders()
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get<any>(API_BASE_URL + 'user-module/deleteUser/' + id, { headers }).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }

  modalPassword(id: number, content) {
    const headers = new HttpHeaders()
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.passForm = this.fb.group({
      field1: ['', [Validators.minLength(5)]],
      field2: ['', [Validators.minLength(5)]]
    })

    this.id_user = id.toString()
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.password_baru = "";
    this.password_lama = "";
    this.http.get(API_BASE_URL + 'user-module/getUserById/' + id, { headers }).toPromise().then((res: any) => {
      res.result.map(res => this.real_password = res.password);
    })
  }

  toggleFieldTextType() { this.fieldTextType = !this.fieldTextType; }
  toggleFieldTextType2() { this.fieldTextType2 = !this.fieldTextType2; }

  changePassword() {
    const headers = new HttpHeaders()
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

    if (this.password_baru === "") {
      this.showToastr("Kata Kunci Baru Kosong")
    } else {

      const body = {
        id_user: this.id_user,
        password_lama: this.password_lama,
        password_baru: this.password_baru
      }

      if (this.passForm.get('field2').errors === null) {

        this.http.post<any>(API_BASE_URL + 'user-module/changePassword', body, { headers }).subscribe(
          (data) => {
            if (data.response_code !== '200') {
              this.showToastr("Kata Kunci Lama Salah")
              this.password_baru = "";
              this.password_lama = "";
            } else {
              this.showToastrBerhasil("Kata Kunci Berhasil Diperbarui")
              this.modalService.dismissAll()
              this.ngOnInit()
            }
          })
      } else {
        this.showToastrBerhasil("Kata Kunci Minimal 5 Karakter")
      }
    }
  }

  alertConfirmation(id: string) {
    Swal.fire({
      text: ("Apakah Anda Yakin Ingin Menghapus Pengguna?"),
      type: "warning",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK",
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteUser(id)
        this.showToastrBerhasil('Berhasil Menghapus Data Pengguna');
      }
    })
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchUbahData(event) {
    this.p = 1;
  }

  gantiPage(n: number) {
    this.sliceDataUser = this.dataUser.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }


  onFileSelected(event) {
    this.foto = event.target.files;
    if (this.foto[0]) {
      let file: File = this.foto[0];
      var idxDot = file.name.lastIndexOf('.') + 1;
      var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
      var sizeFile = file.size;
      var validationExt = this.checkExt(extFile);
      if (validationExt == false) {
        var img: any = document.querySelector('#preview img');
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg: any) {
          return function (e) {
            aImg.src = e.target.result;
          };
        })(img);
        reader.readAsDataURL(file);
        var validationSize = this.checkSize(sizeFile);
      }
    }
  }

  checkExt(extFile) {
    if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'png') {
      this.isLoadingForm = false;
      return false;
    } else {
      this.isLoadingForm = true;
      this.showToastr('Unggah Gambar Dengan Ekstensi jpg,jpeg atau png');
      return true;
    }
  }
  checkSize(sizeFile) {
    if (sizeFile <= 5242880) {
      this.isLoading = false;
      return false;
    } else {
      this.isLoading = true;
      this.showToastr('Gambar Yang Anda Unggah Lebih Dari 5MB');
      return true;
    }
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}