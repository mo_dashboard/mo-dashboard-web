import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDaftarMateriComponent } from './list-daftar-materi.component';

describe('ListDafterMateriComponent', () => {
  let component: ListDaftarMateriComponent;
  let fixture: ComponentFixture<ListDaftarMateriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDaftarMateriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDaftarMateriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
