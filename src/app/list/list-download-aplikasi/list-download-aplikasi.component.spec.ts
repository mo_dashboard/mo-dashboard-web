import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDownloadAplikasiComponent } from './list-download-aplikasi.component';

describe('ListDownloadAplikasiComponent', () => {
  let component: ListDownloadAplikasiComponent;
  let fixture: ComponentFixture<ListDownloadAplikasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDownloadAplikasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDownloadAplikasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
