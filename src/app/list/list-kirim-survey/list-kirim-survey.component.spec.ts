import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListKirimSurveyComponent } from './list-kirim-survey.component';

describe('ListKirimSurveyComponent', () => {
  let component: ListKirimSurveyComponent;
  let fixture: ComponentFixture<ListKirimSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListKirimSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListKirimSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
