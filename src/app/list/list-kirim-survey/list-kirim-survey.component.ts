
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../../services/search.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { API_BASE_URL } from '../../constants'
import { NgxSpinnerService } from 'ngx-spinner';


var moment = require('moment')
@Component({
  selector: 'app-list-kirim-survey',
  templateUrl: './list-kirim-survey.component.html',
  styleUrls: ['./list-kirim-survey.component.css']
})
export class ListKirimSurveyComponent implements OnInit {

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataSurvey: any = [];
  public lengthDataSurvey: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataSurvey: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public p: number = 1;
  public selectedItems: Array<any>;
  public dropdownList: Array<any>;
  public dropdownSettings: any;
  public selectNama: string;
  public user_create: string = localStorage.getItem('id_user');
  public dataUser: any = [];
  public idUser: string = '';
  public masterSurveyById: any = [];
  public surveyId: string;
  public id_master_survey: string;
  public user_update: string = localStorage.getItem('id_user');
  public pertanyaan: string = '';
  public nama_pertanyaan: string = '';
  public id_pertanyaan: string;
  public id_group_pertanyaan: string;
  public nama_group_pertanyaan: string;
  public dataSurveyMap: any = [];
  public groupPertanyaan: any = [];
  public updateUservey: any = [];
  public content: any;
  public idMasterSurvey: string;


  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private searchService: SearchService, private spinner: NgxSpinnerService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    this.dropdownList = [
      { item_id: 1, item_text: 'Cheese' },
      { item_id: 2, item_text: 'Tomatoes' },
      { item_id: 3, item_text: 'Mozzarella' },
      { item_id: 4, item_text: 'Mushrooms' },
      { item_id: 5, item_text: 'Pepperoni' },
      { item_id: 6, item_text: 'Onions' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id_user',
      textField: 'buatSelect',
      selectAllText: 'Pilih Semua',
      unSelectAllText: 'Tidak ada yang Dipilih',
      allowSearchFilter: true,
      searchPlaceholderText: 'Cari'
    };
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.http
      .get(API_BASE_URL + 'survey-module/getAllSurvey', { headers })
      .toPromise()
      .then((res: any) => {
        this.dataSurvey = res.result

        let dataSurveyMap = [];

        this.dataSurvey.map(function (item) {
          var existItem = dataSurveyMap.find(x => x.id_master_survey === item.id_master_survey);
          if (existItem)
            console.log()
          else
            dataSurveyMap.push(item);
        });


        for (let i = 0; i < dataSurveyMap.length; i++) {
          this.dataSurveyMap.push(dataSurveyMap[i])
        }

        var surveyMap = this.dataSurveyMap.map(item => {
          return {
            ...item,
            format_tanggal: moment(item.create_date).format('DD-MM-YYYY HH:mm')
          }
        })



        this.dataSurveyMap = surveyMap.sort((a, b) => {
          return Number(a.id_master_survey) < Number(b.id_master_survey)
            ? 1
            : Number(a.id_master_survey) > Number(b.id_master_survey) ? -1 : 0;
        });
        this.lengthDataSurvey = this.dataSurveyMap.length;
        this.jumlahPage = Math.ceil(this.lengthDataSurvey / this.pembagi);

        this.sliceDataSurvey = this.dataSurveyMap.slice(0, this.pembagi);
        this.isLoading = false;

      });

  }


  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchSurvey(event) {
    this.p = 1;
  }

  gantiPage(n: number) {
    this.sliceDataSurvey = this.dataSurvey.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }

  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  updatePertanyaan() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + this.id_master_survey, { headers }).toPromise().then((res: any) => {
      this.masterSurveyById = res.result;

      if (res.result !== "no data found") {
        this.masterSurveyById = res.result;


      }
    })
  }

  open(id: string, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    // console.log(id)
    this.selectedItems = []

    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + id, { headers }).toPromise().then((res: any) => {
      this.masterSurveyById = res.result;

      if (res.result !== "no data found") {
        this.masterSurveyById = res.result;
        this.idMasterSurvey = res.result[0].id_master_survey


      }
    })


    this.http.get(API_BASE_URL + 'survey-module/getRespondenBelumTerimaSurvey/' + id, { headers }).toPromise().then((res: any) => {
      if (res.result !== "no data found") {
        this.dataUser = res.result;
        this.idUser = res.result[0].id_user;

        this.dataUser = this.dataUser.map((item: any, index: number) => {

          return {
            ...item,
            buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
          }
        })
      }
    })
  }

  editPertanyaan(item: any, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
    // console.log('item pertanyaan',item)
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.content = content

    this.pertanyaan = item.pertanyaan
    this.nama_group_pertanyaan = item.nama_group_pertanyaan
    this.id_group_pertanyaan = item.id_group_pertanyaan
    this.id_master_survey = item.id_master_survey
    this.id_pertanyaan = item.id_pertanyaan

    this.http.get(API_BASE_URL +
      'survey-module/getAllGroupPertanyaan', { headers }).toPromise().then((res: any) => {
        this.groupPertanyaan = res.result;
      })

  }

  ubahPertanyaan(modal) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    const body = {
      id_pertanyaan: this.id_pertanyaan,
      id_master_survey: this.id_master_survey,
      id_group_pertanyaan: this.id_group_pertanyaan,
      pertanyaan: this.pertanyaan,
      user_update: localStorage.getItem('id_user')
    }
    this.http.post(API_BASE_URL +
      'survey-module/updateSurvey', body, { headers }).toPromise().then((res: any) => {
        if (res.response_code === '200') {
          this.showToastrBerhasil('Pertanyaan Berhasil Diperbarui');
          this.updatePertanyaan()
        }
      })
  }

  kirimSurvey(idMasterSurvey) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    this.user_create = localStorage.getItem('id_user')

    var body_responden = this.selectedItems.map((item: any, index: number) => {
      return {
        ...item,
        id_user: item.id_user,
      }
    })

    const body = {
      id_master_survey: idMasterSurvey,
      penerima: body_responden,
      user_create: this.user_create
    }

    this.spinner.show();
    this.http
      .post<any>(API_BASE_URL + 'survey-module/sendSurvey', body, { headers })
      .subscribe(
        (data) => {
          // console.log('send survey', data);
          // console.log(data)
          if (data.response_code === '200') {
            this.showToastrBerhasil('Berhasil Kirim Survey');
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-kirim-survey']);

          } else {
            this.showToastr(data.result);
          }
        },
        (error) => this.showToastr('Terjadi Kesalahan Saat Mengirim Survey')
      )
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}

