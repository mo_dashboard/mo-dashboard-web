import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUndanganUserComponent } from './list-undangan-user.component';

describe('ListUndanganUserComponent', () => {
  let component: ListUndanganUserComponent;
  let fixture: ComponentFixture<ListUndanganUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUndanganUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUndanganUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});