import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRisalahUserComponent } from './list-risalah-user.component';

describe('ListRisalahUserComponent', () => {
  let component: ListRisalahUserComponent;
  let fixture: ComponentFixture<ListRisalahUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRisalahUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRisalahUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
