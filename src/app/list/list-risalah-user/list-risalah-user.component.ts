import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { API_BASE_URL } from '../../constants';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
	selector: 'app-list-risalah-user',
	templateUrl: './list-risalah-user.component.html',
	styleUrls: ['./list-risalah-user.component.css']
})
export class ListRisalahUserComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dataUndangan: any = [];
	public lengthDataUndangan: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceDataUndangan: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isLoadingPdf: boolean = true;
	public isSearching: boolean = false;
	public closeResult: string;
	public notifRisalahById: any;
	public searchTerm: string;
	public p: number = 1;
	public risalahById: number;
	public risalahById2: any;
	public link: string;
	public pdfSrc: SafeUrl;
	public pdfPrint: SafeUrl;
	public id_risalah: string;
	public idNotifRisalah: string;
	public idUser: any;
	public timerInterval: any;

	constructor(
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private toastr: ToastrService,
		private searchService: SearchService,
		private domSanitizer: DomSanitizer
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
	}
	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		const id = localStorage.getItem('id_user');
		this.http
			.get(API_BASE_URL + 'risalah-module/getNotifRisalahByIdUser/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				if (res.result !== "no data found") {
					this.dataUndangan = res.result
					this.idNotifRisalah = res.result[0].id_notifikasi_risalah;
					if (this.dataUndangan[0].link_risalah === null) {
						let element = document.getElementsByClassName('risalahKosong');
					}
					this.lengthDataUndangan = this.dataUndangan.length;
					this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi);
					this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi);
					this.isLoading = false;
				}
			});
	}
	onSearchTermChange(): void {
		this.searchService.search(this.searchTerm).subscribe();
	}

	arrayOne(n: number): any[] {
		return Array(n);
	}

	onSearchListRisalahUser(event) {
		this.p = 1;
	}
	gantiPage(n: number) {
		this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1));
		this.awalan = false;
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	open(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.modalService.open(content, { size: 'lg' });
		this.http
			.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndangan/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.risalahById = res.result;
				this.id_risalah = id.toString();
			});
		this.http
			.get(API_BASE_URL + 'risalah-module/getRespondenDetailRisalah/' + id, { headers })
			.toPromise()
			.then((res: any) => {
				this.notifRisalahById = res.result;
			});
	}

	alertConfirmation(id: number, content) {
		Swal.fire({
			text: 'Berkas Yang Ingin Anda Lihat Bersifat Rahasia',
			type: 'warning',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.isConfirmed) {
				Swal.fire({
					text: 'Mohon Tunggu',
					timer: 1000,
					timerProgressBar: true,
					onBeforeOpen: () => {
						Swal.showLoading();
						this.timerInterval = setInterval(() => {
							const content = Swal.getContent();
						}, 100);
					},
					onClose: () => {
						clearInterval(this.timerInterval);
					}
				});
				this.openFile(id, content);
			}
		});
	}

	openFile(id: number, content) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http
			.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndanganIdUser/' + id + '/' + localStorage.getItem('id_user'), { headers })
			.toPromise()
			.then((res: any) => {
				this.risalahById2 = res.result;
				if (res.result !== 'no data found' && this.risalahById2 !== undefined) {
					this.modalService.open(content, { size: 'lg' });
					var showFile = this;
					var link = res.result[0].link_risalah;
					showFile.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
						API_BASE_URL + 'pdf_file/risalah/' + link + '#toolbar=0&navpanes=0&scrollbar=0'
					);
					showFile.pdfPrint = this.domSanitizer.bypassSecurityTrustResourceUrl(
						API_BASE_URL + 'pdf_file/risalah/' + link
					);
					showFile.isLoadingPdf = false;
				} else {
					this.showToastr('Risalah Sidang Belum Di Unggah');
				}
			});
	}

	counterDownload() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http
			.get(API_BASE_URL + 'risalah-module/getCounterDownloadRisalah/' + this.idNotifRisalah, { headers })
			.toPromise()
			.then((res: any) => {
			});
	}
	counterPrint() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http
			.get(API_BASE_URL + 'risalah-module/getCounterPrintRisalah/' + this.idNotifRisalah, { headers })
			.toPromise()
			.then((res: any) => {
			});
	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}