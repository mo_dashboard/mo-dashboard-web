import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListKirimMateriComponent } from './list-kirim-materi.component';

describe(' ListKirimMateriComponent ', () => {
  let component:  ListKirimMateriComponent ;
  let fixture: ComponentFixture< ListKirimMateriComponent >;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  ListKirimMateriComponent  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent( ListKirimMateriComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
