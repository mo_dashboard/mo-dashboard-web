import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_BASE_URL } from '../../constants';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-list-kirim-materi',
	templateUrl: './list-kirim-materi.component.html',
	styleUrls: ['./list-kirim-materi.component.css']
})
export class ListKirimMateriComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dataUndangan: any = [];
	public lengthDataUndangan: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceDataUndangan: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isSearching: boolean = false;
	public closeResult: string;
	public undanganById: number;
	public notifUndanganById: any = [];
	public selectedItems: Array<any>;
	public selectedItemUmum: Array<any>;
	public selectedItemKhusus: Array<any>;
	public selectedItemKhususContainer: {};
	public selectedItemRahasiaContainer: {};
	public selectedItemRahasia: Array<any>;
	public selectedKhusus: Array<any>;
	public selectedKhususFalse: Array<any>;
	public dropdownList: Array<any>;
	public dropdownUmumSettings: any;
	public dropdownKhususSettings: any;
	public dropdownRahasiaSettings: any;
	public selectNama: string;
	public user_create: string = localStorage.getItem('id_user');
	public dataUser: any = [];
	public dataUserUmum: any = [];
	public dataUserKhusus: any = [];
	public dataUserRahasia: any = [];
	public idUser: string = '';
	public undanganId: string;
	public isTampil: boolean = false;
	public p: number = 1;
	public materiUndanganById: any = [];
	public id_bahan_sidang: string;
	public id_user: string = '1';
	public pilihMateri: any = [];
	public idMateri: any = [];
	public bahan_sidang: string;
	public viewMateri: string;
	public id_sidang: any = [];
	public pdfSrc: SafeUrl;
	public videoShow: SafeUrl;
	public emptyTable: string;
	public length: any = [];
	public showResponden: boolean;
	public showMateri: Array<any>;
	public materiId: any;
	public materiSidang: string;
	public materiDetail: any = [];
	public idSidang: any;
	public dataKirim: any = [];
	public isShowMp4: boolean = false;
	public isNotMp4: boolean = true;
	public isShowAction: boolean = true;
	public notDetail: boolean = true;
	public hideDataUser: boolean = false;

	constructor(
		private router: Router,
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private toastr: ToastrService,
		private domSanitizer: DomSanitizer,
		private spinner: NgxSpinnerService
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
		this.dropdownList = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.showMateri = [];
		this.selectedItemUmum = [];
		this.selectedItemKhusus = [];
		this.selectedItemKhususContainer = {};
		this.selectedItemRahasiaContainer = {};

		this.dropdownUmumSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'buatSelect',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari'
		};

		this.dropdownKhususSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'buatSelect',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari'
		};

		this.dropdownRahasiaSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'buatSelect',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari',

		};
	}

	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		this.http.get(API_BASE_URL + 'undangan-module/getAllUndanganByMateri', { headers }).toPromise().then((res: any) => {
			this.dataUndangan = res.result.sort((a, b) => {
				return ((Number(a.id_undangan) < Number(b.id_undangan)) ? 1 : ((Number(a.id_undangan) > Number(b.id_undangan)) ? -1 : 0));
			})
			this.lengthDataUndangan = this.dataUndangan.length
			this.jumlahPage = Math.ceil(this.lengthDataUndangan / this.pembagi)
			this.sliceDataUndangan = this.dataUndangan.slice(0, this.pembagi)
			this.isLoading = false;

			if (this.dataUndangan[0].materi === null) {
				let element = document.getElementsByClassName('materiKosong');
			}
		})
	}

	arrayOne(n: number): any[] {
		return Array(n);
	}

	updateQuery(e: any) {
		if (e !== '') {
			this.isSearching = true;
		} else {
			this.isSearching = false;
		}
	}

	gantiPage(n: number) {
		this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1));
		this.awalan = false;
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	open(id: number, content, id2: number, materi: any, fileType) {
		//CHECK DEVICE
		let check = false;
		let window: any;
		(function (a) {
			if (
				/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
					a
				) ||
				/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
					a.substr(0, 4)
				)
			)
				check = true;
		})(navigator.userAgent || navigator.vendor || window.opera);
		if (check) {
			this.showToastr('Materi Tidak Tersedia. Gunakan aplikasi E-Kabinet Mobile Anda');
			//END CHECK DEVICE
		} else {
			const headers = new HttpHeaders()
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('Access-Control-Allow-Headers', 'Content-Type')
				.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
			this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false });
			this.dataUser = [];
			this.dataUserUmum = [];
			this.dataUserKhusus = [];
			this.selectedItemUmum = [];
			this.selectedItemKhusus = [];
			this.selectedItemRahasia = [];
			this.pilihMateri = [];
			this.showMateri = [];

			this.http
				.get(API_BASE_URL + 'undangan-module/getUndanganById/' + id, { headers })
				.toPromise()
				.then((res: any) => {
					this.undanganById = res.result;
					this.undanganId = res.result[0].id_undangan;
				});

			this.http
				.get(API_BASE_URL + 'materi-module/getMateriByIdUndangan/' + id, { headers })
				.toPromise()
				.then((res: any) => {
					// console.log('materi undangan by id',res.result)
					this.materiUndanganById = res.result;

					if (res.result !== 'no data found' && fileType !== "video/mp4") {
						this.id_sidang = this.materiUndanganById.map((materi: any) => {
							return {
								file_name: materi.file_name,
								id_bahan_sidang: materi.id_bahan_sidang,
								materi: materi.materi
							};
						});

						this.materiUndanganById.map((materi: any) => (materi = { ...materi, isChecked: false }));
						for (let i = 0; i < this.id_sidang.length; i++) {
							if (id2 !== undefined && id2 === this.id_sidang[i].id_bahan_sidang) {
								this.materiDetail = materi;
								this.viewMateri = this.materiUndanganById[i].materi;
								this.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(this.viewMateri + "#toolbar=0&navpanes=0&scrollbar=0");
								this.isShowMp4 = false;
								this.isNotMp4 = true;
							}
						}
					} else if (res.result !== 'no data found' && fileType === "video/mp4") {
						for (let i = 0; i < this.id_sidang.length; i++) {
							if (id2 !== undefined && id2 === this.id_sidang[i].id_bahan_sidang) {
								this.materiDetail = materi;
								this.viewMateri = this.materiUndanganById[i].materi;
								this.videoShow = this.domSanitizer.bypassSecurityTrustResourceUrl(this.viewMateri + "#toolbar=0&navpanes=0&scrollbar=0");
								this.isShowMp4 = true;
								this.isNotMp4 = false;
							}
						}
					}
					this.undanganId = res.result[0].id_undangan;
				});

			this.http
				.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + id, { headers })
				.toPromise()
				.then((res: any) => {
					this.notifUndanganById = res.result || [];
				});

		}
	}

	kirimMateri() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		const materi = this.pilihMateri.map((materi: any, index: number) => {
			if (materi.sifat_materi === "khusus") {
				var body_responden = this.selectedItemKhususContainer[materi.id_bahan_sidang]
			} else if (materi.sifat_materi === 'umum') {
				var body_responden = this.selectedItemUmum.map((item: any, index: number) => {
					return {
						nama_user: item.nama_jabatan + ' ( ' + item.nama_user + ')',
						id_user: item.id_user
					};
				});
			} else if (materi.sifat_materi === 'rahasia') {
				var body_responden = this.selectedItemRahasiaContainer[materi.id_bahan_sidang]

			}
			return {
				sifat_materi: materi.sifat_materi,
				id_bahan_sidang: materi.id_bahan_sidang,
				id_undangan: materi.id_undangan,
				user_create: this.user_create,
				penerima: body_responden
			};

		})
		this.spinner.show();
		this.http.post<any>(API_BASE_URL + 'materi-module/sendMateri', materi, { headers }).subscribe(
			(data) => {
				if (data.message === 'insert berhasil') {
					setTimeout(() => {
						this.showToastrBerhasil('Berhasil Kirim Materi');
						this.modalService.dismissAll();
						this.router.navigate(['/admin/list/list-kirim-materi']);
						this.spinner.hide();

					}, 5000);
				}
				else if (data.response_code !== '200') {
					this.showToastr(data.result);
					this.spinner.hide();
				}
			},
			(error) => {
				this.showToastr(error)
				this.spinner.hide();
			}
		);


	}

	onProgress() {
		this.showToastr('On Progress Bug Fixing Kirim Materi');
	}

	onChangeMateri(event, materi: any, sifat: any, id_bahan_sidang: any) {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))
		if (materi.isChecked = !(materi.isChecked)) {
			this.pilihMateri.push(materi)
			if (sifat === 'umum') {

				this.http.get(API_BASE_URL + 'undangan-module/getRespondenDetailUndangan/' + materi.id_undangan, { headers }).toPromise().then((res: any) => {

					this.dataUserUmum = res.result;
					this.dataUserUmum = this.dataUserUmum.map((item: any, index: number) => {
						return {
							...item,
							id_user: item.penerima,
							buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'

						}
					})
					materi.umum = true;
					this.selectedItemUmum = this.dataUserUmum.filter((item: any, index: number) => {
						if (item.konfirmasi_kehadiran === undefined) {
							return false; // skip
						}
						return true;
					}).map((item: any, index: number) => {

						return {
							...item,
							id_user: item.penerima,
							buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
						}

					});
				})

			}

			else {

				this.http.get(API_BASE_URL +
					'materi-module/getListUserMateriKhusus/' + materi.id_bahan_sidang, { headers }).toPromise().then((res: any) => {

						if (sifat === 'khusus') {

							this.dataUserKhusus[materi.id_bahan_sidang] = res.result;

							this.dataUserKhusus[materi.id_bahan_sidang] = this.dataUserKhusus[materi.id_bahan_sidang].map((item: any, index: number) => {

								return {
									...item,
									id_user: item.id_user,
									buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
								}
							})
							materi.khusus = true;

							this.selectedItemKhusus[materi.id_bahan_sidang] = this.dataUserKhusus[materi.id_bahan_sidang].filter((item: any, index: number) => {
								if (item.checklist == 'tidak') {
									return false; // skip
								}
								return true;
							}).map((item: any, index: number) => {
								return {
									id_bahan_sidang: id_bahan_sidang,
									id_user: item.id_user,
									buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
								}
							});


							this.selectedItemKhususContainer[materi.id_bahan_sidang] = this.selectedItemKhusus[materi.id_bahan_sidang];
							this.showMateri = this.selectedItemKhususContainer[materi.id_bahan_sidang].map((item: any) => {
								return {
									id_bahan_sidang: item.id_bahan_sidang
								}
							})

						} else if (sifat === 'rahasia') {

							this.hideDataUser = true
							this.dataUserRahasia[materi.id_bahan_sidang] = res.result;

							this.dataUserRahasia[materi.id_bahan_sidang] = this.dataUserRahasia[materi.id_bahan_sidang].map((item: any, index: number) => {

								return {
									...item,
									id_user: item.id_user,
									buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
								}

							})

							materi.rahasia = true;
							this.selectedItemRahasia[materi.id_bahan_sidang] = this.dataUserRahasia[materi.id_bahan_sidang].filter((item: any, index: number) => {
								if (item.checklist == 'tidak') {
									return false; // skip
								}
								return true;


							}).map((item: any, index: number) => {
								return {
									...item,
									id_user: item.id_user,
									buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
								}
							});


							this.selectedItemRahasiaContainer[materi.id_bahan_sidang] = this.selectedItemRahasia[materi.id_bahan_sidang]

						}
					})

			}
		}
		else {
			var index = this.pilihMateri.map(item => item.id_bahan_sidang).indexOf(id_bahan_sidang);
			if (index > -1) {
				this.pilihMateri.splice(index, 1);
			}
			return this.pilihMateri;
		}
	}
	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}

