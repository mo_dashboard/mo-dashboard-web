import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { API_BASE_URL } from '../../constants'
import * as jsPDF from 'jspdf';
import { ExcelService } from '../../services/excel.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

var moment = require('moment')

@Component({
  selector: 'app-laporan-list-survey',
  templateUrl: './list-laporan-survey.component.html',
  styleUrls: ['./list-laporan-survey.component.css']
})

export class ListLaporanSurveyComponent implements OnInit {
  @ViewChild('laporanSurvey', { static: false }) convertPdf: ElementRef;

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public notifRisalahById: any;
  public searchTerm: string;
  public p: number = 1;
  public c: number = 1;
  public surveyById: number;
  public surveyById2: any;
  public link: string;
  public pdfSrc: SafeUrl;
  public pdfPrint: SafeUrl;
  public id_risalah: string;
  public idNotifRisalah: string;
  public idUser: any;
  public timerInterval: any;
  public nama_file: any;
  public nomor_risalah: any;
  public tanggal_ttd: any;
  public penyimpanan_arsip: any;
  public status_risalah: any;
  public id_jenis_sidang: any;
  public lokasi: any;
  public waktu_awal_sidang: any;
  public waktu_akhir_sidang: any;
  public hasilRisalah: any = [];
  public hasilRisalahMap: any;
  public laporanRisalahMap: any;
  public dataJenisSidang: any = [];
  public dataSurvey: any = [];
  public dataSurveyById: any = [];
  public dataJawabanByIdSurvey: any = [];
  public nama_master_survey: string;
  public nama_survey: string;
  public listUser: any = [];
  public jawabanSurvey: any = [];
  public laporan_lokasi: string;
  public laporan_nama_file: string;
  public laporan_waktu_mulai_sidang: string;
  public laporan_status_risalah: string;
  public laporan_nomor_risalah: string;
  public laporan_tema: string;
  public laporan_nama_sidang: string;
  public laporan_tanggal_risalah: string;
  public laporan_penyimpanan_arsip: string;
  public laporan_waktu_sidang_undangan: string;
  public cariWatermarkMap: any = [];
  public cariWatermarkRes: any;
  public kode_watermark: string;
  public showAlertFilter: boolean = true;

  fileName = 'Laporan Survey.xlsx';

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }

  constructor(private spinner: NgxSpinnerService, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private searchService: SearchService, private domSanitizer: DomSanitizer, private excelService: ExcelService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }
  ngOnInit() {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.http.get(API_BASE_URL + 'master-module/getAllJenisSidang', { headers }).toPromise().then((res: any) => {
      this.dataJenisSidang = res.result

      this.isLoading = false;
    })

    this.http.get(API_BASE_URL + 'survey-module/getAllMastersurvey', { headers }).toPromise().then((res: any) => {
      this.dataSurvey = res.result

      this.isLoading = false;
    })
  }

  async changeSurvey(event, nama_survey) {
    if (event.target.value !== 0) {
      this.showAlertFilter = false
    }

    this.nama_survey = nama_survey

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    await this.spinner.show();

    await this.http.get(API_BASE_URL + 'survey-module/getSurveyByMasterId/' + event.target.value, { headers }).toPromise().then((res: any) => {
      this.dataSurveyById = res.result

      this.nama_master_survey = this.dataSurveyById[0].nama_master_survey

      this.isLoading = false;
    })

    await this.http.get(API_BASE_URL + 'user-module/getAllUser', { headers }).toPromise().then((res: any) => {
      this.listUser = res.result
    })

    await this.http.get(API_BASE_URL + 'survey-module/getJawabanSurvey/' + event.target.value, { headers }).toPromise().then((res: any) => {
      this.dataJawabanByIdSurvey = res.result
      this.isLoading = false;
    })

    this.jawabanSurvey = await this.listUser.filter((item: any, index: number) => {
      if (this.dataJawabanByIdSurvey[item.id_user] === undefined) {
        return false
      }
      return true

    }).map(item => {
      return {
        id_user: item.id_user,
        nama_user: item.nama_user,
        jawaban_survey: Array.from(this.dataJawabanByIdSurvey[item.id_user].reduce((m, t) => m.set(t.id_pertanyaan, t), new Map()).values())
      }
    })

    await this.spinner.hide()
  }

  exportAsXLSX(): void {

    var dataExcel = this.jawabanSurvey.map((item: any, index: number) => {
      return {
        'Nomor': index + 1,
        'Nama User': item.nama_user,
        'Jawaban Survey': item.jawaban_survey.map(data => {
          if (data.jawaban === null) {
            return data.jawaban = '-'
          } else {
            return data.jawaban = data.jawaban
          }
        }).toString()
      }
    })
    this.excelService.exportAsExcelFile(dataExcel, 'Laporan Survey');

  }
  onSearchTermChange(): void {
    this.searchService.search(this.searchTerm).subscribe();
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchRisalah(event) {
    this.p = 1;
  }

  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  exportAsPDF() {
    const documentDefinition = this.getDocumentDefinition();
    pdfMake.createPdf(documentDefinition).download('Laporan Risalah.pdf');
  }

  getDocumentDefinition() {
    return {
      pageOrientation: 'landscape',
      pageMargins: [10, 40, 10, 40],
      content: [
        {
          text: 'Laporan Risalah',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          table: {
            headerRows: 1,
            widths: ['20%', '12%', '8%', '8%', '8%', '10%', '10%', '5%', '9%', '10%'],
            body: [
              [
                {
                  text: 'Nama File',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'No. Risalah',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Lokasi',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Mulai Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Nama Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Tema',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Tanggal Risalah',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Arsip',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Status Risalah',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Waktu Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                }
              ],
              ...this.hasilRisalahMap.map(item => {
                return [
                  item.file_name,
                  item.nomor_risalah,
                  item.lokasi,
                  item.jam_mulai_sidang,
                  item.nama_sidang,
                  item.tema,
                  moment(item.tanggal_ttd_risalah).format('DD-MM-YYYY'),
                  item.penyimpanan_arsip,
                  item.status_risalah === '1' ? "Distribusi" : "Tidak Distribusi",
                  moment(item.waktu_sidang).format('DD-MM-YYYY HH:mm:ss')
                ];
              })
            ]
          }
        }
      ]
    }
  }

  cariWatermark(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  changeWatermark(event) {
    if (event !== "") {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

      this.http.get<any>(API_BASE_URL + 'risalah-module/searchByKodewatermark/' + event, { headers }).subscribe(
        (data) => {
          this.cariWatermarkRes = data.result

          if (data.result !== 'no data found') {
            this.cariWatermarkMap = data.result.map((item: any) => {
              return {
                ...item,
              }
            })
          }
        })
    }

  }

  changeRisalah(event) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    if (Object.prototype.toString.call((event)) === '[object Date]') {

      if (this.waktu_awal_sidang === undefined || this.waktu_awal_sidang === "") {
        this.waktu_awal_sidang = moment(event).format('YYYY-MM-DD')
        this.waktu_akhir_sidang = ""
      } else if (this.waktu_awal_sidang !== undefined) {
        this.waktu_akhir_sidang = moment(event).format('YYYY-MM-DD')
      }

    } else if (event.target.id === 'inputGroupSelect01') {
      this.lokasi = event.target.value
    } else if (event.target.id === 'inputGroupSelect02') {
      this.id_jenis_sidang = event.target.value
    } else if (event.target.id === 'inputGroupSelect03') {
      this.status_risalah = event.target.value
    }
    if (this.lokasi === "Semua Lokasi") {

      this.lokasi = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    } else if (this.id_jenis_sidang === "Semua Jenis Sidang") {

      this.id_jenis_sidang = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    } else if (this.status_risalah === "Semua Status Risalah") {

      this.status_risalah = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    }

    const cariRisalah = {
      lokasi: this.lokasi,
      id_jenis_sidang: this.id_jenis_sidang,
      waktu_awal_sidang: this.waktu_awal_sidang,
      waktu_akhir_sidang: this.waktu_akhir_sidang,
      status_risalah: this.status_risalah
    };
    this.http.post<any>(API_BASE_URL + 'risalah-module/laporanRisalah', cariRisalah, { headers }).subscribe(
      (data) => {
        this.hasilRisalah = data.result;
        if (data.result !== 'no data found') {
          this.hasilRisalahMap = data.result
          this.laporanRisalahMap = data.result.map((item: any) => {
            return {
              Nama_File: item.file_name,
              Nomor_Risalah: item.nomor_risalah,
              Lokasi: item.lokasi,
              Waktu_Mulai_Sidang: item.jam_mulai_sidang,
              Nama_Sidang: item.nama_sidang,
              Tema: item.tema,
              Tanggal_Risalah: moment(item.tanggal_ttd_risalah).format('DD-MM-YYYY'),
              Penyimpanan_Arsip: item.penyimpanan_arsip,
              Status_Risalah: item.status_risalah === '1' ? "Distribusi" : "Tidak Distribusi",
              Waktu_Sidang_Undangan: moment(item.waktu_sidang).format('DD-MM-YYYY HH:mm:ss')

            }
          })
        }
      }
    );
  }
  alertConfirmation(id: number, content) {
    Swal.fire({
      text: ("Berkas Yang Ingin Anda Lihat Bersifat Rahasia"),
      type: "warning",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK",
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          text: 'Mohon Tunggu',
          timer: 1000,
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            this.timerInterval = setInterval(() => {
              const content = Swal.getContent()
            }, 100)
          },
          onClose: () => {
            clearInterval(this.timerInterval)
          }
        })
        this.openFile(id, content)
      }
    })
  }

  openFile(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndangan/' + id, { headers }).toPromise().then((res: any) => {
      this.surveyById2 = res.result
      if (res.result !== 'no data found' && this.surveyById2 !== undefined) {
        this.modalService.open(content, { size: 'lg' })
        var showFile = this;
        var link = res.result[0].file_name;
        showFile.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah/' + link + "#toolbar=0&navpanes=0&scrollbar=0"
        );
        showFile.pdfPrint = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah/' + link
        );
        showFile.isLoadingPdf = false;
      } else {
        this.showToastr('Risalah Sidang Belum Di Unggah');
      }
    });
    this.idUser = localStorage.getItem('id_user');
    this.http
      .get(API_BASE_URL + 'risalah-module/getNotifRisalahByIdUser/' + this.idUser, { headers })
      .toPromise()
      .then((res: any) => {
        this.idNotifRisalah = res.result[0].id_notifikasi_risalah;
      });
  }

  counterDownload() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterDownloadRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }
  counterPrint() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterPrintRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }


  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}

