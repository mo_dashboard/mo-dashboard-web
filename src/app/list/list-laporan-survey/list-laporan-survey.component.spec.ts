import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLaporanSurveyComponent } from './list-laporan-survey.component';

describe('ListLaporanSurveyComponent', () => {
  let component: ListLaporanSurveyComponent;
  let fixture: ComponentFixture<ListLaporanSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLaporanSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLaporanSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
