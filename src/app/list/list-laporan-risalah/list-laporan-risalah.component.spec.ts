import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLaporanRisalahComponent } from './list-laporan-risalah.component';

describe('ListLaporanRisalahComponent', () => {
  let component: ListLaporanRisalahComponent;
  let fixture: ComponentFixture<ListLaporanRisalahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLaporanRisalahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLaporanRisalahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
