import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { tap } from 'rxjs/operators';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { API_BASE_URL } from '../../constants'
import { ExcelService } from '../../services/excel.service';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

var moment = require('moment')

@Component({
  selector: 'app-laporan-list-risalah',
  templateUrl: './list-laporan-risalah.component.html',
  styleUrls: ['./list-laporan-risalah.component.css']
})

export class ListLaporanRisalahComponent implements OnInit {
  @ViewChild('laporanRisalah', { static: false }) convertPdf: ElementRef;

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataUndangan: any = [];
  public lengthDataUndangan: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceDataUndangan: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isLoadingPdf: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;
  public notifRisalahById: any;
  public searchTerm: string;
  public p: number = 1;
  public risalahById: number;
  public risalahById2: any;
  public link: string;
  public pdfSrc: SafeUrl;
  public pdfPrint: SafeUrl;
  public id_risalah: string;
  public idNotifRisalah: string;
  public idUser: any;
  public timerInterval: any;
  public nama_file: any;
  public nomor_risalah: any;
  public tanggal_ttd: any;
  public penyimpanan_arsip: any;
  public status_risalah: any;
  public id_jenis_sidang: any;
  public lokasi: any;
  public lokasi_alt: any;
  public waktu_awal_sidang: any;
  public waktu_akhir_sidang: any;
  public hasilRisalah: any = [];
  public hasilRisalahMap: any;
  public laporanRisalahMap: any;
  public dataJenisSidang: any = [];
  public dataLokasi: any = [];
  public laporan_lokasi: string;
  public laporan_nama_file: string;
  public laporan_waktu_mulai_sidang: string;
  public laporan_status_risalah: string;
  public laporan_nomor_risalah: string;
  public laporan_tema: string;
  public laporan_nama_sidang: string;
  public laporan_tanggal_risalah: string;
  public laporan_penyimpanan_arsip: string;
  public laporan_waktu_sidang_undangan: string;
  public cariWatermarkMap: any = [];
  public cariWatermarkRes: any;
  public kode_watermark: string;
  public detailLaporan: any = [];

  constructor(private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private searchService: SearchService, private domSanitizer: DomSanitizer, private excelService: ExcelService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }
  ngOnInit() {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    this.http.get(API_BASE_URL + 'master-module/getAllJenisSidang', { headers }).toPromise().then((res: any) => {
      this.dataJenisSidang = res.result

      this.isLoading = false;
    })

    this.http.get(API_BASE_URL + 'master-module/getAllLokasi', { headers }).toPromise().then((res: any) => {
      this.dataLokasi = res.result

      this.isLoading = false;
    })
  }


  onSearchTermChange(): void {
    this.searchService.search(this.searchTerm).subscribe();
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchRisalah(event) {
    this.p = 1;
  }

  gantiPage(n: number) {
    this.sliceDataUndangan = this.dataUndangan.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  exportAsXLSX(): void {

    this.excelService.exportAsExcelFile(this.laporanRisalahMap, 'Laporan Pengantar Presiden');

  }

  exportAsPDF() {
    // console.log(pdfMake);
    const documentDefinition = this.getDocumentDefinition();

    pdfMake.createPdf(documentDefinition).download('Laporan Pengantar Presiden.pdf');
  }

  getDocumentDefinition() {
    return {
      pageOrientation: 'landscape',
      pageMargins: [10, 40, 10, 40],
      content: [
        {
          text: 'Laporan Pengantar Presiden',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          table: {
            headerRows: 1,
            widths: ['8%', '9%', '7%', '7%', '13%', '8%', '9%', '5%', '9%', '9%', '10%', '7%'],
            body: [
              [
                {
                  text: 'Nama Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Waktu Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Mulai Sidang',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Lokasi',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Tema',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Status Pengantar',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'No.Pengantar',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Arsip',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Tanggal Pengantar',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Waktu Distribusi',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Nama File',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                },
                {
                  text: 'Jumlah Penerima',
                  fontSize: 13,
                  bold: true,
                  alignment: 'center'
                }
              ],
              ...this.hasilRisalahMap.map(item => {
                return [
                  item.Nama_Sidang,
                  item.Waktu_Sidang_Undangan,
                  item.Waktu_Mulai_Sidang,
                  item.Lokasi,
                  item.Tema,
                  item.Status_Risalah,
                  item.Nomor_Risalah,
                  item.Penyimpanan_Arsip,
                  item.Tanggal_Risalah,
                  item.Waktu_Distribusi,
                  item.Nama_File,
                  item.Jumlah_Penerima
                ];
              })
            ]
          }
        }
      ]
    }
  }

  cariWatermark(content) {
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false });
  }

  changeWatermark(event) {
    if (event !== "") {

      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

      this.http.get<any>(API_BASE_URL + 'risalah-module/searchByKodewatermark/' + event, { headers }).subscribe(
        (data) => {
          this.cariWatermarkRes = data.result

          if (data.result !== 'no data found') {
            this.cariWatermarkMap = data.result.map((item: any) => {
              return {
                ...item,
              }
            })
          }
        })
    }

  }

  changeRisalah(event) {

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

    if (typeof event === 'string') {
      this.penyimpanan_arsip = event
    } else if (Object.prototype.toString.call((event)) === '[object Date]') {

      if (this.waktu_awal_sidang === undefined || this.waktu_awal_sidang === "") {
        this.waktu_awal_sidang = moment(event).format('YYYY-MM-DD')
        this.waktu_akhir_sidang = ""
      } else if (this.waktu_awal_sidang !== undefined) {
        this.waktu_akhir_sidang = moment(event).format('YYYY-MM-DD')
      }

    } else if (event.target.id === 'inputGroupSelect01') {
      this.lokasi = event.target.value
    } else if (event.target.id === 'inputGroupSelect02') {
      this.id_jenis_sidang = event.target.value
    } else if (event.target.id === 'inputGroupSelect03') {
      this.status_risalah = event.target.value
    }
    if (this.lokasi === "Semua Lokasi") {

      this.lokasi = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    } else if (this.id_jenis_sidang === "Semua Jenis Sidang") {

      this.id_jenis_sidang = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    } else if (this.status_risalah === "Semua Status Pengantar") {

      this.status_risalah = ""
      this.waktu_awal_sidang = ""
      this.waktu_akhir_sidang = ""
    }

    const cariRisalah = {
      lokasi: this.lokasi,
      id_jenis_sidang: this.id_jenis_sidang,
      waktu_awal_sidang: this.waktu_awal_sidang,
      waktu_akhir_sidang: this.waktu_akhir_sidang,
      status_risalah: this.status_risalah,
      penyimpanan_arsip: this.penyimpanan_arsip
    };
    this.http.post<any>(API_BASE_URL + 'risalah-module/laporanRisalah', cariRisalah, { headers }).subscribe(
      (data) => {
        this.hasilRisalah = data.result;
        if (data.result !== 'no data found') {
          this.hasilRisalahMap = data.result.map((item: any) => {
            return {
              Id_Risalah: item.id_risalah,
              Nama_Sidang: item.nama_sidang,
              Waktu_Sidang_Undangan: moment(item.waktu_sidang).format('DD-MM-YYYY HH:mm:ss'),
              Waktu_Mulai_Sidang: item.jam_mulai_sidang,
              Lokasi: item.lokasi === 'Lainnya' ? item.lokasi_alt : item.lokasi,
              Tema: item.tema,
              Status_Risalah: item.status_risalah === '1' ? "Distribusi" : "Tidak Distribusi",
              Nomor_Risalah: item.nomor_risalah,
              Penyimpanan_Arsip: item.penyimpanan_arsip,
              Tanggal_Risalah: moment(item.tanggal_ttd_risalah).format('DD-MM-YYYY'),
              Nama_File: item.file_name,
              Waktu_Distribusi: item.waktu_distribusi,
              Jumlah_Penerima: item.jumlah_penerima
            }
          })
          this.laporanRisalahMap = data.result.map((item: any) => {
            return {
              'Nama Sidang': item.nama_sidang,
              'Waktu Sidang': moment(item.waktu_sidang).format('DD-MM-YYYY HH:mm:ss'),
              'Mulai Sidang': item.jam_mulai_sidang,
              'Lokasi': item.lokasi === 'Lainnya' ? item.lokasi_alt : item.lokasi,
              'Tema': item.tema,
              'Status Pengantar': item.status_risalah === '1' ? "Distribusi" : "Tidak Distribusi",
              'No.Pengantar': item.nomor_risalah,
              'Arsip': item.penyimpanan_arsip,
              'Tanggal Pengantar': moment(item.tanggal_ttd_risalah).format('DD-MM-YYYY'),
              'Waktu Distribusi': item.waktu_distribusi,
              'Nama File': item.file_name,
              'Jumlah Penerima': item.jumlah_penerima

            }
          })
        }
      }
    );
  }

  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    this.http
      .get(API_BASE_URL + 'risalah-module/getListRisalahTerkirim/' + id, { headers })
      .toPromise()
      .then((res: any) => {
        this.detailLaporan = res.result
        if (res.result !== 'no data found') {
          this.detailLaporan = res.result
        }
      });
  }

  alertConfirmation(id: number, content) {
    Swal.fire({
      text: ("Berkas Yang Ingin Anda Lihat Bersifat Rahasia"),
      type: "warning",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK",
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          text: 'Mohon Tunggu',
          timer: 1000,
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            this.timerInterval = setInterval(() => {
              const content = Swal.getContent()
            }, 100)
          },
          onClose: () => {
            clearInterval(this.timerInterval)
          }
        })
        this.openFile(id, content)
      }
    })
  }

  openFile(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get(API_BASE_URL + 'risalah-module/getRisalahByIdUndangan/' + id, { headers }).toPromise().then((res: any) => {
      this.risalahById2 = res.result
      if (res.result !== 'no data found' && this.risalahById2 !== undefined) {
        this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
        var showFile = this;
        var link = res.result[0].file_name;
        showFile.pdfSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah/' + link + "#toolbar=0&navpanes=0&scrollbar=0"
        );
        showFile.pdfPrint = this.domSanitizer.bypassSecurityTrustResourceUrl(
          API_BASE_URL + 'pdf_file/risalah/' + link
        );
        showFile.isLoadingPdf = false;
      } else {
        this.showToastr('Risalah Sidang Belum Di Unggah');
      }
    });
    this.idUser = localStorage.getItem('id_user');
    this.http
      .get(API_BASE_URL + 'risalah-module/getNotifRisalahByIdUser/' + this.idUser, { headers })
      .toPromise()
      .then((res: any) => {
        this.idNotifRisalah = res.result[0].id_notifikasi_risalah;
      });
  }

  counterDownload() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterDownloadRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }
  counterPrint() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http
      .get(
        API_BASE_URL + 'risalah-module/getCounterPrintRisalah/' +
        this.idNotifRisalah,
        { headers }
      )
      .toPromise()
      .then((res: any) => {
      });
  }


  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}

