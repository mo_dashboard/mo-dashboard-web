
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { query } from '@angular/animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { data } from 'jquery';
import { Router } from '@angular/router';
import { API_BASE_URL } from '../../constants'
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Component({
  selector: 'app-list-lokasi',
  templateUrl: './list-lokasi.component.html',
  styleUrls: ['./list-lokasi.component.css']
})
export class ListLokasiComponent implements OnInit {
  [x: string]: any;

  public visitorsOptions: EChartOption = {};
  public visitsOptions: EChartOption = {};
  public sidebarVisible: boolean = true;
  public dataLokasi: any = [];
  public lengthDataLokasi: number = 0;
  public pembagi: number = 5;
  public jumlahPage: number = 0;
  public sliceLokasi: any = [];
  public awalan: boolean = true;
  public isLoading: boolean = true;
  public isSearching: boolean = false;
  public closeResult: string;

  public id_lokasi: string;
  public nama_lokasi: string = '';
  public lokasiById: Array<any>;
  public user_create: string = localStorage.getItem('id_user');
  public user_update: string = localStorage.getItem('id_user');
  public p: number = 1;

  constructor(private router: Router, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService) {
    this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
    this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
  }

  ngOnInit() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.http.get(API_BASE_URL + 'master-module/getAllLokasi', { headers }).toPromise().then((res: any) => {
      this.dataLokasi = res.result
      this.lengthDataLokasi = this.dataLokasi.length
      this.jumlahPage = Math.ceil(this.lengthDataLokasi / this.pembagi)
      this.sliceLokasi = this.dataLokasi.slice(0, this.pembagi)
      this.isLoading = false;
    })
  }

  open(id: number, content) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    this.id_lokasi = id.toString()
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })

    this.http.get(API_BASE_URL + 'master-module/getLokasiById/' + id, { headers }).toPromise().then((res: any) => {
      this.lokasiById = res.result
      this.lokasiById.map(res => this.nama_lokasi = res.nama_lokasi)
    })
  }

  tambahModal(content) {
    this.nama_lokasi = "";
    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
  }

  tambahLokasi(content) {

    this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
    const body = {
      nama_lokasi: this.nama_lokasi,
      user_create: this.user_create
    }
    this.http
      .post<any>(API_BASE_URL + 'master-module/createLokasi', body, { headers })
      .subscribe(
        (data: any) => {
          if (data.message === "insert berhasil") {
            this.showToastrBerhasil('Berhasil Menambah Lokasi');
            this.nama_lokasi = "";
            this.modalService.dismissAll()
            this.router.navigate(['/admin/list/list-lokasi']);
            this.ngOnInit()
          } else {
            this.nama_lokasi = "";
            this.showToastr('Terjadi Kesalahan Saat Menambah Lokasi');
          }
        }
      )
  }

  tutupModal() {
    this.nama_lokasi = "";
    this.modalService.dismissAll();
  }

  ubahLokasi() {
    if (this.nama_lokasi !== '') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
      const body = {
        id_lokasi: this.id_lokasi,
        nama_lokasi: this.nama_lokasi,
        user_update: this.user_update
      }
      this.http
        .post<any>(API_BASE_URL + 'master-module/updateLokasi/', body, { headers })
        .subscribe(
          (data: any) => {
            if (data.message === "data berhasil diupdate") {
              this.showToastrBerhasil('Berhasil Mengubah Lokasi');
              this.modalService.dismissAll()
              this.router.navigate(['/admin/list/list-lokasi']);
              this.ngOnInit()
            } else {
              this.showToastr('Terjadi Kesalahan Saat Mengubah Lokasi');
            }
          }
        )
    } else {
      this.showToastr('Terjadi Kesalahan Saat Mengubah Lokasi ');
    }
  }

  deleteLokasi(id: string) {
    this.http.get<any>(API_BASE_URL + 'master-module/deleteLokasi/' + id).subscribe(
      (data) => {
        this.ngOnInit()
      })
  }

  alertConfirmation(id: string) {
    Swal.fire({
      text: ("Apakah Anda Yakin Ingin Menghapus Lokasi Sidang?"),
      type: "warning",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK",
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteLokasi(id)
        this.showToastrBerhasil('Berhasil Menghapus Lokasi Sidang');
      }
    })
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onSearchLokasi(event) {
    this.p = 1;
  }
  gantiPage(n: number) {
    this.sliceLokasi = this.dataLokasi.slice(this.pembagi * n, this.pembagi * (n + 1))
    this.awalan = false
  }
  toggleFullWidth() {
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
    this.cdr.detectChanges();
  }

  showToastrBerhasil(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  showToastr(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  onFileSelected(event) {
    this.nama_lokasi = event.target.files;
  }

  loadLineChartOptions(data, color) {
    let chartOption: EChartOption;
    let xAxisData: Array<any> = new Array<any>();

    data.forEach(element => {
      xAxisData.push("");
    });

    return chartOption = {
      xAxis: {
        type: 'category',
        show: false,
        data: xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        type: 'value',
        show: false
      },
      tooltip: {
        trigger: 'axis',
        formatter: function (params, ticket, callback) {
          return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
        }
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '0%',
        top: '0%',
        containLabel: false
      },
      series: [{
        data: data,
        type: 'line',
        showSymbol: false,
        symbolSize: 1,
        lineStyle: {
          color: color,
          width: 1
        }
      }]
    };
  }

}