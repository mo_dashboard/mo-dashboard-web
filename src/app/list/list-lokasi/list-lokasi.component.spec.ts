import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLokasiComponent } from './list-lokasi.component';

describe('ListLokasiComponent', () => {
  let component: ListLokasiComponent;
  let fixture: ComponentFixture<ListLokasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLokasiComponent
   ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLokasiComponent
  );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
