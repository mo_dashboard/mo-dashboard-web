import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable({
	providedIn: 'root'
})
export class EventService {

	public undangan: string;

	public getEvents(): Observable<any> {
		const dateObj = new Date();
		const yearMonth = dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
		let data: any = [
		{
			title: 'Rapat Terbatas',
			start: '2020-6-07',
			color: '#dc3545'
		},
		{
			title: 'Sidang Kabinet',
			start: '2020-6-08',
			color: '#17a2b8'
		},
		{
			title: 'Sidang Kabinet',
			start: '2020-6-09',
			color: '#17a2b8'
		},
		{
			title: 'Sidang Kabinet',
			start: '2020-6-12',
			color: '#17a2b8'
		},
		{
			title: 'Sidang Kabinet',
			start: '2020-6-15',
			color: '#17a2b8'
		},
		{
			title: 'Sidang Kabinet',
			start: '2020-6-17',
			color: '#17a2b8'
		},
		{
			title: 'Rapat Terbatas',
			start: '2020-6-20',
			color: '#dc3545'
		}
		// {
		// 	id: 999,
		// 	title: 'Repeating Event',
		// 	start: '2020-6-07'16:00:00',
		// 	color: '#343a40'
		// },
		// {
		// 	id: 999,
		// 	title: 'Repeating Event',
		// 	start: '2020-6-07'16:00:00'
		// },
		// {
		// 	title: 'Conference',
		// 	start: '2020-6-07',
		// 	end: yearMonth + '-13',
		// 	color: '#343a40'
		// },
		// {
		// 	title: 'Meeting',
		// 	start: '2020-6-07'10:30:00',
		// 	end: yearMonth + '-12T12:30:00'
		// },
		// {
		// 	title: 'Lunch',
		// 	start: '2020-6-07'12:00:00'
		// },
		// {
		// 	title: 'Meeting',
		// 	start: '2020-6-07'14:30:00'
		// },
		// {
		// 	title: 'Happy Hour',
		// 	start: '2020-6-07'17:30:00'
		// },
		// {
		// 	title: 'Dinner',
		// 	start: '2020-6-07'20:00:00'
		// },
		// {
		// 	title: 'Birthday Party',
		// 	start: '2020-6-07'07:00:00'
		// },
		// {
		// 	title: 'Click for Google',
		// 	url: 'http://google.com/',
		// 	start: '2020-6-07',
		// 	color: '#007bff'
		// }
	];
		// console.log(data)
		return of(data);
	}
}
