import { Injectable, Inject } from '@angular/core'
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http'

// import { download, Download } from './download'
// import { map } from 'rxjs/operators'
// import { Observable } from 'rxjs'
// import { SAVER, Saver } from './saver.provider'

@Injectable({providedIn: 'root'})
export class DownloadService {

    constructor(
        private http: HttpClient,
        // @Inject(SAVER) private save: Saver
    ) {
    }

    downloadFile(url: string) {
        // console.log('url service', url)
        // console.log('headers', headers)
        // const headers = new HttpHeaders()
        // .set('Content-Type', 'application/json')
        // .set('Accept', 'application/json')
        // .set('Access-Control-Allow-Headers', 'Content-Type')
        // .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

        return this.http.get(url, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Control-Allow-Headers': 'Content-Type',
                // 'Authorization': 'Basic ' + btoa('ekab40:Ek4b#116')
            }), 
            responseType: 'blob'
        })
    }

    // download(url: string, filename?: string): Observable<Download> {
    //     return this.http.get(url, {
    //     reportProgress: true,
    //     observe: 'events',
    //     responseType: 'blob'
    //     }).pipe(download(blob => this.save(blob, filename)))
    // }


    // blob(url: string, filename?: string): Observable<Blob> {
    //     return this.http.get(url, {
    //     responseType: 'blob'
    //     })
    // }
}