export interface Undangan {
    id_jenis_sidang : string,
	pimpinan_sidang : string,
	waktu_sidang : string,
	tema : string,
	lokasi : string,
	nomor_undangan : string,
	sifat : string,
	status : string,
	pembuat_undangan : string,
	user_create : string
}