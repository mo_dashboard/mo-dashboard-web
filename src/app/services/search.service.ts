import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject, of } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Undangan } from './undangan';
import 'rxjs/add/observable/of';
import { API_BASE_URL } from '../constants'

@Injectable({
	providedIn: 'root'
})
export class SearchService {

	constructor(private httpClient: HttpClient) {
	}
	private apps: Undangan[];
	private filteredApps$: Subject<Undangan[]> = 
    new ReplaySubject<Undangan[]>(1);

	getSearchResults(): Observable<Undangan[]> {
		return this.filteredApps$.asObservable();
	}

	search(searchTerm: string): Observable<void> {
		return this.fetchApps().pipe(
		tap((apps: Undangan[]) => {
			apps = apps.filter(app => 
				app.pimpinan_sidang.toLowerCase().includes(searchTerm)
				);
			this.filteredApps$.next(apps);
		}),
		map(() => void 0)
		);
	}

	private fetchApps(): Observable<Undangan[]> {
		// return cached apps
		if (this.apps) {
		return of(this.apps);
		}

		// fetch and cache apps
		return this.httpClient.get(API_BASE_URL+'user-module/getAllUser').pipe(
		tap((apps: Undangan[]) => this.apps = apps)
		);
	}
}
