import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsEditStatusUndanganComponent } from './forms-edit-status-undangan.component';

describe('FormsTambahUndanganComponent', () => {
  let component: FormsEditStatusUndanganComponent;
  let fixture: ComponentFixture<FormsEditStatusUndanganComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsEditStatusUndanganComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsEditStatusUndanganComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
