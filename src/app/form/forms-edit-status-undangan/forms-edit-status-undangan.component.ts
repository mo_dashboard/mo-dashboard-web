import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SidebarService } from '../../services/sidebar.service';
import { ToastrService } from 'ngx-toastr';
import { API_BASE_URL } from '../../constants'


@Component({
	selector: 'app-forms-edit-status-undangan',
	templateUrl: './forms-edit-status-undangan.component.html',
	styleUrls: [ './forms-edit-status-undangan.component.css' ]
})
export class FormsEditStatusUndanganComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dropdownList: Array<any>;
	public selectedItems: Array<any>;
	public dropdownSettings: any;
	public data: any = {};

	public id_jenis_sidang: string = '1';
	public pimpinan_sidang: string = '11';
	public waktu_sidang: string = '';
	public tema: string = '';
	public lokasi: string = '';
	public nomor_undangan: string = '';
	public sifat: string = 'Segera';
	public perihal: string = '-';
	public status: string = 'Terjadwal';
	public selectedDataSifat: string = '';
	public selectedDataStatus: string = '';
	public pembuat_undangan: string = '3';
	public catatan_undangan: string = '-';
	public user_create: string = '1';
	public berkas_undangan: any = [];
	public path_berkas_undangan: string = '';
	public dataUser: any;

	constructor(
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private toastr: ToastrService
	) {
		this.visitorsOptions = this.loadLineChartOptions([ 3, 5, 1, 6, 5, 4, 8, 3 ], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([ 4, 6, 3, 2, 5, 6, 5, 4 ], '#f4516c');
		this.dropdownList = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.selectedItems = [];
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'nama_user',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText : 'Cari'
		};
	}

	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type');
		this.http.get(API_BASE_URL+'user-module/getAllUser', { headers }).toPromise().then((res: any) => {
			// console.log('get all user', res.result);
			this.dataUser = res.result;
		});
	}
	tambahUndangan() {
		// console.log('waktu sidang',this.waktu_sidang)
		// console.log('waktu tema',this.tema)
		// console.log('lokasi',this.lokasi)
		// console.log('nomor undangan',this.nomor_undangan)
		// console.log('status',this.status)
		// console.log('berkas undangan',this.berkas_undangan)

		if (
			(this.waktu_sidang !== '') &&
			(this.tema !== '' ) &&
			(this.lokasi !== '') &&
			(this.nomor_undangan !== '') &&
			(this.status !== '' ) &&
			(this.berkas_undangan !== []) &&
			(this.berkas_undangan[0] !== undefined)
			 
		) {
			const headers = new HttpHeaders()
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('Access-Control-Allow-Headers', 'Content-Type')
				.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
			const body = {
				id_jenis_sidang: this.id_jenis_sidang,
				pimpinan_sidang: this.pimpinan_sidang,
				waktu_sidang: this.waktu_sidang,
				tema: this.tema,
				lokasi: this.lokasi,
				nomor_undangan: this.nomor_undangan,
				sifat: this.sifat,
				status: this.status,
				pembuat_undangan: this.pembuat_undangan,
				user_create: this.user_create
			};
			// console.log(body)

			this.http
				.post<any>(API_BASE_URL+'undangan-module/createUndangan', body, { headers })
				.subscribe(
					(data: any) => {
						// console.log('tester', data);

						if (data.message === 'insert berhasil') {
							var formdata = new FormData();
							formdata.append(
								'undangan',
								this.berkas_undangan[0],
								'/' + this.path_berkas_undangan.replace(/\\/g, '/')
							);
							formdata.append('id_undangan', data.id_undangan);
							formdata.append('user_create', this.user_create);
							var body_upload = formdata;
							// console.log(body_upload);

							this.http
								.post<any>(API_BASE_URL+'undangan-module/uploadUndangan', body_upload,{ headers})
								.subscribe(
									(data) => {
										// console.log('tester', data);
										if (data.message === 'upload berhasil') {
											this.showToastrBerhasil('Berhasil Membuat Undangan');
										} else {
											this.showToastr('Terjadi Kesalahan Saat Membuat Undangan');
										}
									},
									(error) => this.showToastr('Terjadi Kesalahan Saat Membuat Undangan')
								);
						} else {
							this.showToastr('Terjadi Kesalahan Saat Membuat Undangan ');
						}
					},
					(error) => this.showToastr('Terjadi Kesalahan Saat Membuat Undangan ')
				);
		} else {
			this.showToastr('Data Tidak Lengkap');
		}
	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	onFileSelected(event) {
		// console.log(event.target.files);
		this.berkas_undangan = event.target.files;
	}
	onSubmit(isValid: Boolean) {
		if (isValid) {
			// Logic to add/update data here.
		}
	}

	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function(params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}