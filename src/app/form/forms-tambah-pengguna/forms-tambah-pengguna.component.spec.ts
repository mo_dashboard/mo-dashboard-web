import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsTambahPenggunaComponent } from './forms-tambah-pengguna.component';

describe('FormsTambahPenggunaComponent', () => {
  let component: FormsTambahPenggunaComponent;
  let fixture: ComponentFixture<FormsTambahPenggunaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsTambahPenggunaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsTambahPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
