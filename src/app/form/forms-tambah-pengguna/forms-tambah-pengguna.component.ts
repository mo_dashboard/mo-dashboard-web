import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SidebarService } from '../../services/sidebar.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Select2OptionData } from 'ng-select2';
import { API_BASE_URL } from '../../constants';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
	selector: 'app-forms-tambah-pengguna',
	templateUrl: './forms-tambah-pengguna.component.html',
	styleUrls: ['./forms-tambah-pengguna.component.css']
})
export class FormsTambahPenggunaComponent implements OnInit {
	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dropdownList: Array<any>;
	public selectedItems: Array<any>;
	public dropdownSettings: any;
	public data: any = {};
	public id_role: string = '1';
	public id_jabatan: string = '3';
	public id_group: string = '';
	public username: string = '';
	public nama_user: string = '';
	public password: string = '';
	public msisdn: string = '';
	public imei: string = '';
	public email: string = '';
	public alamat_pos: string = '';
	public alamat_langsung: string = '';
	public user_create: string = '';
	public confirm_password: string = '';
	public selectedDataRole: string = '';
	public selectedDataJabatan: string = '';
	public selectedDataGroup: string = '';
	public dataRole: any;
	public dataJabatan: any;
	public dataGrup: Array<Select2OptionData>;
	public dataGroup: any;
	public foto: any = [];
	public path_foto: string = '';
	public path_foto_fix: SafeUrl;
	public fieldTextType: boolean;
	public fieldTextType1: boolean;
	public isLoading: boolean = false;
	public passForm: FormGroup;
	public checkEmail1: boolean;
	public checkEmail2: boolean;

	constructor(
		private router: Router,
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private toastr: ToastrService,
		private sanitizer: DomSanitizer,
		private fb: FormBuilder
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
		this.dropdownList = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.selectedItems = [
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' }
		];
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'item_id',
			textField: 'item_text',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			allowSearchFilter: true
		};
	}
	sanitizeImageUrl(imageUrl: string): SafeUrl {
		return this.sanitizer.bypassSecurityTrustUrl(imageUrl);
	}

	toggleFieldTextType() {
		this.fieldTextType = !this.fieldTextType;
	}
	toggleFieldTextType1() {
		this.fieldTextType1 = !this.fieldTextType1;
	}
	ngOnInit() {

		this.passForm = this.fb.group({
			field1: ['', [Validators.minLength(5)]],
			field2: ['', [Validators.minLength(5)]]
		})
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http
			.get(API_BASE_URL + 'master-module/getAllRole', { headers })
			.toPromise()
			.then((res: any) => {
				this.dataRole = res.result;
			});
		this.http
			.get(API_BASE_URL + 'master-module/getAllJabatan', { headers })
			.toPromise()
			.then((res: any) => {
				this.dataJabatan = res.result;
			});
		this.http
			.get(API_BASE_URL + 'master-module/getAllGroup', { headers })
			.toPromise()
			.then((res: any) => {
				this.dataGroup = res.result;
			});
	}

	userChange(event) {
		const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		this.checkEmail1 = re.test(event)
		this.username = event
	}

	emailChange(event) {
		const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		this.checkEmail2 = re.test(event)
		this.email = event
	}

	changed(value) {
		this.selectedDataGroup = value.toString();
	}
	onChangePass(event) {
	}
	tambahUser() {
		const headers = new HttpHeaders()
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		var formData = new FormData();

		this.user_create = localStorage.getItem('id_user')

		if (
			this.password !== '' &&
			this.confirm_password !== '' &&
			this.selectedDataRole !== '' &&
			this.selectedDataGroup !== '' &&
			this.selectedDataJabatan !== '' &&
			this.alamat_pos !== '' &&
			this.alamat_langsung !== '' &&
			this.nama_user
		) {
			if (this.foto.length !== 0) {
				formData.append('foto', this.foto[0], '/' + this.path_foto.replace(/\\/g, '/'));
			}
			formData.append('id_role', this.selectedDataRole);
			formData.append('id_jabatan', this.selectedDataJabatan);
			formData.append('id_group', this.selectedDataGroup);
			formData.append('username', this.username);
			formData.append('nama_user', this.nama_user);
			formData.append('password', this.password);
			formData.append('confirm_password', this.confirm_password);
			formData.append('msisdn', this.msisdn);
			formData.append('imei', this.imei);
			formData.append('email', this.email);
			formData.append('alamat_pos', this.alamat_pos);
			formData.append('alamat_langsung', this.alamat_langsung);
			formData.append('user_create', this.user_create);
			var body_user = formData;
			if (this.username !== this.email) {
				this.showToastr('Username dan Email Tidak Sama');

			} else {

				if (this.checkEmail1 === false && this.checkEmail2 === false) {
					this.showToastr('Email Yang Anda Masukkan Tidak Valid');

				} else {

					if (
						this.passForm.get('field1').errors === null &&
						this.passForm.get('field2').errors === null
					) {
						if (this.password === this.confirm_password) {
							this.http
								.post<any>(API_BASE_URL + 'user-module/createUserByAdmin', body_user, { headers })
								.subscribe((data) => {
									if (data.message === 'registrasi akun berhasil') {
										this.showToastrBerhasil('Berhasil Menambahkan Pengguna Baru');
										this.router.navigate(['/admin/list/list-pengguna']);
									} else if (data.message === 'email tidak valid') {
										this.showToastr('Data Tidak Lengkap');
									} else if (data.message === 'email sudah terdaftar') {
										this.showToastr('Email Yang Anda Masukkan Sudah Terdaftar');
									} else if (data.result === "ekstensi file salah") {
										this.showToastr('Ekstensi File Salah');
									}
								});

						} else {
							this.showToastr('Konfirmasi Kata Kunci Tidak Sesuai');
						}
					} else {
						this.showToastr('Kata Kunci Minimal Sebanyak 5 Karakter');
					}
				}
			}

		} else {
			this.showToastr('Data Tidak Lengkap');
		}


	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}
	onFileSelected(event) {
		this.foto = event.target.files;
		if (this.foto[0]) {
			let file: File = this.foto[0];
			var idxDot = file.name.lastIndexOf('.') + 1;
			var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
			var sizeFile = file.size;
			var validationExt = this.checkExt(extFile);
			if (validationExt == false) {
				var img: any = document.querySelector('#preview img');
				img.file = file;
				var reader = new FileReader();
				reader.onload = (function (aImg: any) {
					return function (e) {
						aImg.src = e.target.result;
					};
				})(img);
				reader.readAsDataURL(file);
				var validationSize = this.checkSize(sizeFile);
			}
		}
	}

	checkExt(extFile) {
		if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'png') {
			this.isLoading = false;
			return false;
		} else {
			this.isLoading = true;
			this.showToastr('Unggah Gambar Dengan Ekstensi jpg,jpeg atau png');
			return true;
		}
	}
	checkSize(sizeFile) {
		if (sizeFile <= 5242880) {
			this.isLoading = false;
			return false;
		} else {
			this.isLoading = true;
			this.showToastr('Gambar Yang Anda Unggah Lebih Dari 5MB');
			return true;
		}
	}


	removeFile(index) {
		let file = this.foto[0];
		if (index !== -1) {
			file.splice(index, 1);
		}
	}

	onSubmit(isValid: Boolean) {
		if (isValid) {
			// Logic to add/update data here.
		}
	}

	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}