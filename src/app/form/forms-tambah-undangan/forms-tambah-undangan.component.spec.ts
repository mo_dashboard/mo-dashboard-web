import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsTambahUndanganComponent } from './forms-tambah-undangan.component';

describe('FormsTambahUndanganComponent', () => {
  let component: FormsTambahUndanganComponent;
  let fixture: ComponentFixture<FormsTambahUndanganComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsTambahUndanganComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsTambahUndanganComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
