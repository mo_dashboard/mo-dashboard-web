import { Component, OnInit, ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { EChartOption } from 'echarts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SidebarService } from '../../services/sidebar.service';
import { ToastrService } from 'ngx-toastr';
import { RouterLink } from '@angular/router';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { API_BASE_URL } from '../../constants'
import { NgxSpinnerService } from 'ngx-spinner';

var moment = require('moment')

declare var $: any;

@Component({
	selector: 'app-forms-tambah-undangan',
	templateUrl: './forms-tambah-undangan.component.html',
	styleUrls: ['./forms-tambah-undangan.component.css']
})
export class FormsTambahUndanganComponent implements OnInit {

	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public dropdownList: Array<any>;
	public selectedItems: Array<any>;
	public dropdownSettings: any;
	public data: any = {};
	public isLoading: boolean = false;

	public id_jenis_sidang: string = '1';
	public pimpinan_sidang: string = '1';
	public waktu_sidang: string = '';
	public tema: string = '';
	public lokasi: string = 'Istana Negara';
	public nomor_undangan: string = '';
	public sifat: string = 'Segera';
	public perihal: string = '-';
	public status: string = 'Terjadwal';
	public zona_waktu: string = 'WIB';
	public selectedDataSifat: string = '';
	public selectedDataStatus: string = '';
	public pembuat_undangan: string = '3';
	public catatan_undangan: string = '-';
	public user_create: string = localStorage.getItem('id_user');
	public berkas_undangan: any = [];
	public path_berkas_undangan: string = '';
	public dataUser: any;
	public jabatanPimpinan: any;
	public namaSidang: any;
	public namaLokasi: any;
	public lokasiSidang: string = '1';
	public dataSidang: any;
	public dataLokasi: any;
	public passphrase: string;
	public dataCreateUndangan: any = [];
	public body_undangan: object = {};
	public passphrase_next: string = '';
	public modelX: any;
	public fieldTextType: boolean;

	// Tambahan LOKASI
	public nama_lokasi: string = '';
	public lokasiData: any;
	public dropdownLokasi: Array<any>;
	public selectedLokasi: Array<any>;
	public SettingsLokasi: any;
	public lokasiAlt: any;

	date: Date = new Date();
	settings = {
		bigBanner: true,
		timePicker: true,
		format: 'dd-MMM-yyyy HH:mm',
		defaultOpen: false,
		closeOnSelect: true,
		showMeridian: false
	}
	constructor(
		private sidebarService: SidebarService,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private toastr: ToastrService,
		private router: Router,
		private modalService: NgbModal,
		private spinner: NgxSpinnerService
	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], '#49c5b6');
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], '#f4516c');
		this.dropdownList = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.selectedItems = [];
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'id_user',
			textField: 'buatSelect',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari'
		};
		//LOKASI
		this.dropdownLokasi = [
			{ item_id: 1, item_text: 'Cheese' },
			{ item_id: 2, item_text: 'Tomatoes' },
			{ item_id: 3, item_text: 'Mozzarella' },
			{ item_id: 4, item_text: 'Mushrooms' },
			{ item_id: 5, item_text: 'Pepperoni' },
			{ item_id: 6, item_text: 'Onions' }
		];
		this.selectedLokasi = [];
		this.SettingsLokasi = {
			singleSelection: true,
			idField: 'id_lokasi',
			textField: 'lokasiSelected',
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari',
			closeDropDownOnSelection: true
		};
	}

	ngOnInit() {
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http.get(API_BASE_URL +
			'undangan-module/getRespondenUndangan', { headers }).toPromise().then((res: any) => {
				this.dataUser = res.result;
				this.dataUser = this.dataUser.map((item: any, index: number) => {

					return {
						...item,
						buatSelect: item.nama_jabatan + ' ( ' + item.nama_user + ')'
					}
				})
			});
		this.http.get(API_BASE_URL +
			'user-module/getAllUser', { headers }).toPromise().then((res: any) => {
				this.jabatanPimpinan = res.result;
				this.jabatanPimpinan = this.jabatanPimpinan.map((item: any, index: number) => {

					return {
						...item,
						jabatan: item.nama_jabatan
					}
				})
			});
		this.http.get(API_BASE_URL +
			'master-module/getAllJenisSidang', { headers }).toPromise().then((res: any) => {
				this.namaSidang = res.result;
				this.dataSidang = Array.from(this.namaSidang.reduce((m, t) => m.set(t.nama_sidang, t), new Map()).values());
			});
		this.http.get(API_BASE_URL +
			'master-module/getAllLokasi', { headers }).toPromise().then((res: any) => {
				this.namaLokasi = res.result;
				this.dataLokasi = Array.from(this.namaLokasi.reduce((m, t) => m.set(t.nama_lokasi, t), new Map()).values());
			});
	}
	tambahModal(content) {
		this.nama_lokasi = "";
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
	}
	tambahLokasi(content) {

		this.modalService.open(content, { size: 'lg' })
		const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		const body = {
			nama_lokasi: this.nama_lokasi,
			user_create: this.user_create
		}
		this.http
			.post<any>(API_BASE_URL + 'master-module/createLokasi', body, { headers })
			.subscribe(
				(data: any) => {
					if (data.message === "insert berhasil") {
						this.showToastrBerhasil('Berhasil Menambah Lokasi');
						this.nama_lokasi = "";
						this.namaLokasi = [];
						this.modalService.dismissAll()
						this.ngOnInit()
					} else {
						this.nama_lokasi = "";
						this.showToastr('Terjadi Kesalahan Saat Menambah Lokasi');
					}
				}
			)
	}
	tutupModal() {
		this.nama_lokasi = "";
		this.modalService.dismissAll();
	}



	tambahUndangan() {
		this.isLoading = true;
	}

	passphraseSubmit() {
		if (
			this.waktu_sidang !== '' &&
			this.tema !== '' &&
			this.lokasi !== '' &&
			this.nomor_undangan !== '' &&
			this.zona_waktu !== '' &&
			this.status !== '' &&
			this.berkas_undangan !== [] &&
			this.berkas_undangan[0] !== undefined
		) {
			this.spinner.show();
			const headers = new HttpHeaders()
				.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

			var formdata = new FormData();
			formdata.append(
				'undangan',
				this.berkas_undangan[0],
				'/' + this.path_berkas_undangan.replace(/\\/g, '/')
			);
			formdata.append('id_jenis_sidang', this.id_jenis_sidang);
			formdata.append('pimpinan_sidang', this.pimpinan_sidang);
			formdata.append('waktu_sidang', moment(this.waktu_sidang).format('YYYY-MM-DD HH:mm'));
			formdata.append('zona_waktu', this.zona_waktu);
			formdata.append('tema', this.tema);
			formdata.append('lokasi', this.lokasi);
			formdata.append('lokasi_alt', this.lokasiAlt);
			formdata.append('nomor_undangan', this.nomor_undangan);
			formdata.append('sifat', this.sifat);
			formdata.append('status', this.status);
			formdata.append('pembuat_undangan', this.pembuat_undangan);
			formdata.append('user_create', this.user_create);
			formdata.append('passphrase', this.passphrase_next);

			this.body_undangan = formdata;
			this.http
				.post<any>(API_BASE_URL +
					'undangan-module/createUndangan', this.body_undangan, { headers })
				.subscribe(
					(data: any) => {
						if (data.response_code !== '200') {
							this.isLoading = false
							this.showToastr("Passphrase anda salah")
							this.spinner.hide();
							this.modalService.dismissAll();
							this.passphrase_next = ""
							return null
						}
						var body_penerima = this.selectedItems.map((item: any, index: number) => {
							return {
								...item,

								id_user: item.id_user,
								user_create: this.user_create,
								id_undangan: data.id_undangan
							}
						})
						this.http
							.post<any>(API_BASE_URL +
								'undangan-module/sendUndangan', body_penerima, { headers })
							.subscribe(
								(data) => {
									this.isLoading = false
									if (data.message === 'insert berhasil') {
										setTimeout(() => {
											this.showToastrBerhasil('Berhasil Membuat Undangan');
											this.modalService.dismissAll();
											this.router.navigate(['/admin/list/list-undangan']);
											this.spinner.hide();
										}, 5000);
									}
									else {
										this.showToastr('Terjadi Kesalahan Saat Mengirim Undangan');
										this.spinner.hide();
									}
								},
								(error) => {
									this.isLoading = false
									this.showToastr('Terjadi Kesalahan Saat Mengirim Undangan')
									this.spinner.hide();
								}
							);

					},
					error => {
						this.isLoading = false
						this.showToastr('Maaf, Periksa Kembali Koneksi Internet Anda')
						this.spinner.hide();
					}
				);
		} else {
			this.isLoading = false
			this.showToastr('Data Tidak Lengkap');
			this.spinner.hide();
		}
	}

	closeModal() {
		this.isLoading = false;
		this.modalService.dismissAll();
	}

	open(content) {
		this.modalService.open(content, { size: 'lg', backdrop: 'static', keyboard: false })
	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	toggleFieldTextType() { this.fieldTextType = !this.fieldTextType; }

	onFileSelected(event) {
		this.berkas_undangan = event.target.files;
		if (this.berkas_undangan[0]) {
			let file: File = this.berkas_undangan[0];
			var idxDot = file.name.lastIndexOf('.') + 1;
			var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
			if (extFile == 'pdf') {
				this.isLoading = false;
			} else {
				this.isLoading = true;
				this.showToastr('Unggah Berkas Undangan Dengan Ekstensi pdf');
			}
		}

	}
	onSubmit(isValid: Boolean) {
		if (isValid) {
			// Logic to add/update data here.
		}
	}

	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	methodx(e: any) {
		this.waktu_sidang = e.value.toLocaleDateString();
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach((element) => {
			xAxisData.push('');
		});

		return (chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return (
						'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' +
						color +
						';"></span>' +
						params[0].value
					);
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [
				{
					data: data,
					type: 'line',
					showSymbol: false,
					symbolSize: 1,
					lineStyle: {
						color: color,
						width: 1
					}
				}
			]
		});
	}
}