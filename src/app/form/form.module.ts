
import { NgModule,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsTambahPenggunaComponent } from './forms-tambah-pengguna/forms-tambah-pengguna.component';
import { FormsTambahUndanganComponent } from './forms-tambah-undangan/forms-tambah-undangan.component';
import { FormsEditStatusUndanganComponent } from './forms-edit-status-undangan/forms-edit-status-undangan.component';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelect2Module } from 'ng-select2';
// import { MomentModule } from 'ngx-moment'
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { NgxSpinnerModule } from 'ngx-spinner';



import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
	providers: [
		// use french locale
		{provide: OWL_DATE_TIME_LOCALE, useValue: 'id'},
	],
	declarations: [FormsTambahPenggunaComponent,FormsTambahUndanganComponent,FormsEditStatusUndanganComponent],
	imports: [ LazyLoadImageModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
		FormsModule,
		ReactiveFormsModule,
		NgbModule,
		//AngularDateTimePickerModule,
		NgSelect2Module,
		// MomentModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSpinnerModule
		
	],
	
	schemas: [CUSTOM_ELEMENTS_SCHEMA,         
			NO_ERRORS_SCHEMA]
})
export class FormModule { }
