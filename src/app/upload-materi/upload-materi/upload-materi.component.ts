import { Component, HostListener, AfterViewInit, OnInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SidebarService } from '../../services/sidebar.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, UrlSerializer, DefaultUrlSerializer } from '@angular/router';
import { API_BASE_URL } from '../../constants';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';


var moment = require('moment')
@Component({
	selector: 'app-upload-materi',
	templateUrl: './upload-materi.component.html',
	styleUrls: ['./upload-materi.component.css']
})
export class UploadMateriComponent implements OnInit  {

	public dataSurvey: any = [];
	public isValidFormSubmitted: boolean = null;
	public useForm: FormGroup;
	public groupPertanyaan: any = [];
	public jawaban: any = [];
	public saran: string = '';
	public timerInterval : any;
	public id : any;
	public id2 : any;
	public dataUser : any = [];
	public nama_user : string;
	public nama_jabatan : string;
	public survey_length : any = [];
	public id_pertanyaan : string;
	public jawaban_event : string;
	public checked : boolean;
	public undanganById : any = [];
	public tanggal_sidang : string;
	public jam_sidang_formatted : string;
	public materiRahasia : boolean;
	public sifat_materi : string;
	public selectedItems: any = [];
	public dropdownSettings: any;
	public dataUserKhusus: any = [];
	public dataUserUmum: any = [];
	public lengthFileAdd: number;
	public lengthFileRemove: number;
	public daftarResponden: any = [] ;
	public penerimaMateri: any = [] ;
	public data: any;
	public responden: any;
	public wordingResponse: string = '';

	constructor(
		private http: HttpClient,
		private toastr: ToastrService,
		private router: Router,
		private fb:FormBuilder,
		private route: ActivatedRoute,
		private serializer: UrlSerializer,
		// private defaultserializer: DefaultUrlSerializer,
		private location: Location,
		private modalService: NgbModal,
		private spinner: NgxSpinnerService,
		private cdr: ChangeDetectorRef
		) {

		this.selectedItems = []
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'penerima',
			textField: 'buatSelect' ,
			selectAllText: 'Pilih Semua',
			unSelectAllText: 'Tidak ada yang Dipilih',
			allowSearchFilter: true,
			searchPlaceholderText: 'Cari'
		};
	}

	ngOnInit() {

		this.id = this.route.snapshot.paramMap.get('id')
		this.id2 = this.route.snapshot.paramMap.get('id2')

		const headers = new HttpHeaders()
		.set('Content-Type', 'application/json')
		.set('Accept', 'application/json')
		.set('Access-Control-Allow-Headers', 'Content-Type')
		.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));
		this.http.get(API_BASE_URL+'undangan-module/getRespondenDetailUndangan/'+this.id, { headers }).toPromise().then((res: any) => {
			if(res.result !=="no data found" ){
			  
				this.dataUserKhusus = res.result.map((item: any, index: number) => {
			
					return {
						...item,
						buatSelect: item.nama_user + ' ( ' + item.nama_jabatan + ')',
						isChecked: false
					}
				
				})


				this.dataUserUmum = res.result.map((item: any, index: number) => {
			
					return {
						...item,
						buatSelect: item.nama_user + ' ( ' + item.nama_jabatan + ')',
					}
				
				})

			}
		})

		this.http.get(API_BASE_URL+'user-module/getUserById/'+this.id2, { headers }).toPromise().then((res: any) => {
			this.dataSurvey = res.result;
			res.result.map((item: any) => {
				this.nama_user = item.nama_user, 
				this.nama_jabatan = item.nama_jabatan
			})
		});
	}

	dropdown1(umum){	  
		this.sifat_materi = 'umum'

		let element = document.getElementById('dropdownMenu1');
		element.innerHTML = umum
		element.style.textAlign = "left"
		this.daftarResponden.map((item: any) => {
			if(item.isChecked = !(item.isChecked)) {
			}
		})
		this.daftarResponden = []

	}

	dropdown2(khusus){
		this.sifat_materi = 'khusus'
		let element = document.getElementById('dropdownMenu1');
		element.innerHTML = khusus
		element.style.textAlign = "left"
	}

	dropdown3(rahasia){
		this.sifat_materi = 'rahasia'

		let element = document.getElementById('dropdownMenu1');
		element.innerHTML = rahasia
		element.style.textAlign = "left"

		this.daftarResponden.map((item: any) => {
			if(item.isChecked = !(item.isChecked)) {
			}
		})
		this.daftarResponden = []
	}

	open(content) {
		const headers = new HttpHeaders()
		  .set('Content-Type', 'application/json')
		  .set('Accept', 'application/json')
		  .set('Access-Control-Allow-Headers', 'Content-Type')
		  .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))	
		this.modalService.open(content, { size: 'lg',backdrop:'static',keyboard:false })
	}

	onChangeSifat(event, sifatmateri, penerima){

		if(sifatmateri.isChecked = !(sifatmateri.isChecked)) {
			
			this.daftarResponden.push(sifatmateri)
		} else {

			var index = this.daftarResponden.map(item => item.penerima).indexOf(penerima);
			if (index > -1) {
				this.daftarResponden.splice(index, 1);
			} 
			return this.daftarResponden;

		}

	}

	files: File[] = [];

	onSelect(event: any,content) {
		if (event.rejectedFiles.length > 0) {
		this.modalService.open(content, { size: 'lg',backdrop:'static',keyboard:false, centered: true })
			this.wordingResponse = 'Unggah Materi Dengan Ekstensi Berikut png,jpeg,jpg,pdf dan mp4'
		} else {
		  this.lengthFileAdd = this.files.push(...event.addedFiles);

		}
	}

	onRemove(event) {
		this.lengthFileRemove = this.files.splice(this.files.indexOf(event), 1).length;
	}
	unggahMateri(content){
		const headers = new HttpHeaders()
		.set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'));

		if(this.sifat_materi === 'khusus'){
			this.penerimaMateri = this.daftarResponden.map((item: any, index: number) =>  {
				return {
					id_user: item.penerima
				}
			})

		} else if (this.sifat_materi === 'umum'){
			this.penerimaMateri = this.dataUserUmum.map((item: any, index: number) =>  {
					return {
					id_user: item.penerima
				}
			})
		} else if (this.sifat_materi === 'rahasia'){
			this.penerimaMateri = []
		}
		var formdata = new FormData();
		for (let i = 0; i < this.files.length; i++) {
		formdata.append(
			'materi[]',
			this.files[i]
		);
		}
		formdata.append('user_create', this.id2);
		formdata.append('id_undangan', this.id);
		formdata.append('sifat_materi', this.sifat_materi);
		formdata.append('penerima',JSON.stringify(this.penerimaMateri));
		var body_upload = formdata;

		if (this.files.length === 0){
			this.modalService.open(content, { size: 'lg',backdrop:'static',keyboard:false, centered: true })
			this.wordingResponse = 'Pilih materi terlebih dahulu'
		} else if (this.sifat_materi === undefined){
			this.modalService.open(content, { size: 'lg',backdrop:'static',keyboard:false, centered: true })
			this.wordingResponse = 'Pilih sifat materi terlebih dahulu'
		} else {
			this.spinner.show();
			this.http.post<any>(API_BASE_URL+'materi-module/createMateri', body_upload, { headers })
			.subscribe(
				(data) => 
				{
					if (data.message === 'insert berhasil') {
						setTimeout(() => {
							this.showToastrBerhasil('Berhasil Menambahkan Materi');
							this.daftarResponden = []
							this.spinner.hide();
							this.files = []
							this.router.navigate(['/authentication/page-login'])
						}, 5000);
					} else if (data.message === 'ekstensi file salah') {
						/** spinner ends after 5 seconds */
						this.showToastr("Ekstensi file salah");
						this.files = []
						this.spinner.hide();
					} else if (data.message === 'file tidak ditemukan') {
						this.showToastr('Pilih Berkas Terlebih Dahulu');
						/** spinner ends after 5 seconds */
						this.spinner.hide();
						this.daftarResponden = []
						this.files = []
					} else if (data.result === 'parameter tidak lengkap') {
						this.showToastr('Pilih Responden Terlebih Dahulu');

						/** spinner ends after 5 seconds */
						this.spinner.hide();
						this.files = []
						this.daftarResponden = []
					}
					this.router.navigate(['/upload-materi/mobile/'+this.id+'/'+this.id2]);
	
				}, (error) => {
					setTimeout(() => {
						/** spinner ends after 5 seconds */
						this.spinner.hide();
						this.files = []
						this.daftarResponden = []
						this.showToastr('Terjadi Kesalahan Saat Unggah Berkas')
					}, 5000);
				}
			);
		}
	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
		  closeButton: true,
		  positionClass: 'toast-bottom-right'
		});
	}
	
	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
		  closeButton: true,
		  positionClass: 'toast-bottom-right'
		});
	}

}