import { Routes, RouterModule, Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { UploadMateriComponent } from './upload-materi/upload-materi.component';

const routes: Routes = [   
    {
        path: 'upload-materi',
        children: [
            { path: 'mobile/:id/:id2', component: UploadMateriComponent, data: { 
                
               title : 'e-kabinet' 
            }}
        ]
    },
];

export const routing = RouterModule.forChild(routes);