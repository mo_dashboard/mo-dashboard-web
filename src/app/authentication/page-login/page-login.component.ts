import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { API_BASE_URL } from '../../constants'


@Component({
	selector: 'app-page-login',
	templateUrl: './page-login.component.html',
	styleUrls: ['./page-login.component.css']
})
export class PageLoginComponent implements OnInit {

	public username: string = ""
	public password: string = ""
	public galleryOptions: NgxGalleryOptions[];
	public _albums: Array<NgxGalleryImage> = new Array<NgxGalleryImage>();
	public fieldTextType: boolean;
	public returnUrl: string;

	constructor(private router: Router, private http: HttpClient ,private toastr: ToastrService, private activateRoute: ActivatedRoute) { 
		// get return url from route parameters or default to '/'
		this.returnUrl = this.activateRoute.snapshot.queryParams['return'] || '/';
		this.galleryOptions = [
            {
                width: '100%',
                height: '100%',
				imageAnimation: NgxGalleryAnimation.Fade,
				imageAutoPlay: true,
				imageInfinityMove:true,
				imageArrows : false,
				imageArrowsAutoHide : false,
				imageAutoPlayInterval : 3000,
				preview:false,
				thumbnails:false,
				
            },
            // max-width 800
            {
                breakpoint: 800,
                imagePercent: 100           
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false
            }
		];
		
		for (let i = 1; i <= 5; i++) {
			const src = 'assets/images/image-gallery/' + i + '.jpg';
			const thumb = 'assets/images/image-gallery/' + i + '.jpg';
            const album: NgxGalleryImage = {
                small: thumb,
                medium: thumb,
                big: src
			};
			this._albums.push(album);
		}
	}
	toggleFieldTextType() { this.fieldTextType = !this.fieldTextType; }

	ngOnInit() {
		
		// console.log('url: '+ this.returnUrl)
	}
	showToastr(pesan: string) {
        this.toastr.warning(pesan, undefined, {
            closeButton: true,
            positionClass: 'toast-bottom-right'
        });
    }
	onSubmit() {
		// console.log(this.username)
		// console.log(this.password)
		const headers = new HttpHeaders()
		.set('Content-Type', 'application/json')
		.set('Accept', 'application/json')
		.set('Access-Control-Allow-Headers', 'Content-Type')
		.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
		const body = {
			"username": this.username,
			"password": this.password,
		}
		this.http.post<any>(API_BASE_URL+
			'user-module/login', body, { headers }).subscribe(data => {
			// console.log(data)
			if (data.message === "silahkan buka email anda untuk verifikasi otp") {
				// this.showToastr(data.message)
				this.router.navigate(['/authentication/page-otp', { username: this.username, return: this.returnUrl}]);
		} else {
				this.showToastr("Password yang anda masukkan salah")
			}
			// console.log('tester login', data)
		})
	}
	// this.router.navigate(['/admin/dashboard/index']);
	// this.router.navigate(['/authentication/page-otp']);
}
