import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLoginComponent } from './page-login/page-login.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { routing } from './authentication.routing';
import { PageOTPComponent } from './page-otp/page-otp.component';
import { PagesModule } from '../pages/pages.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxGalleryModule } from 'ngx-gallery';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
	declarations: [PageLoginComponent, AuthenticationComponent, PageOTPComponent],
	imports: [ LazyLoadImageModule,
		CommonModule,
		routing,
		PagesModule,
        RouterModule,
		FormsModule,
		NgxGalleryModule,
		NgxSpinnerModule,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AuthenticationModule { }
