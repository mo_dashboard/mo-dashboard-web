import { Routes, RouterModule } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { PageLoginComponent } from './page-login/page-login.component';
import { PageOTPComponent } from './page-otp/page-otp.component';



const routes: Routes = [
    {
        path: '',
        component: AuthenticationComponent,
        children: [
            { path: '', redirectTo: 'page-login', pathMatch: 'full' },
            { path: 'page-login', component: PageLoginComponent, data: { 
               title : 'E-Kabinet' 
            }},
                // title: 'Login :: e-kabinet' } },
            { path: 'page-otp', component: PageOTPComponent, data: { 
               title : 'E-Kabinet' 
            }},
        ]
    }
];

export const routing = RouterModule.forChild(routes);