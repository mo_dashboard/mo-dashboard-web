import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../guards/auth.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { API_BASE_URL } from '../../constants'
import { NG_VALIDATORS } from '@angular/forms';


@Component({
    selector: 'app-page-otp',
    templateUrl: './page-otp.component.html',
    styleUrls: ['./page-otp.component.css']
})
export class PageOTPComponent implements OnInit {

    public otp : string="";
    public username : string = "";
    public galleryOptions: NgxGalleryOptions[];
    public _albums: Array<NgxGalleryImage> = new Array<NgxGalleryImage>();
    public fieldTextType: boolean;
    redirectURL: string;
    public pushOtp: any = [];

    
    constructor(private router: Router, private route : ActivatedRoute, private http: HttpClient, private authservice : AuthService ,private toastr: ToastrService) { 
        this.username = this.route.snapshot.paramMap.get('username')
        this.redirectURL = this.route.snapshot.paramMap.get('return')

        this.galleryOptions = [
            {
                width: '100%',
                height: '100%',
				imageAnimation: NgxGalleryAnimation.Fade,
				imageAutoPlay: true,
				imageInfinityMove:true,
				imageArrows : false,
				imageArrowsAutoHide : false,
				imageAutoPlayInterval : 3000,
				preview:false,
				thumbnails:false,
				
            },
            // max-width 800
            {
                breakpoint: 800,
                imagePercent: 100           
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false
            }
		];
		
		for (let i = 1; i <= 5; i++) {
			const src = 'assets/images/image-gallery/' + i + '.jpg';
			const thumb = 'assets/images/image-gallery/' + i + '.jpg';
            const album: NgxGalleryImage = {
                small: thumb,
                medium: thumb,
                big: src
			};
			this._albums.push(album);
		}
    }
    toggleFieldTextType() { this.fieldTextType = !this.fieldTextType; }

    ngOnInit() {
        // console.log(this.route.snapshot.paramMap.get('username'))
        // console.log(this.redirectURL)
    }

    showToastr(pesan: string) {
        this.toastr.warning(pesan, undefined, {
            closeButton: true,
            positionClass: 'toast-bottom-right'
        })
    }

    showToastrBerhasil(pesan: string) {
        this.toastr.success(pesan, undefined, {
            closeButton: true,
            positionClass: 'toast-bottom-right',
        });
    }


    onSubmit() {
        const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
        const body = {
            "username": this.username,
            "token": this.otp
        } 

        this.http.post<any>(API_BASE_URL+
            'user-module/verLogin', body, { headers }).subscribe(data => {
            // console.log('tester verifikasi login', data)
            this.pushOtp.push({
                otp:this.otp
            })
            // console.log('push otp', this.pushOtp)
            if (data.message === 'verifikasi otp berhasil') {
                this.authservice.changeIsLoggedIn(true)
                this.showToastrBerhasil('Verifikasi OTP Berhasil')
                localStorage.setItem('nama_role',data.nama_role)
                localStorage.setItem('id_user',data.id_user)
                localStorage.setItem('id_role',data.id_role)
                localStorage.setItem('nama_user',data.nama_user)
                // localStorage.setItem('contoh','hallo')
                // if (this.redirectURL){
                // console.log(this.redirectURL.includes('/vcon/meeting'))
                if (this.redirectURL.includes('/vcon/meeting')){
                    this.router.navigateByUrl(this.redirectURL)
                }else{
                    this.router.navigate(['/admin/dashboard/index']);
                } 
                if(data.nama_role ==='User Tipe 2' || data.nama_role ==='User Tipe 3'|| data.nama_role ==='User Tipe 4'|| data.nama_role ==='User Tipe 5'){
                this.router.navigate(['/admin/list/list-undangan-user']);
                }
                // if(data.nama_role ==='User Tipe 2' || data.nama_role ==='User Tipe 3'|| data.nama_role ==='User Tipe 4'|| data.nama_role ==='User Tipe 5'){
                //     this.router.navigate(['/admin/list/list-undangan-user']);
                // }
            } else {
                this.showToastr("OTP yang anda masukkan tidak cocok!")
            }
            if (this.pushOtp.length == '3') {
                this.router.navigate(['/authentication/page-login']);  
            }
        })

        // this.router.navigate(['/authentication/page-login']);
        // this.router.navigate(['/admin/dashboard/index']);
    }

}