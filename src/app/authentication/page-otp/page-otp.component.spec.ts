import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOTPComponent } from './page-otp.component';

describe('PageOTPComponent', () => {
  let component: PageOTPComponent;
  let fixture: ComponentFixture<PageOTPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOTPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOTPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
