import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageProfileComponent } from './page-profile/page-profile.component';
import { PageProfileV2Component } from './page-profile-v2/page-profile-v2.component';
import { PageGalleryComponent } from './page-gallery/page-gallery.component';
import { PageSearchResultsComponent } from './page-search-results/page-search-results.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { CommonElementsModule } from '../common-elements/common-elements.module';
import { NgxGalleryModule } from 'ngx-gallery';

import { LazyLoadImageModule } from 'ng-lazyload-image';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelect2Module } from 'ng-select2';
import { NgxPaginationModule } from 'ngx-pagination';
// import { UiSwitchModule } from 'ngx-toggle-switch';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
	declarations: [
		PageProfileComponent,
		PageProfileV2Component,
		PageGalleryComponent,
		PageSearchResultsComponent
	],
	imports: [ LazyLoadImageModule,
		CommonModule,
		NgbModule,
		RouterModule,
		NgxEchartsModule,
        CommonElementsModule,
		NgxGalleryModule,
		FormsModule,
		ReactiveFormsModule ,
		NgMultiSelectDropDownModule,
		NgSelect2Module,
		NgxPaginationModule,
		// UiSwitchModule,
		PdfViewerModule,
		NgxDropzoneModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSpinnerModule,
		Ng2SearchPipeModule,
		ReactiveFormsModule
	],
	exports: []
})
export class PagesModule { }
