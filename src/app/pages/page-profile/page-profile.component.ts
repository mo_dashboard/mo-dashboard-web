import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EChartOption } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { query } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import { Select2OptionData } from 'ng-select2';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_BASE_URL } from '../../constants'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { AuthService } from '../../guards/auth.service';

@Component({
	selector: 'app-page-profile',
	templateUrl: './page-profile.component.html',
	styleUrls: ['./page-profile.component.css']
})
export class PageProfileComponent implements OnInit {

	public visitorsOptions: EChartOption = {};
	public visitsOptions: EChartOption = {};
	public sidebarVisible: boolean = true;
	public userId: string;
	public dataUser: any = [];
	public lengthDataUser: number = 0;
	public pembagi: number = 5;
	public jumlahPage: number = 0;
	public sliceDataUser: any = [];
	public awalan: boolean = true;
	public isLoading: boolean = true;
	public isLoadingGambar: boolean = true;
	public isSearching: boolean = false;
	public dataUserById: string;
	public passwordById: string;
	public username: string;
	public result: any = [];
	public id_user: string;
	public id_role: string;
	public id_group: string;
	public id_jabatan: string;
	public nama_jabatan: string;
	public nama_user: string;
	public msisdn: string;
	public imei: string;
	public email: string;
	public user_update: string = localStorage.getItem('id_user')
	public isiModal: any = {};
	public dataJabatan: Array<Select2OptionData>;
	public dataRole: any;
	public dataGroup: any;
	public selectedDataRole: string = '';
	public selectedDataJabatan: string = '';
	public selectedDataGroup: string = '';
	public foto: any = [];
	public path_foto: string = '';
	public link_foto: string;
	public p: number = 1;
	public password_lama: string;
	public password_baru: string;
	public real_password: string;
	public fieldTextType: boolean;
	public fieldTextType2: boolean;
	public isLoadingForm: boolean = false;
	public passForm: FormGroup;
	public alamat_pos: string = '';
	public alamat_langsung: string = '';
	// public isLoading:boolean = false
	// public exampleData: Array<Select2OptionData>;

	// Properties
	public timerInterval : any;

	constructor(
		private sidebarService: SidebarService,
		private router: Router,
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private modalService: NgbModal,
		private toastr: ToastrService,
		private sanitizer: DomSanitizer,
		private fb: FormBuilder,
		private spinner: NgxSpinnerService,
		private authservice: AuthService

	) {
		this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
		this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
	}

	keluar() {
		this.authservice.logoutUser()
	}

	opensweetalertcst(){
		// console.log('tertekan')
		Swal.fire({
			width: '25rem',
			text: "Apakah anda ingin keluar?",
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, keluar',
			cancelButtonText: 'Batal',
			allowOutsideClick: false,
			allowEscapeKey: false
		  }).then((result) => {
			if (result.value) {
				Swal.fire({
					text: 'Anda Berhasil Keluar',
					timer: 1000,
					timerProgressBar: true,
					onBeforeOpen: () => {
						Swal.showLoading()
						this.timerInterval = setInterval(() => {
						const content = Swal.getContent()
						// if (content) {
						// 	const b = content.querySelector('b')
						// 	if (b) {
						// 	b.textContent = Swal.getTimerLeft()
						// 	}
						// }
						}, 100)
					},
					onClose: () => {
						clearInterval(this.timerInterval)
					}
				})
				this.keluar()
			}
		  })
	  }

	ngOnInit() {

		this.passForm = this.fb.group({
			field1: ['', [Validators.minLength(5)]],
			field2: ['', [Validators.minLength(5)]]
		})

		const headers = new HttpHeaders()
		.set('Content-Type', 'application/json')
		.set('Accept', 'application/json')
		.set('Access-Control-Allow-Headers', 'Content-Type')
		.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

		let id_user = localStorage.getItem('id_user')
		// console.log(id)
		this.http.get(API_BASE_URL+'user-module/getUserById/'+id_user,{headers}).toPromise().then((res:any)=>{
		// console.log('get by id undangan API 1',res.result)
		this.dataUserById = res.result
		})
	}

	toggleFieldTextType() { this.fieldTextType = !this.fieldTextType; }
	toggleFieldTextType2() { this.fieldTextType2 = !this.fieldTextType2; }

	changePassword(){
		const headers = new HttpHeaders()
		.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
	
		this.id_user = localStorage.getItem('id_user')

		if(this.password_baru === "") {
		  this.showToastr("Kata kunci baru kosong!")
		} else {
	
		  const body = {
			id_user: this.id_user,
			password_lama: this.password_lama,
			password_baru: this.password_baru
		  }
	
		  if(this.passForm.get('field2').errors === null) {
			
			this.http.post<any>(API_BASE_URL+'user-module/changePassword', body, {headers}).subscribe(
			  (data)=> {
			  // console.log('change pass',data)
			  if (data.response_code !== '200'){
				this.showToastr("Kata kunci lama salah!")
				this.password_baru = "";
				this.password_lama = "";
			  } else {
				this.showToastrBerhasil("Kata kunci berhasil diperbaharui!")
				this.modalService.dismissAll()
				this.passForm.reset()
				// this.keluar()
				this.opensweetalertcst()
				// this.ngOnInit()
			  }
			})
		  } else {
			this.showToastrBerhasil("Kata kunci minimal 5 karakter")
		  }
		}
	  }

	// ubahDataPengguna() {

	// 	// console.log('update user')
	// 	// console.log(this);
	// 	if (
	// 		this.id_user !== '' && this.id_role !== '' && this.nama_jabatan !== '' && this.id_jabatan !== '' && this.id_group !== '' && this.nama_user !== '' && this.email !== '' && this.user_update !== ''
	// 	) {
	// 		this.spinner.show();
	// 		const headers = new HttpHeaders()
	// 			// .set('Content-Type', 'application/json')
	// 			// .set('Accept', 'application/json')
	// 			// .set('Access-Control-Allow-Headers', 'Content-Type')
	// 			.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
	// 		var formData = new FormData();

	// 		//  console.log('kosong',this.foto.length)
	// 		if (this.foto.length !== 0) {
	// 			// console.log('loheee')
	// 			formData.append(
	// 				'foto',
	// 				this.foto[0],
	// 				'/' + this.path_foto.replace(/\\/g, '/')
	// 			);
	// 		}
	// 		// formData.append(
	// 		// 'foto',
	// 		// this.foto[0],
	// 		// '/' + this.path_foto.replace(/\\/g, '/')
	// 		// );

	// 		formData.append('id_user', this.id_user);
	// 		formData.append('id_role', this.id_role);
	// 		formData.append('id_jabatan', this.id_jabatan);
	// 		formData.append('id_group', this.id_group);
	// 		formData.append('nama_user', this.nama_user);
	// 		formData.append('email', this.email);
	// 		formData.append('imei', this.imei);
	// 		formData.append('msisdn', this.msisdn);
	// 		formData.append('user_update', this.user_update);

	// 		var body_update = formData;
	// 		// console.log('data user', body_update); 
	// 		this.http
	// 			.post<any>(API_BASE_URL + 'user-module/updateUser', body_update, { headers })
	// 			.subscribe(
	// 				(data: any) => {
	// 					// console.log('ubah user api', data);
	// 					if (data.message === "data berhasil diupdate") {
	// 						this.showToastrBerhasil('Berhasil mengubah data pengguna');
	// 						this.modalService.dismissAll()
	// 						// this.getAllUser();
	// 						// this.router.navigate(['/admin/list/list-ubah-data-pengguna']);
	// 						this.ngOnInit()
	// 						this.spinner.hide();
	// 					} else if (data.result === 'ekstensi file salah') {
	// 						this.showToastr('Ekstensi file salah');
	// 						this.modalService.dismissAll()
	// 						this.spinner.hide();
	// 					}
	// 					else {
	// 						this.showToastr('Terjadi kesalahan saat mengubah data pengguna');
	// 						this.spinner.hide();
	// 					}
	// 				}
	// 			)
	// 	} else {
	// 		this.showToastr('Data tidak lengkap ');
	// 		// this.spinner.hide();
	// 	}
	// }

	// arrayOne(n: number): any[] {
	// 	return Array(n);
	// }

	// onSearchUbahData(event) {
	// 	// console.log('triger search ubah data');
	// 	this.p = 1;
	// }

	// updateQuery(e : any) {
	//   // console.log('hehe', e)
	//   if (e !== "") {
	//     this.isSearching = true;
	//   } else {
	//     this.isSearching = false;
	//   }
	// }

	// gantiPage(n: number) {
	// 	this.sliceDataUser = this.dataUser.slice(this.pembagi * n, this.pembagi * (n + 1))
	// 	this.awalan = false
	// }


	onFileSelected(event) {
		// console.log('tester console', event.target.files);
		this.foto = event.target.files;
		if (this.foto[0]) {
			let file: File = this.foto[0];
			var idxDot = file.name.lastIndexOf('.') + 1;
			var extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
			var sizeFile = file.size;
			var validationExt = this.checkExt(extFile);
			// console.log('validation', validationExt);
			if (validationExt == false) {
				var img: any = document.querySelector('#preview img');
				img.file = file;
				var reader = new FileReader();
				reader.onload = (function (aImg: any) {
					return function (e) {
						aImg.src = e.target.result;
					};
				})(img);
				reader.readAsDataURL(file);
				var validationSize = this.checkSize(sizeFile);
			}
		}
		// console.log('size', sizeFile);
	}

	checkExt(extFile) {
		if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'png') {
			this.isLoadingForm = false;
			return false;
		} else {
			this.isLoadingForm = true;
			this.showToastr('Unggah gambar dengan ekstensi jpg,jpeg atau png');
			return true;
		}
	}
	checkSize(sizeFile) {
		if (sizeFile <= 5242880) {
			this.isLoading = false;
			return false;
		} else {
			this.isLoading = true;
			this.showToastr('Gambar yang anda unggah lebih dari 5MB');
			return true;
		}
	}

	showToastr(pesan: string) {
		this.toastr.warning(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}

	showToastrBerhasil(pesan: string) {
		this.toastr.success(pesan, undefined, {
			closeButton: true,
			positionClass: 'toast-bottom-right'
		});
	}
	toggleFullWidth() {
		this.sidebarService.toggle();
		this.sidebarVisible = this.sidebarService.getStatus();
		this.cdr.detectChanges();
	}

	loadLineChartOptions(data, color) {
		let chartOption: EChartOption;
		let xAxisData: Array<any> = new Array<any>();

		data.forEach(element => {
			xAxisData.push("");
		});

		return chartOption = {
			xAxis: {
				type: 'category',
				show: false,
				data: xAxisData,
				boundaryGap: false,
			},
			yAxis: {
				type: 'value',
				show: false
			},
			tooltip: {
				trigger: 'axis',
				formatter: function (params, ticket, callback) {
					return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
				}
			},
			grid: {
				left: '0%',
				right: '0%',
				bottom: '0%',
				top: '0%',
				containLabel: false
			},
			series: [{
				data: data,
				type: 'line',
				showSymbol: false,
				symbolSize: 1,
				lineStyle: {
					color: color,
					width: 1
				}
			}]
		};
	}

}