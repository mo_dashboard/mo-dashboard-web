import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { TEL, GEN, API_BASE_URL, INIT_ROOM } from '../../constants'
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import * as CryptoJS from 'crypto-js';
import Swal from 'sweetalert2/dist/sweetalert2.js';


declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  public displayName = localStorage.getItem('nama_user')
  public userRole = localStorage.getItem('nama_role')
  public userId = localStorage.getItem('id_user')
  public roomId: string;
  public temaSidang: string;
  public sidangId: string;
  public returnURL;
  public api : any
  // public api: any;
	public timerInterval : any;
  public showButton : boolean = false;
  public btnVisible : boolean = true;

  title = 'app';
  domain: string = "setkab.umeetme.id";
  // script.src = "https://meet.jit.si/external_api.js";
  // domain:string = "meet.jit.si";
  options: any;

  spinner6 = 'sp6';


  constructor(private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private _location: Location, private router: Router) {
    this.returnURL = this.activatedRoute.snapshot.paramMap.get('return')
    this.sidangId = this.getID(this.returnURL);

  }

  ngOnInit() {
    // this.getID(this.returnURL)
    // console.log(this.sidangId)
  }

  ngAfterViewInit(): void {
    this.initRoom();
    // this.buatRoom(); // FOR TESTING ONLY
  }

  getID(txt) {
    // this.showSpinner('sp6')
    // console.log(this.returnURL)
    let key = CryptoJS.enc.Utf8.parse(GEN);
    let iv = CryptoJS.enc.Utf8.parse(GEN);
    let decrypt;
    decrypt = CryptoJS.AES.decrypt(txt, key, {
      keySize: 256,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    })

    // this.getURL = decrypt.toString(CryptoJS.enc.Utf8);
    // console.log("decr: " + this.getURL)
    return decrypt.toString(CryptoJS.enc.Utf8);
  }

  showSpinner(name: string) {
    this.spinner.show(name);
  }
  hideSpinner(name: string) {
    this.spinner.hide(name);
  }

  showToastrSuc(pesan: string) {
    this.toastr.success(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    })
  }

  showToastrWarn(pesan: string) {
    this.toastr.warning(pesan, undefined, {
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });
  }

  async initRoom() {
    this.spinner.show()
    // console.log('user role',this.userRole)
    if (this.userRole == 'Super Admin' || this.userRole == 'Admin Sidang') {
      // console.log('masuk role 1 dan 2')
      await this.createRoom()
    } else {
      // console.log('masuk selain role 1 dan 2')
      await this.joinRoom()
    }
  }

  async buatRoom() {
    this.showSpinner('sp6')
    await this.spinner.show()
    this.showButton = true
    this.options = {
      roomName: this.roomId,  //FOR DEPLOY
      // roomName: this.returnURL, // FOR TESTING ONLY
      tel: TEL,
      width: "100%",
      // height: "720px",
      parentNode: document.querySelector('#meet'),
      configOverwrite: {
        isTelAuthEnabled: true,
        disableDeepLinking: true,
        defaultLanguage: 'id',
        subject: this.temaSidang
        // subject: 'Sidang E-Kabinet' // FOR TESTING ONLY
      },
      interfaceConfigOverwrite: {
        APP_NAME: 'E-Kabinet',
        NATIVE_APP_NAME: 'E-Kabinet',
        WATERMARK_IMAGE_PATH: API_BASE_URL+"assets/images/logo-setkab.png",
        filmStripOnly: false,
        inviteServiceUrl: API_BASE_URL+"#/vcon/meeting;return=" + this.returnURL,
        MOBILE_APP_PROMO: false,
        TOOLBAR_BUTTONS: [
          'microphone', 'camera', 'closedcaptions', 'desktop', 'embedmeeting', 'fullscreen',
          'fodeviceselection', 'profile', 'chat', 'recording',
          'livestreaming', 'etherpad', 'sharedvideo', 'settings', 'raisehand',
          'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
          'tileview', 'videobackgroundblur', 'download'
        ],
        ENFORCE_NOTIFICATION_AUTO_DISMISS_TIMEOUT: 3000,
        LANG_DETECTION: false,
      },
      userInfo: {
        // email: 'email@jitsiexamplemail.com',
        displayName: this.displayName
      }
    }

    
    let api = new JitsiMeetExternalAPI(this.domain, this.options);

    this.hideSpinner('sp6')


    // it will fire only for guests
    api.on('passwordRequired', () => {
      api.executeCommand('password', INIT_ROOM)
    });

    //this will setup the password for 1st user
    api.on('participantRoleChanged', (event) => {
      if (event.role === 'moderator') {
        api.executeCommand('password', INIT_ROOM)
      }
    })
    let endBtn = document.querySelector('#btnEnd')
    
    endBtn.addEventListener('click', function (event) {
      // var confirmHangup = confirm("Are you sure !");
      // if (confirmHangup == true) {
      //   // console.log('Hang Up')
      //   api.executeCommand('hangup');
      // }
      this.btnVisible = false

      Swal.fire({         
        text: ("Apakah Anda Yakin Ingin Mengakhiri Sidang?"),         
        type: "warning",         
        icon:'warning',         
        showCancelButton: true,         
        confirmButtonColor: '#3085d6',         
        cancelButtonColor: '#d33',         
        confirmButtonText: "OK",         
        cancelButtonText: 'Batal'         
      }).then((result) => {         
        if (result.isConfirmed) {         
          api.executeCommand('hangup');         
        } 
      })
    })
    
    await this.spinner.hide()

    api.on('readyToClose', () => {
      // console.log('end');
      this.btnVisible = false
      this.hilang()
      this.closeRoom();
      api.dispose();
    });
  }

  // opensweetalertcst(api){
	// 	// console.log('tertekan')
	// 	Swal.fire({
	// 		width: '25rem',
	// 		text: "Apakah anda yakin ingin mengakhiri meeting?",
	// 		showCancelButton: true,
	// 		confirmButtonColor: '#3085d6',
	// 		cancelButtonColor: '#d33',
	// 		confirmButtonText: 'Ya',
	// 		cancelButtonText: 'Batal'
	// 	  }).then((result) => {
	// 		if (result.value) {
	// 			Swal.fire({
	// 				text: 'Anda Berhasil Keluar Meeting',
	// 				timer: 1000,
	// 				timerProgressBar: true,
	// 				onBeforeOpen: () => {
	// 					Swal.showLoading()
	// 					this.timerInterval = setInterval(() => {
	// 					const content = Swal.getContent()
	// 					// if (content) {
	// 					// 	const b = content.querySelector('b')
	// 					// 	if (b) {
	// 					// 	b.textContent = Swal.getTimerLeft()
	// 					// 	}
	// 					// }
	// 					}, 100)
	// 				},
	// 				onClose: () => {
	// 					clearInterval(this.timerInterval)
	// 				}
  //       })
  //       this.closeRoom()
	// 			// this.keluar()
	// 		}
	// 	  })
  //   } 
    
    keluar() {
      this.router.navigate(['/admin/list/list-undangan-user']);
    }
  
    hilang() {
      this.btnVisible = false
    }

  async createRoom() {
    await this.showSpinner('sp6')
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    await this.http.post<any>(
      API_BASE_URL + 'vcon-module/createRoom',
      {
        "undangan_id": this.sidangId,
        "user_id": this.userId
      },
      {
        headers
      }).toPromise().then(async (res: any) => {
        // console.log(JSON.stringify(res))
        if (res.response_code == '200') {
          this.showToastrSuc(res.message)
          this.roomId = res.roomId
          this.temaSidang = res.tema
          await this.buatRoom();
        } else {
          this.showToastrWarn(res.message)

          setTimeout(() => {
            this.hideSpinner('sp6')
            this._location.back()
            // window.top.close();
          }, 2000)
        }
      })
  }

  async joinRoom() {
    await this.showSpinner('sp6')
    // console.log(this.sidangId)
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type')
      .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

    await this.http.post<any>(
      API_BASE_URL + 'vcon-module/joinRoom',
      {
        "undangan_id": this.sidangId,
        "user_id": this.userId
      },
      {
        headers
      }).toPromise().then(async (res: any) => {
        if (res.response_code == '200') {
          // console.log(JSON.stringify(res))
          this.showToastrSuc(res.message)
          this.roomId = res.roomId
          this.temaSidang = res.tema
          await this.buatRoom();
        } else {
          this.showToastrWarn(res.message)

          setTimeout(() => {
            this.hideSpinner('sp6')
            this._location.back()
            // window.top.close();
          }, 2000)
        }
      })
  }

  async closeRoom() {
    await this.showSpinner('sp6');
    if (this.userRole == '1' || this.userRole == '2') {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set('Authorization', 'Basic ' + btoa('ekab40:Ek4b#116'))

      this.spinner.show()

      await this.http.post<any>(
        API_BASE_URL + 'vcon-module/closeRoom',
        {
          "undangan_id": this.sidangId
        },
        {
          headers
        }).toPromise().then((res: any) => {
          if (res.response_code == '200') {
            this.showToastrSuc(res.message)
            setTimeout(() => {
              this.hideSpinner('sp6')
              this._location.back()
              this.btnVisible = false
              // window.top.close();
            }, 8000)
          } else {
            this.showToastrWarn(res.message)
            this.hideSpinner('sp6')
          }
        })
    } else {
      this.hideSpinner('sp6')
      await this._location.back()
    }

  }

}