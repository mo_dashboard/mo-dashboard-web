import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeetingComponent } from './meeting/meeting.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [MeetingComponent],
  imports: [
    NgbModule,
    CommonModule,
    NgxSpinnerModule,
  ],
  // bootstrap: [MeetingComponent]
  // schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class VconModule { }
