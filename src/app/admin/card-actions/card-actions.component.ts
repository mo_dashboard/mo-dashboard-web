import { Component, OnInit,} from '@angular/core';

@Component({
	selector: 'app-card-actions',
	templateUrl: './card-actions.component.html',
	styleUrls: ['./card-actions.component.css']
})
export class CardActionsComponent implements OnInit {


	
	public showActions: boolean = false;
	public actions: Array<any> = [{ "key": "Grup Rapat Terbatas 001/20200612/XV", "url": "" }, { "key": "Grup Sidang Kabinet 001/20200613/XV", "url": "" }, { "key": "Grup Rapat Terbatas 001/20200614/XV", "url": "" }];

	constructor() { }

	ngOnInit() {
	}

}
