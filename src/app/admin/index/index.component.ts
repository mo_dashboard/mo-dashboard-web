import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EChartOption,ECharts } from 'echarts';
import { SidebarService } from '../../services/sidebar.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as echarts from 'echarts';
import { API_BASE_URL } from '../../constants'

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;
    public visitorsOptions: EChartOption = {};
    public visitsOptions: EChartOption = {};
    public totalUndangan: EChartOption = {};
    public undangan: number;
    public totalPengguna: EChartOption = {};
    public pengguna : number;
    public totalMateri: EChartOption = {};
    public materi: number;
    public totalRisalah: EChartOption = {};
    public risalah: number;
    public visitsAreaOptions: EChartOption = {};
    public LikesOptions: EChartOption = {};
    public stackedBarChart: EChartOption = {};
    public dataManagedBarChart: EChartOption = {};
    public LargeAreaOptions: any = {};


    public visitsAreaOptionsSeries: Array<number> = [1, 4, 2, 3, 1, 5];
    public visits: number = (this.visitsAreaOptionsSeries.reduce((a, b) => a + b, 0) * 100);
    public LikesOptionsSeries: Array<number> = [1, 3, 5, 1, 4, 2];
    public likes: number = (this.LikesOptionsSeries.reduce((a, b) => a + b, 0) * 10);

    public interval: any = {};

    public pieOptions: any = {};
    public chartOptions: any = {};
    public nama_role : string = localStorage.getItem('nama_role');


    constructor(private activatedRoute: ActivatedRoute, private sidebarService: SidebarService, private cdr: ChangeDetectorRef, private toastr: ToastrService,private http : HttpClient) {
        this.visitorsOptions = this.loadLineChartOptions([3, 5, 1, 6, 5, 4, 8, 3], "#49c5b6");
        this.visitsOptions = this.loadLineChartOptions([4, 6, 3, 2, 5, 6, 5, 4], "#f4516c");
        this.visitsAreaOptions = this.loadLineAreaChartOptions([1, 4, 2, 3, 1, 5], "#4aacc5", "#92cddc");
        this.LikesOptions = this.loadLineAreaChartOptions([1, 3, 5, 1, 4, 2], "#4f81bc", "#95b3d7");
        this.dataManagedBarChart = this.getDataManagedChartOptions();
    }

    ngOnInit() {

        const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
		    this.http
			.get(API_BASE_URL+'dashboard-module/getTotalUser', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get all role', res.result)
                this.pengguna = res.result[0].total_user;
                // console.log('total',res.result)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getTotalUndangan', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get all role', res.result)
                this.undangan = res.result[0].total_undangan;
                // console.log('total',res.result)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getTotalMateri', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get all role', res.result)
                this.materi = res.result[0].total_materi;
                // console.log('total',res.result)
			});
            this.http
			.get(API_BASE_URL+'dashboard-module/getTotalRisalah', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get all role', res.result)
                this.risalah = res.result[0].total_risalah;
                // console.log('total',res.result)
			});




        let that = this;
        // setTimeout(function () {
        //     that.showToastr();
        // }, 1000);
        this.chartIntervals();
        var base = +new Date(1968, 9, 3);
        var oneDay = 24 * 3600 * 1000;
        var date = [];

        var largeData = [Math.random() * 300];

        for (var i = 1; i < 20000; i++) {
            var now = new Date(base += oneDay);
            date.push([now.getFullYear(), now.getMonth() + 1, now.getDate()].join('/'));
            let datum = Math.round((Math.random() - 0.5) * 20 + largeData[i - 1]);
            if (datum < 0) {
                datum = (datum * -1);
            }
            largeData.push(datum);
        }
        this.LargeAreaOptions = {
            tooltip: {
                trigger: 'axis'
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: true,
                data: date,
                axisLine: {
                    lineStyle: {
                        color: "#C2C2C2",
                    },
                },
                axisLabel: {
                    textStyle: {
                        color: "#C2C2C2",
                    },
                }
            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '100%'],
                axisLine: {
                    lineStyle: {
                        color: "#C2C2C2",
                    },
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: "#EBEEF2",
                    },
                },
                axisLabel: {
                    textStyle: {
                        color: "#C2C2C2",
                    },
                }
            },
            dataZoom: [{
                type: 'inside',
                start: 0,
                end: 10
            }, {
                start: 0,
                end: 10,
                handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                handleSize: '80%',
                handleStyle: {
                    color: '#fff',
                    shadowBlur: 3,
                    shadowColor: "#3eacff",
                    shadowOffsetX: 2,
                    shadowOffsetY: 2
                }
            }],
            series: [
                {
                    name: 'Simulation data',
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    sampling: 'average',
                    itemStyle: {
                        color: "#e47297"
                    },
                    areaStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: "#3eacff"
                        }, {
                            offset: 1,
                            color: "#e47297"
                        }])
                    },
                    data: largeData
                }
            ]
        };

        // tambahan
        this.pieOptions = {
            color: ["#ffce4b", "#3eacff","#e26d5c","#ffe66d"],
			title: {
                x: 'center',
                textStyle: {
                    color: "#C2C2C2"
                }
			},
			tooltip: {
				trigger: 'item',
				formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			legend: {
				orient: 'horizontal',
				left: 'left',
                data: ['Rapat Terbatas','Sidang Kabinet Paripurna','Pertemuan Lain','TPA'],
                textStyle: {
                    color: "#C2C2C2"
                }
			},
			series: [
				{
					name: 'Materi',
					type: 'pie',
					radius: '55%',
					center: ['50%', '60%'],
					data: [
						{ value: 310, name: 'Rapat Terbatas' },
                        { value: 234, name: 'Sidang Kabinet Paripurna' },
                        { value: 120, name: 'Pertemuan Lain'},
						{ value: 434, name: 'TPA' }
					],
					itemStyle: {
						emphasis: {
							shadowBlur: 10,
							shadowOffsetX: 0,
							shadowColor: 'rgba(0, 0, 0, 0.5)'
						}
					}
				}
			]
        };
        this.chartOptions = {
            color: ["#49c5b6", "#e26d5c", "#a27ce6", "#ffce4b", "#3eacff"],
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'cross',
					label: {
						backgroundColor: '#6a7985'
					}
				}
			},
			legend: {
                data: ['Rapat Terbatas', 'Sidang Kabinet Paripurna', 'Pertemuan Lain', 'TPA'],
                textStyle:{
                    color: "#C2C2C2"
                }
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis: [
				{
					type: 'category',
					boundaryGap: false,
                    data: ['April', 'Mei', 'Juni', 'Juli', 'Agustus'],
                    axisLine: {
                        lineStyle: {
                            color: "#C2C2C2",
                        },
                    },
                    axisLabel: {
                        textStyle: {
                            color: "#C2C2C2",
                        },
                    },
				}
			],
			yAxis: [
				{
                    type: 'value',
                    splitLine: {
                        show: false
                    },
                    axisLine: {
                        lineStyle: {
                            color: "#C2C2C2",
                        },
                    },
                    axisLabel: {
                        textStyle: {
                            color: "#C2C2C2",
                        },
                    },
				}
			],
			series: [
				{
					name: 'Rapat Terbatas',
					type: 'line',
					stack: 'Total amount',
					areaStyle: {},
					data: [120, 132, 101, 134, 90, 230, 210]
				},
				{
					name: 'Sidang Kabinet Paripurna',
					type: 'line',
					stack: 'Total amount',
					areaStyle: {},
					data: [220, 182, 191, 234, 290, 330, 310]
				},
				{
					name: 'Pertemuan Lain',
					type: 'line',
					stack: 'Total amount',
					areaStyle: {},
					data: [150, 232, 201, 154, 190, 330, 410]
				},
				{
					name: 'TPA',
					type: 'line',
					stack: 'Total amount',
					areaStyle: { normal: {} },
					data: [320, 332, 301, 334, 390, 330, 320]
				}
			]
		};
    }

    ngOnDestroy() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    showToastr() {
        this.toastr.info('Hello, welcome to Lucid, a unique admin Template.', undefined, {
            closeButton: true,
            positionClass: 'toast-top-right'
        });
    }

    chartIntervals() {
        let that = this;
        this.interval = setInterval(function () {
            // that.earningOptionsSeries.shift();
            let rand = Math.floor(Math.random() * 11);
            if (!rand) {
                rand = 1;
            }
            // that.earningOptionsSeries.push(rand);
            // that.earningOptions = that.loadLineAreaChartOptions(that.earningOptionsSeries, "#f79647", "#fac091");
            // that.earnings = '' + (that.earningOptionsSeries.reduce((a, b) => a + b, 0) * 1).toLocaleString();


            // that.salesOptionsSeries.shift();
            rand = Math.floor(Math.random() * 11);
            if (!rand) {
                rand = 1;
            }
            // that.salesOptionsSeries.push(rand);
            // that.salesOptions = that.loadLineAreaChartOptions(that.salesOptionsSeries, "#604a7b", "#a092b0");
            // that.sales = '' + (that.salesOptionsSeries.reduce((a, b) => a + b, 0) * 1).toLocaleString();

            that.visitsAreaOptionsSeries.shift();
            rand = Math.floor(Math.random() * 11);
            if (!rand) {
                rand = 1;
            }
            that.visitsAreaOptionsSeries.push(rand);
            that.visits += rand;
            that.visitsAreaOptions = that.loadLineAreaChartOptions(that.visitsAreaOptionsSeries, "#4aacc5", "#92cddc");

            that.LikesOptionsSeries.shift();
            rand = Math.floor(Math.random() * 11);
            if (!rand) {
                rand = 1;
            }
            that.LikesOptionsSeries.push(rand);
            that.likes += rand;
            that.LikesOptions = that.loadLineAreaChartOptions(that.LikesOptionsSeries, "#4f81bc", "#95b3d7");
            that.cdr.markForCheck();
        }, 3000);

    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
        let that = this;
        setTimeout(function () {
            that.isResizing = false;
            that.cdr.detectChanges();
        }, 400);
    }

    loadLineChartOptions(data, color) {
        let chartOption: EChartOption;
        let xAxisData: Array<any> = new Array<any>();

        data.forEach(element => {
            xAxisData.push("");
        });

        return chartOption = {
            xAxis: {
                type: 'category',
                show: false,
                data: xAxisData,
                boundaryGap: false,
            },
            yAxis: {
                type: 'value',
                show: false
            },
            tooltip: {
                trigger: 'axis',
                formatter: function (params, ticket, callback) {
                    return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
                }
            },
            grid: {
                left: '0%',
                right: '0%',
                bottom: '0%',
                top: '0%',
                containLabel: false
            },
            series: [{
                data: data,
                type: 'line',
                showSymbol: false,
                symbolSize: 1,
                lineStyle: {
                    color: color,
                    width: 1
                }
            }]
        };
    }

    loadLineAreaChartOptions(data, color, areaColor) {
        let chartOption: EChartOption;
        let xAxisData: Array<any> = new Array<any>();

        data.forEach(element => {
            xAxisData.push("");
        });

        return chartOption = {
            xAxis: {
                type: 'category',
                show: false,
                data: xAxisData,
                boundaryGap: false,
            },
            yAxis: {
                type: 'value',
                show: false,
                min: 1
            },
            tooltip: {
                trigger: 'axis',
                formatter: function (params, ticket, callback) {
                    return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + color + ';"></span>' + params[0].value;
                }
            },
            grid: {
                left: '0%',
                right: '0%',
                bottom: '0%',
                top: '0%',
                containLabel: false
            },
            series: [{
                data: data,
                type: 'line',
                showSymbol: false,
                symbolSize: 1,
                lineStyle: {
                    color: color,
                    width: 1
                },
                areaStyle: {
                    color: areaColor
                }
            }]
        };
    }


    getDataManagedChartOptions() {
        let options: any = {
            tooltip: {
                trigger: 'item',
            },
            grid: {
                borderWidth: 0,
                y: 80,
                y2: 60
            },
            xAxis: [
                {
                    type: 'category',
                    show: false,
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    show: false
                }
            ],
            series: [
                {

                    type: 'bar',
                    stack: 'Gedgets',
                    data: [2, 0, 5, 0, 4, 0, 8, 0, 3, 0, 9, 0, 1, 0, 5],
                    itemStyle: {
                        color: '#7460ee'
                    },
                    barWidth: '5px'
                },
                {

                    type: 'bar',
                    stack: 'Gedgets',
                    data: [0, -5, 0, -1, 0, -9, 0, -3, 0, -8, 0, -4, 0, -5, 0],
                    itemStyle: {
                        color: '#afc979'
                    },
                    barWidth: '5px'
                }
            ]
        };

        return options;
    }
}
