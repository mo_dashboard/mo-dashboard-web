import { NgModule, ApplicationModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { routing } from './admin.routing';
import { NgxEchartsModule } from 'ngx-echarts';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { FullCalendarModule } from 'ng-fullcalendar';

import { AdminComponent } from './admin/admin.component';
import { DetailTilesComponent } from './detail-tiles/detail-tiles.component';
import { CardActionsComponent } from './card-actions/card-actions.component';
import { ManagedDataComponent } from './managed-data/managed-data.component';
import { TopProductsComponent } from './top-products/top-products.component';
import { ApplicationsModule } from '../applications/applications.module';
import { ChartsModule } from '../charts/charts.module';
import { FileManagerModule } from '../file-manager/file-manager.module';
import { PagesModule } from '../pages/pages.module';
import { RouterModule } from '@angular/router';
import { CommonElementsModule } from '../common-elements/common-elements.module';
import { ListModule } from '../list/list.module';
import { UiElementsModule } from '../ui-elements/ui-elements.module';
import { LayoutModule } from '../layout/layout.module';
import { FormModule } from '../form/form.module';

// import { BlogsModule } from '../blogs/blogs.module';
// import { WidgetsModule } from '../widgets/widgets.module';
// import { MapsModule } from '../maps/maps.module';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { VconModule } from '../vcon/vcon.module';
// import { NgxSpinnerModule } from 'ngx-spinner';
import { PageNotFoundAdminComponent } from '../authentication/page-not-found-admin/page-not-found-admin.component';
import { AppModule } from '../app.module'

@NgModule({
	imports: [ LazyLoadImageModule,
		CommonModule,
		routing,
		NgxEchartsModule,
		LayoutModule,
		RichTextEditorAllModule,
		NgbModule,
		// FullCalendarModule,
		ApplicationsModule,
		ChartsModule,
		FileManagerModule,
		PagesModule,
		RouterModule,
		CommonElementsModule,
		ListModule,
		UiElementsModule,
		FormModule,		
	
		// BlogsModule,
        // WidgetsModule,
		// MapsModule,
		// NgxSpinnerModule,
		// VconModule
	],
	declarations: [
		AdminComponent,
		IndexComponent,
		DetailTilesComponent,
		CardActionsComponent,
		ManagedDataComponent,
		TopProductsComponent,
		PageNotFoundAdminComponent,
	],
	// schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdminModule { }
