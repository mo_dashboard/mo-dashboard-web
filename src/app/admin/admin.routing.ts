import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../guards/auth-guard.service';
import { AdminComponent } from './admin/admin.component';
import { IndexComponent } from './index/index.component';
import { FileDocumentsComponent } from '../file-manager/file-documents/file-documents.component';
import { FileMediaComponent } from '../file-manager/file-media/file-media.component';
import { FileImagesComponent } from '../file-manager/file-images/file-images.component';
import { AppChatComponent } from '../applications/app-chat/app-chat.component';
import { ListPenggunaComponent } from '../list/list-pengguna/list-pengguna.component';
import { ListUndanganComponent } from '../list/list-undangan/list-undangan.component';
import { ListRisalahComponent } from '../list/list-risalah/list-risalah.component';
import { ListMateriComponent } from '../list/list-materi/list-materi.component';
import { ListDenahComponent } from '../list/list-denah/list-denah.component';
import { ListUbahDataPenggunaComponent } from '../list/list-ubah-data-pengguna/list-ubah-data-pengguna.component';
import { ListTambahRespondenUndanganComponent } from '../list/list-tambah-responden-undangan/list-tambah-responden-undangan.component';
import { ListKirimMateriComponent } from '../list/list-kirim-materi/list-kirim-materi.component';
import { ListUnggahMateriComponent } from '../list/list-unggah-materi/list-unggah-materi.component';
import { ListDaftarMateriComponent } from '../list/list-daftar-materi/list-daftar-materi.component';
import { ListMasterDataComponent } from '../list/list-master-data/list-master-data.component';
import { ListJenisSidangComponent } from '../list/list-jenis-sidang/list-jenis-sidang.component';
import { ListJabatanComponent } from '../list/list-jabatan/list-jabatan.component';
import { ListRoleComponent } from '../list/list-role/list-role.component';
import { ListLokasiComponent } from '../list/list-lokasi/list-lokasi.component';
import { ListGroupComponent } from '../list/list-group/list-group.component';
import { ListUnggahKirimRisalahComponent } from '../list/list-unggah-kirim-risalah/list-unggah-kirim-risalah.component';
import { ListTambahRespondenRisalahComponent } from '../list/list-tambah-responden-risalah/list-tambah-responden-risalah.component';
import { ListUbahRisalahComponent } from '../list/list-ubah-risalah/list-ubah-risalah.component';
import { ListLaporanRisalahComponent } from '../list/list-laporan-risalah/list-laporan-risalah.component';
import { ListRisalahUserComponent } from '../list/list-risalah-user/list-risalah-user.component';
import { ListSurveyComponent } from '../list/list-survey/list-survey.component';
import { ListKirimSurveyComponent } from '../list/list-kirim-survey/list-kirim-survey.component';
import { ListLaporanSurveyComponent } from '../list/list-laporan-survey/list-laporan-survey.component';
import { ListDownloadAplikasiComponent } from '../list/list-download-aplikasi/list-download-aplikasi.component';
import { FormsTambahPenggunaComponent } from '../form/forms-tambah-pengguna/forms-tambah-pengguna.component';
import { FormsTambahUndanganComponent } from '../form/forms-tambah-undangan/forms-tambah-undangan.component';
import { FormsEditStatusUndanganComponent } from '../form/forms-edit-status-undangan/forms-edit-status-undangan.component';
import { MeetingComponent } from '../vcon/meeting/meeting.component';
import { ListUndanganUserComponent } from '../list/list-undangan-user/list-undangan-user.component';
import { PageNotFoundAdminComponent } from '../authentication/page-not-found-admin/page-not-found-admin.component';
import { PageProfileComponent } from '../pages/page-profile/page-profile.component';


const routes: Routes = [   
    {
        path: '',
        component: AdminComponent,
        
        children: [
            { path: '', redirectTo:'dashboard'},
            {
                path: 'dashboard',
                //canActivate: [AuthGuard],
                children: [
                    { path: '', redirectTo: 'index', pathMatch: 'full' },
                    { path: 'index', component: IndexComponent, data: { 
                        
                       title : 'MO Dashboard' 
                    }},
                ],
                data: {
                    //expectedRole: ['Super Admin', 'Admin Materi', 'Admin Risalah', 'User Tipe 1']
                }
            },
            { 
                path: 'app',
                children: [
                    { path: 'app-chat', component: AppChatComponent, data: { 
                        
                       title : 'MO Dashboard' 
                    }},
                        // title: '' } },
                        // title: '' } },
                        // title: '' } },
                    
                ]
            },
           
            {
                path: 'file-manager',
                children: [
                    { path: '', redirectTo: 'file-documents', pathMatch: 'full' },
                    { path: 'file-documents', component: FileDocumentsComponent, data: { 
                        
                       title : 'MO Dashboard' 
                    }},
                        // title: '' } },
                    { path: 'file-media', component: FileMediaComponent, data: { 
                        
                       title : 'MO Dashboard' 
                    }},
                        // title: '' } },
                    { path: 'file-images', component: FileImagesComponent, data: { 
                        
                       title : 'MO Dashboard' 
                    }},
                        // title: '' } }
                ]
            },
            {
                path: 'list',
                children: [
                    { path: '', redirectTo: 'list-pengguna', pathMatch: 'full' },
                    // { path: '**', component: PageNotFoundAdminComponent, data: { 
                        
                    //     title : 'MO Dashboard' 
                    //  }},
                    { 
                        path: 'list-pengguna', component: ListPenggunaComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-undangan', component: ListUndanganComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Sidang'],
                            title : 'MO Dashboard' 
                        
                        }
                    },
                    { 
                        path: 'list-undangan-user', component: ListUndanganUserComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['User Tipe 1','User Tipe 2', 'User Tipe 3', 'User Tipe 4', 'User Tipe 5'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-risalah', component: ListRisalahComponent, 
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Risalah'],
                            title : 'MO Dashboard'
                                                    
                        }
                    },
                    { 
                        path: 'list-materi', component: ListMateriComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Materi', 'User Tipe 1','User Tipe 2', 'User Tipe 3', 'User Tipe 4', 'User Tipe 5'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-denah', component: ListDenahComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-tambah-responden-undangan', component: ListTambahRespondenUndanganComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-kirim-materi', component: ListKirimMateriComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Materi'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-unggah-materi', component: ListUnggahMateriComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Materi', 'User Tipe 1','User Tipe 2', 'User Tipe 3'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-daftar-materi', component: ListDaftarMateriComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['User Tipe 1','User Tipe 2', 'User Tipe 3'],
                            title : 'MO Dashboard' 
                            
                        }
                    },
                    { 
                        path: 'list-ubah-data-pengguna', component: ListUbahDataPenggunaComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-master-data', component: ListMasterDataComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-jenis-sidang', component: ListJenisSidangComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-jabatan', component: ListJabatanComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-role', component: ListRoleComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-lokasi', component: ListLokasiComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-group', component: ListGroupComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-unggah-kirim-risalah', component: ListUnggahKirimRisalahComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Risalah'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-tambah-responden-risalah', component: ListTambahRespondenRisalahComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Risalah'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-ubah-risalah', component: ListUbahRisalahComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Risalah'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-risalah-user', component: ListRisalahUserComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['User Tipe 1', 'User Tipe 2', 'User Tipe 3'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-laporan-risalah', component: ListLaporanRisalahComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Risalah'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-survey', component: ListSurveyComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-kirim-survey', component: ListKirimSurveyComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-laporan-survey', component: ListLaporanSurveyComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'list-download-aplikasi', component:ListDownloadAplikasiComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin','Admin Sidang','Admin Risalah','Admin Materi','User Tipe 1','User Tipe 2','User Tipe 3','User Tipe 4','User Tipe 5'],
                            title : 'MO Dashboard' 
                        }
                    },

                ]
            },
          
            {
                path: 'forms',
                children: [
                    { path: '', redirectTo: 'forms-tambah-pengguna', pathMatch: 'full' },
                    // { path: '**', component: PageNotFoundAdminComponent, data: { 
                        
                    //     title : 'MO Dashboard' 
                    //  }},
                    { 
                        path: 'forms-tambah-pengguna', component: FormsTambahPenggunaComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'forms-tambah-undangan', component: FormsTambahUndanganComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin', 'Admin Sidang'],
                            title : 'MO Dashboard' 
                        }
                    },
                    { 
                        path: 'forms-edit-status-undangan/:id', component: FormsEditStatusUndanganComponent, 
                        //canActivate: [AuthGuard],
                        data: { 
                            //expectedRole: ['Super Admin'],
                            title : 'MO Dashboard' 
                        }
                    },

                        // title: ':: MO Dashboard Form Validations Forms' } },
                        // title: ':: MO Dashboard Form Basic Forms' } }
                ]
            },
            {
                path: 'pages',
                children: [
                    { path: '', redirectTo: 'page-profile', pathMatch: 'full' },
                    // { path: '**', component: PageNotFoundAdminComponent, data: { 
                        
                    //     title : 'MO Dashboard' 
                    //  }},
                    { 
                        path: 'page-profile', component: PageProfileComponent,
                        //canActivate: [AuthGuard], 
                        data: { 
                            //expectedRole: ['Super Admin','Admin Sidang', 'Admin Materi', 'Admin Risalah', 'User Tipe 1', 'User Tipe 2', 'User Tipe 3', 'User Tipe 4', 'User Tipe 5'],
                            title : 'MO Dashboard' 
                        }
                    },
                ]
            },
            // {
            //     path: 'vcon',
            //     children: [
            //         { path: '', redirectTo: 'meeting', pathMatch: 'full' },
            //         { path: 'meeting', component: MeetingComponent, data: { 
                        
            //            title : 'MO Dashboard' 
            //         }},
            //             // title: '' } },
                    
            //             // title: '' } },
            //     ]
            // },         
            
        ]
    },
    
];

export const routing = RouterModule.forChild(routes);