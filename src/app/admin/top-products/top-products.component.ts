import { Component, OnInit,Input} from '@angular/core';
declare var require: any;
import { EChartOption } from 'echarts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASE_URL } from '../../constants'

var moment = require('moment')
@Component({
    selector: 'app-top-products',
    templateUrl: './top-products.component.html',
    styleUrls: ['./top-products.component.css']
})
export class TopProductsComponent implements OnInit {

    @Input()     
    customTitle: string;

    public stackedBarChart: EChartOption = {};
    public judul : string = '';
    public statistikUndangan : any = [];
    public xAxis : any = [];
    public xAxis2 : any = [];
    public xAxis3 : any = [];
    public xAxis4 : any = [];
    public xAxis5 : any = [];
    public xAxis6 : any = [];
    public xAxis7 : any = [];
    public statistikPengguna : any = [];
    public statistikMateri : any = [];
    public filterTahunMateri : any = [];
    public filterTahunRisalah : any = [];
    public filterTahunUndangan : any = [];
    // public tahunMateri : string = moment().format("YYYY");
    // public tahunRisalah : string = moment().format("YYYY");
    public tahunMateri : string = moment().format("YYYY");
    public tahunRisalah : string = moment().format("YYYY");
    public tahunUndangan : string = moment().format("YYYY");
    public statistikRisalah : any = [];
    public statistikSifatMateri : any = [];
    public statistikSifatRisalah : any = [];
    public statistikCounterRisalah : any = [];
    public bulan : any = [];
    public mediaQuery : any;
    // public superAdmin : string;
    constructor(private http : HttpClient) {
         
    }

    ngOnInit() {
        this.mediaQuery = window.matchMedia('(max-width: 700px)')
        this.getTopProductChartOptions(this.mediaQuery) // Call listener function at run time
        this.mediaQuery.addListener(this.getTopProductChartOptions(this.mediaQuery)) // Attach listener function on state changes

        this.judul = this.customTitle;
        // console.log('tahun ini ', this.tahunMateri);
        // console.log('tahun ini ', this.tahunRisalah);
        // console.log('tahun ini ', this.tahunUndangan);
        this.filterTahunUndangan = [
            {
                'id': '1',
                "tahun": '2020'
            },
            {
                'id': '2',
                "tahun": '2021'
            }
        ]
        // console.log('tahun undangan', this.filterTahunUndangan)

        const headers = new HttpHeaders()
			.set('Content-Type', 'application/json')
			.set('Accept', 'application/json')
			.set('Access-Control-Allow-Headers', 'Content-Type')
			.set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));
		    this.http
			.get(API_BASE_URL+'dashboard-module/getStatistikUndangan/'+this.tahunUndangan, { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get statistik undangan', res.result)
                this.statistikUndangan = res.result
                this.xAxis = res.xAxis
                this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
                // console.log('statistik',res.result)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getTotalPengguna', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get total pengguna', res.result)
                this.statistikPengguna = res.result
                this.xAxis2 = res.result
                // this.stackedBarChart = this.getTopProductChartOptions();
                // console.log('pengguna',res.result)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getStatistikMateri', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get statistik materi', res.result)
                this.statistikMateri = res.result
                this.xAxis3 = res.result

                let idTahun = this.statistikMateri.map((item: any, index: number) => {

                    return {
                        ...item,
                        id : index+1
                    }

                })

                this.filterTahunMateri = Array.from(idTahun.reduce((m, t) => m.set(t.tahun, t), new Map()).values())

                // console.log('id tahun', this.filterTahunMateri)

                // this.stackedBarChart = this.getTopProductChartOptions();
                // console.log('pengguna',res.result)

			});
            this.http
			.get(API_BASE_URL+'dashboard-module/getStatistikRisalah', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get statistik risalah', res.result)
                this.statistikRisalah = res.result
                this.xAxis4 = res.result
                this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
                // console.log('risalah',res.result)
                let idTahun = this.statistikRisalah.map((item: any, index: number) => {

                    return {
                        ...item,
                        id : index+1
                    }

                })

                this.filterTahunRisalah = Array.from(idTahun.reduce((m, t) => m.set(t.tahun, t), new Map()).values())

                // console.log('id tahun risalah', this.filterTahunRisalah)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getStatistikSifatMateri', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get statistik sifat materi', res.result)
                this.statistikSifatMateri = res.result
                this.xAxis5 = res.result
                // this.stackedBarChart = this.getTopProductChartOptions();
                // console.log('materi',res.result)
			});
            this.http
			.get(API_BASE_URL+'dashboard-module/getStatistikSifatRisalah', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get statistik sifat risalah', res.result)
                this.statistikSifatRisalah = res.result
                this.xAxis6 = res.result
                this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
                // console.log('risalah',res.result)
            });
            this.http
			.get(API_BASE_URL+'dashboard-module/getTotalCounterRisalah', { headers })
			.toPromise()
			.then((res: any) => {
				// console.log('get total counter risalah', res.result)
                this.statistikCounterRisalah = res.result
                this.xAxis7 = res.result
                this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
                // console.log('risalah',res.result)
            }); 

            // this.changeTahun(this.tahun)        
    }

    changeTahunMateri(event){
        // console.log('tahun materi', event.target.value)
        this.tahunMateri = event.target.value
        
        this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
    }

    changeTahunRisalah(event){
        // console.log('tahun risalah', event.target.value)
        this.tahunRisalah = event.target.value
        
        this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
    }

    changeTahunUndangan(event){
        // console.log('tahun undangan', event.target.value)
        this.tahunUndangan = event.target.value
        this.callApiStatistikUndangan(this.tahunUndangan)
        // this.stackedBarChart = this.getTopProductChartOptions();
    }

    callApiStatistikUndangan(tahun: any){
        const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Access-Control-Allow-Headers', 'Content-Type')
        .set("Authorization", 'Basic ' + btoa('ekab40:Ek4b#116'));

        this.http
        .get(API_BASE_URL+'dashboard-module/getStatistikUndangan/'+tahun, { headers })
        .toPromise()
        .then((res: any) => {
            // console.log('get statistik undangan', res.result)
            this.statistikUndangan = res.result
            this.xAxis = res.xAxis
            this.stackedBarChart = this.getTopProductChartOptions(this.mediaQuery);
            // console.log('statistik',res.result)
        });
    }

    
    
    getTopProductChartOptions(pixel) {

        // console.log('tester judul ', this.judul)
        
        if(this.judul === 'Statistik Undangan') {
            // console.log('statistik undangan', this.statistikUndangan)

            let dataRapatTerbatas = []
            Object.keys(this.statistikUndangan).map((key,index)=> {
                Object.keys(this.statistikUndangan[key]).map((key2,index2)=> {
                    if (key2 == '1') {
                        dataRapatTerbatas.push(Number(this.statistikUndangan[key][key2]))
                     }
                })
            })
            let dataSidangKabinetParipurna = []
            Object.keys(this.statistikUndangan).map((key,index)=> {
                Object.keys(this.statistikUndangan[key]).map((key2,index2)=> {
                    if (key2 == '2') {
                        dataSidangKabinetParipurna.push(Number(this.statistikUndangan[key][key2]))
                     }
                })
            })
            let dataPertemuanLain = []
            Object.keys(this.statistikUndangan).map((key,index)=> {
                Object.keys(this.statistikUndangan[key]).map((key2,index2)=> {
                    if (key2 == '3') {
                        dataPertemuanLain.push(Number(this.statistikUndangan[key][key2]))
                     }
                })
            })
            let dataTpa = []
            Object.keys(this.statistikUndangan).map((key,index)=> {
                Object.keys(this.statistikUndangan[key]).map((key2,index2)=> {
                    if (key2 == '4') {
                        dataTpa.push(Number(this.statistikUndangan[key][key2]))
                     }
                })
            })
            let dataInternal = []
            Object.keys(this.statistikUndangan).map((key,index)=> {
                Object.keys(this.statistikUndangan[key]).map((key2,index2)=> {
                    if (key2 == '5') {
                        dataInternal.push(Number(this.statistikUndangan[key][key2]))
                     }
                })
            })
            // console.log(this.statistikUndangan)
            // console.log(dataRapatTerbatas)
            // console.log(dataSidangKabinetParipurna)
            // console.log(dataPertemuanLain)
            // console.log(dataTpa)
            let options : any = {

                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: ['Rapat Terbatas', 'Sidang Kabinet Paripurna', 'Pertemuan Lain','TPA','Rapat Internal'] ,
                    right: '2%',
                    textStyle: {
                        color: "#C2C2C2",
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: this.xAxis,
                        axisLine: {
                            show: false
                        },
                        axisLabel: {
                            textStyle: {
                                color: "#C2C2C2",
                            },
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        minInterval: 25,
                        splitLine: {
                            lineStyle: {
                                type: 'dotted'
                            }
                        },
                        axisLine: {
                            show: false
                        },
                        axisLabel: {
                            formatter: function (value, index) {
                                // if (value > 0) {
                                //     return (value / 1000) + ' K';
                                // } else {
                                //     return 0;
                                // }
                                return value
                            },
                            textStyle: {
                                color: "#C2C2C2",
                            }
                        }
                    }
                ],
                series: [
                    {
                        name: 'Rapat Terbatas',
                        type: 'bar',
                        stack: 'Gedgets',
                        data: dataRapatTerbatas,
                        itemStyle: {
                            color: "#ffe66d"
                        },
                        barWidth: "40px"
                    },
                    {
                        name: 'Sidang Kabinet Paripurna',
                        type: 'bar',
                        stack: 'Gedgets',
                        data: dataSidangKabinetParipurna,
                        itemStyle: {
                            color: "#4ea8de"
                        },
                        barWidth: "40px"
                    },
                    {
                        name: 'Pertemuan Lain',
                        type: 'bar',
                        stack: 'Gedgets',
                        data: dataPertemuanLain,
                        itemStyle: {
                            color: "#32e0c4"
                        },
                        barWidth: "40px"
                    },
                    {
                        name: 'TPA',
                        type: 'bar',
                        stack: 'Gedgets',
                        data: dataTpa,
                        itemStyle: {
                            color: "#e26d5c"
                            
                        },
                        barWidth: "40px"
                    },
                    {
                        name: 'Rapat Internal',
                        type: 'bar',
                        stack: 'Gedgets',
                        data: dataInternal,
                        itemStyle: {
                            color: '#C9463D'
                            
                        },
                        barWidth: "40px"
                    }
                ]
            }
            // console.log('option',options)

            return options;
        }

        if(this.judul === 'Statistik Total Pengguna') {
            let superAdmin:string;
            let adminSidang:string;
            let adminRisalah:string;
            let adminMateri:string;
            let user1:string;
            let user2:string;
            let user3:string;
            let user4:string;
            let user5:string;

            // console.log('statistik pengguna', this.statistikPengguna)
            this.statistikPengguna.forEach(function (getNamaRole) {
                if (getNamaRole.nama_role == 'Super Admin') {
                    superAdmin = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'Admin Sidang') {
                    adminSidang = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'Admin Materi') {
                    adminMateri = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'Admin Risalah') {
                    adminRisalah = getNamaRole.total_user                    
                }
                if (getNamaRole.nama_role == 'User Tipe 1') {
                    user1 = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'User Tipe 2') {
                    user2 = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'User Tipe 3') {
                    user3 = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'User Tipe 4') {
                    user4 = getNamaRole.total_user
                }
                if (getNamaRole.nama_role == 'User Tipe 5') {
                    user5 = getNamaRole.total_user
                }
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                xAxis: {
                  show: true,
                  axisTick:{
                      show : true
                  },
                  axisLabel:{
                    show : true
                }, 
                  type: 'category',
                  data: ['User Tipe 1', 'User Tipe 2','User Tipe 3', 'User Tipe 4', 'User Tipe 5'],
                },
                yAxis: {
                  type: 'value',
                },
                series: [
                  {
                    // data: [user1, user2, user3, user4, user5 ],
                    barWidth: "40px",
                    data: [
                        {
                            value: user1,
                            itemStyle: {color: '#e26d5c'},
                        },
                        {
                            value:user2,
                            itemStyle: {color: '#32e0c4'},
                        },
                        {
                            value: user3,
                            itemStyle: {color: '#4ea8de'},
                        },
                        {
                            value: user4,
                            itemStyle: {color: 'orange'},
                        },
                        {
                            value: user5,
                            itemStyle: {color: '#fddb3a'},
                        }
                    ],

                    type: 'bar',
                  },
                ],
              };
            return options;
        }
        
        if(this.judul === 'Statistik Total Materi') {

            let januari:string;
            let februari:string;
            let maret:string
            let april:string;
            let mei:string;
            let juni:string;
            let juli:string;
            let agustus:string;
            let september:string;
            let oktober:string;
            let november:string;
            let desember:string;

            // console.log('statistik materi', this.statistikMateri)

            if(this.tahunMateri === '2020'){
                // console.log('masuk 2020')

                this.statistikMateri.filter((item: any) => {
                    if(item.tahun !== '2020'){
                        return false
                    }

                    return true
                }).map((data: any) => {
                    // console.log('data', data)
                    
                    if (data.bulan == 'Januari') {
                        // console.log('januari')
                        return januari = data.total_materi
                    } 
                    if (data.bulan == 'Februari') {
                        // console.log('februari')
                        return februari = data.total_materi
                    } 
                    if (data.bulan == 'Maret') {
                        // console.log('maret')
                        return maret = data.total_materi
                    } 
                    if (data.bulan == 'April') {
                        // console.log('april')
                        return april = data.total_materi
                    } 
                    if (data.bulan == 'Mei') {
                        // console.log('mei')
                        return mei = data.total_materi
                    } 
                    if (data.bulan == 'Juni') {
                        // console.log('juni')
                        return juni = data.total_materi
                    } 
                    if (data.bulan == 'Juli') {
                        // console.log('juli')
                        return juli = data.total_materi
                    } 
                    if (data.bulan == 'Agustus') {
                        // console.log('agustus')
                        agustus = data.total_materi
                    } 
                    if (data.bulan == 'September') {
                        // console.log('september')
                        return september = data.total_materi
                    }
                    if (data.bulan == 'Oktober') {
                        // console.log('oktober')
                        return oktober = data.total_materi
                    } 
                    if (data.bulan == 'November') {
                        // console.log('november')
                        return november = data.total_materi
                    }    
                    if (data.bulan == 'Desember') {
                        // console.log('desember')
                        return desember = data.total_materi
                    }
                })

                // console.log('value januari', januari)

                let options : any = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        left: '1%',
                        right: '1%',
                        bottom: '2%',
                        containLabel: true
                    },
                    xAxis: {
                        show: true,
                        axisTick:{
                            show : true
                        },
                        axisLabel:{
                          show : true
                      }, 
                        type: 'category',                    
                        data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                      },
                      yAxis: {
                        type: 'value',
                      },
                    series: [
                      {
                        // data: [user1, user2, user3, user4, user5 ],
                        barWidth: "40px",
                        data: [
                            {
                                value: januari,
                                itemStyle: {color: '#4ea8de'},
                            },
                            {
                                value: februari,
                                itemStyle: {color: '#FFFF00'},
                            },
                            {
                                value: maret,
                                itemStyle: {color: '#90EE90'},
                            },
                            {
                                value: april,
                                itemStyle: {color: '#ADD8E6'},
                            },
                            {
                                value: mei,
                                itemStyle: {color: '#D3D3D3'},
                            },
                            {
                                value: juni,
                                itemStyle: {color: '#40E0D0'},
                            },
                            {
                                value: juli,
                                itemStyle: {color: '#FFA500'},
                            },
                            {
                                value: agustus,
                                itemStyle: {color: '#BDB76B'},
                            },
                            {
                                value: september,
                                itemStyle: {color: '#F4A460'},
                            },
                            {
                                value: oktober,
                                itemStyle: {color: '#E9967A'},
                            },
                            {
                                value: november,
                                itemStyle: {color: '#e26d5c'},
                            },
                            {
                                value: desember,
                                itemStyle: {color: '#D2691E'},
                            }
                        ],
    
                        type: 'bar',
                      },
                    ],
                  };
                return options;

            } else if(this.tahunMateri === '2021'){

                if(pixel.matches){

                    this.statistikMateri.filter((item: any) => {
                        if(item.tahun !== '2021'){
                            return false
                        }
                        
                        // console.log('masuk 2021')
                        return true
                    }).map((data: any) => {
                        // console.log('data', data)
                        
                        if (data.bulan == 'Januari') {
                            // console.log('januari')
                            return januari = data.total_materi
                        } 
                        if (data.bulan == 'Februari') {
                            // console.log('februari')
                            return februari = data.total_materi
                        } 
                        if (data.bulan == 'Maret') {
                            // console.log('maret')
                            return maret = data.total_materi
                        } 
                        if (data.bulan == 'April') {
                            // console.log('april')
                            return april = data.total_materi
                        } 
                        if (data.bulan == 'Mei') {
                            // console.log('mei')
                            return mei = data.total_materi
                        } 
                        if (data.bulan == 'Juni') {
                            // console.log('juni')
                            return juni = data.total_materi
                        } 
                        if (data.bulan == 'Juli') {
                            // console.log('juli')
                            return juli = data.total_materi
                        } 
                        if (data.bulan == 'Agustus') {
                            // console.log('agustus')
                            agustus = data.total_materi
                        } 
                        if (data.bulan == 'September') {
                            // console.log('september')
                            return september = data.total_materi
                        }
                        if (data.bulan == 'Oktober') {
                            // console.log('oktober')
                            return oktober = data.total_materi
                        } 
                        if (data.bulan == 'November') {
                            // console.log('november')
                            return november = data.total_materi
                        }    
                        if (data.bulan == 'Desember') {
                            // console.log('desember')
                            return desember = data.total_materi
                        }
                    })
    
                    // console.log('value januari', januari)
    
                    // console.log('statistik materi', this.statistikMateri)
    
                    let options : any = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'shadow'
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '1%',
                            bottom: '2%',
                            containLabel: true
                        },
                        xAxis: {
                            show: true,
                            axisTick:{
                                show : true
                            },
                            axisLabel:{
                              show : true
                          }, 
                            type: 'category',                    
                            data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                          },
                          yAxis: {
                            type: 'value',
                          },
                        series: [
                          {
                            // data: [user1, user2, user3, user4, user5 ],
                            barWidth: "20px",
                            data: [
                                {
                                    value: januari,
                                    itemStyle: {color: '#4ea8de'},
                                },
                                {
                                    value: februari,
                                    itemStyle: {color: '#FFFF00'},
                                },
                                {
                                    value: maret,
                                    itemStyle: {color: '#90EE90'},
                                },
                                {
                                    value: april,
                                    itemStyle: {color: '#ADD8E6'},
                                },
                                {
                                    value: mei,
                                    itemStyle: {color: '#D3D3D3'},
                                },
                                {
                                    value: juni,
                                    itemStyle: {color: '#40E0D0'},
                                },
                                {
                                    value: juli,
                                    itemStyle: {color: '#FFA500'},
                                },
                                {
                                    value: agustus,
                                    itemStyle: {color: '#BDB76B'},
                                },
                                {
                                    value: september,
                                    itemStyle: {color: '#F4A460'},
                                },
                                {
                                    value: oktober,
                                    itemStyle: {color: '#E9967A'},
                                },
                                {
                                    value: november,
                                    itemStyle: {color: '#e26d5c'},
                                },
                                {
                                    value: desember,
                                    itemStyle: {color: '#D2691E'},
                                }
                            ],
        
                            type: 'bar',
                          },
                        ],
                    };
                    return options;

                } else {

                    this.statistikMateri.filter((item: any) => {
                        if(item.tahun !== '2021'){
                            return false
                        }
                        
                        // console.log('masuk 2021')
                        return true
                    }).map((data: any) => {
                        // console.log('data', data)
                        
                        if (data.bulan == 'Januari') {
                            // console.log('januari')
                            return januari = data.total_materi
                        } 
                        if (data.bulan == 'Februari') {
                            // console.log('februari')
                            return februari = data.total_materi
                        } 
                        if (data.bulan == 'Maret') {
                            // console.log('maret')
                            return maret = data.total_materi
                        } 
                        if (data.bulan == 'April') {
                            // console.log('april')
                            return april = data.total_materi
                        } 
                        if (data.bulan == 'Mei') {
                            // console.log('mei')
                            return mei = data.total_materi
                        } 
                        if (data.bulan == 'Juni') {
                            // console.log('juni')
                            return juni = data.total_materi
                        } 
                        if (data.bulan == 'Juli') {
                            // console.log('juli')
                            return juli = data.total_materi
                        } 
                        if (data.bulan == 'Agustus') {
                            // console.log('agustus')
                            agustus = data.total_materi
                        } 
                        if (data.bulan == 'September') {
                            // console.log('september')
                            return september = data.total_materi
                        }
                        if (data.bulan == 'Oktober') {
                            // console.log('oktober')
                            return oktober = data.total_materi
                        } 
                        if (data.bulan == 'November') {
                            // console.log('november')
                            return november = data.total_materi
                        }    
                        if (data.bulan == 'Desember') {
                            // console.log('desember')
                            return desember = data.total_materi
                        }
                    })
    
                    // console.log('value januari', januari)
    
                    // console.log('statistik materi', this.statistikMateri)
    
                    let options : any = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'shadow'
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '1%',
                            bottom: '2%',
                            containLabel: true
                        },
                        xAxis: {
                            show: true,
                            axisTick:{
                                show : true
                            },
                            axisLabel:{
                              show : true
                          }, 
                            type: 'category',                    
                            data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                          },
                          yAxis: {
                            type: 'value',
                          },
                        series: [
                          {
                            // data: [user1, user2, user3, user4, user5 ],
                            barWidth: "40px",
                            data: [
                                {
                                    value: januari,
                                    itemStyle: {color: '#4ea8de'},
                                },
                                {
                                    value: februari,
                                    itemStyle: {color: '#FFFF00'},
                                },
                                {
                                    value: maret,
                                    itemStyle: {color: '#90EE90'},
                                },
                                {
                                    value: april,
                                    itemStyle: {color: '#ADD8E6'},
                                },
                                {
                                    value: mei,
                                    itemStyle: {color: '#D3D3D3'},
                                },
                                {
                                    value: juni,
                                    itemStyle: {color: '#40E0D0'},
                                },
                                {
                                    value: juli,
                                    itemStyle: {color: '#FFA500'},
                                },
                                {
                                    value: agustus,
                                    itemStyle: {color: '#BDB76B'},
                                },
                                {
                                    value: september,
                                    itemStyle: {color: '#F4A460'},
                                },
                                {
                                    value: oktober,
                                    itemStyle: {color: '#E9967A'},
                                },
                                {
                                    value: november,
                                    itemStyle: {color: '#e26d5c'},
                                },
                                {
                                    value: desember,
                                    itemStyle: {color: '#D2691E'},
                                }
                            ],
        
                            type: 'bar',
                          },
                        ],
                    };
                    return options;
                }
                
            }

            this.statistikMateri.forEach(function (getMateri) {
                if (getMateri.bulan == 'Januari') {
                    januari = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Februari') {
                    februari = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Maret') {
                    maret = getMateri.total_materi
                } 
                if (getMateri.bulan == 'April') {
                    april = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Mei') {
                    mei = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Juni') {
                    juni = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Juli') {
                    juli = getMateri.total_materi
                } 
                if (getMateri.bulan == 'Agustus') {
                    agustus = getMateri.total_materi
                } 
                if (getMateri.bulan == 'September') {
                    september = getMateri.total_materi
                }
                if (getMateri.bulan == 'Oktober') {
                    oktober = getMateri.total_materi
                } 
                if (getMateri.bulan == 'November') {
                    november = getMateri.total_materi
                }    
                if (getMateri.bulan == 'Desember') {
                    desember = getMateri.total_materi
                }             
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                xAxis: {
                    show: true,
                    axisTick:{
                        show : true
                    },
                    axisLabel:{
                      show : true
                  }, 
                    type: 'category',                    
                    data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                  },
                  yAxis: {
                    type: 'value',
                  },
                series: [
                  {
                    // data: [user1, user2, user3, user4, user5 ],
                    barWidth: "40px",
                    data: [
                        {
                            value: januari,
                            itemStyle: {color: '#4ea8de'},
                        },
                        {
                            value: februari,
                            itemStyle: {color: '#FFFF00'},
                        },
                        {
                            value: maret,
                            itemStyle: {color: '#90EE90'},
                        },
                        {
                            value: april,
                            itemStyle: {color: '#ADD8E6'},
                        },
                        {
                            value: mei,
                            itemStyle: {color: '#D3D3D3'},
                        },
                        {
                            value: juni,
                            itemStyle: {color: '#40E0D0'},
                        },
                        {
                            value: juli,
                            itemStyle: {color: '#FFA500'},
                        },
                        {
                            value: agustus,
                            itemStyle: {color: '#BDB76B'},
                        },
                        {
                            value: september,
                            itemStyle: {color: '#F4A460'},
                        },
                        {
                            value: oktober,
                            itemStyle: {color: '#E9967A'},
                        },
                        {
                            value: november,
                            itemStyle: {color: '#e26d5c'},
                        },
                        {
                            value: desember,
                            itemStyle: {color: '#D2691E'},
                        }
                    ],

                    type: 'bar',
                  },
                ],
            };
            return options;
        }
        
        if(this.judul === 'Statistik Total Pengantar Presiden') {

            let januari:string;
            let februari:string;
            let maret:string
            let april:string;
            let mei:string;
            let juni:string;
            let juli:string;
            let agustus:string;
            let september:string;
            let oktober:string;
            let november:string;
            let desember:string;

            // console.log('statistik risalah', this.statistikRisalah)

            if(this.tahunRisalah === '2020'){
                // console.log('masuk 2020')

                this.statistikRisalah.filter((item: any) => {
                    if(item.tahun !== '2020'){
                        return false
                    }

                    return true
                }).map((data: any) => {
                    // console.log('data', data)
                    
                    if (data.bulan == 'Januari') {
                        // console.log('januari')
                        return januari = data.total_risalah
                    } 
                    if (data.bulan == 'Februari') {
                        // console.log('februari')
                        return februari = data.total_risalah
                    } 
                    if (data.bulan == 'Maret') {
                        // console.log('maret')
                        return maret = data.total_risalah
                    } 
                    if (data.bulan == 'April') {
                        // console.log('april')
                        return april = data.total_risalah
                    } 
                    if (data.bulan == 'Mei') {
                        // console.log('mei')
                        return mei = data.total_risalah
                    } 
                    if (data.bulan == 'Juni') {
                        // console.log('juni')
                        return juni = data.total_risalah
                    } 
                    if (data.bulan == 'Juli') {
                        // console.log('juli')
                        return juli = data.total_risalah
                    } 
                    if (data.bulan == 'Agustus') {
                        // console.log('agustus')
                        agustus = data.total_risalah
                    } 
                    if (data.bulan == 'September') {
                        // console.log('september')
                        return september = data.total_risalah
                    }
                    if (data.bulan == 'Oktober') {
                        // console.log('oktober')
                        return oktober = data.total_risalah
                    } 
                    if (data.bulan == 'November') {
                        // console.log('november')
                        return november = data.total_risalah
                    }    
                    if (data.bulan == 'Desember') {
                        // console.log('desember')
                        return desember = data.total_risalah
                    }
                })

                // console.log('value januari', januari)

                let options : any = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        left: '1%',
                        right: '1%',
                        bottom: '2%',
                        containLabel: true
                    },
                    xAxis: {
                        show: true,
                        axisTick:{
                            show : true
                        },
                        axisLabel:{
                          show : true
                      }, 
                        type: 'category',                    
                        data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                      },
                      yAxis: {
                        type: 'value',
                      },
                    series: [
                      {
                        // data: [user1, user2, user3, user4, user5 ],
                        barWidth: "40px",
                        data: [
                            {
                                value: januari,
                                itemStyle: {color: '#4ea8de'},
                            },
                            {
                                value: februari,
                                itemStyle: {color: '#FFFF00'},
                            },
                            {
                                value: maret,
                                itemStyle: {color: '#90EE90'},
                            },
                            {
                                value: april,
                                itemStyle: {color: '#ADD8E6'},
                            },
                            {
                                value: mei,
                                itemStyle: {color: '#D3D3D3'},
                            },
                            {
                                value: juni,
                                itemStyle: {color: '#40E0D0'},
                            },
                            {
                                value: juli,
                                itemStyle: {color: '#FFA500'},
                            },
                            {
                                value: agustus,
                                itemStyle: {color: '#BDB76B'},
                            },
                            {
                                value: september,
                                itemStyle: {color: '#F4A460'},
                            },
                            {
                                value: oktober,
                                itemStyle: {color: '#E9967A'},
                            },
                            {
                                value: november,
                                itemStyle: {color: '#e26d5c'},
                            },
                            {
                                value: desember,
                                itemStyle: {color: '#D2691E'},
                            }
                        ],
    
                        type: 'bar',
                      },
                    ],
                  };
                return options;

            } else if(this.tahunRisalah === '2021'){
                
                this.statistikRisalah.filter((item: any) => {
                    if(item.tahun !== '2021'){
                        return false
                    }
                    
                    // console.log('masuk 2021')
                    return true
                }).map((data: any) => {
                    // console.log('data', data)
                    
                    if (data.bulan == 'Januari') {
                        // console.log('januari')
                        return januari = data.total_risalah
                    } 
                    if (data.bulan == 'Februari') {
                        // console.log('februari')
                        return februari = data.total_risalah
                    } 
                    if (data.bulan == 'Maret') {
                        // console.log('maret')
                        return maret = data.total_risalah
                    } 
                    if (data.bulan == 'April') {
                        // console.log('april')
                        return april = data.total_risalah
                    } 
                    if (data.bulan == 'Mei') {
                        // console.log('mei')
                        return mei = data.total_risalah
                    } 
                    if (data.bulan == 'Juni') {
                        // console.log('juni')
                        return juni = data.total_risalah
                    } 
                    if (data.bulan == 'Juli') {
                        // console.log('juli')
                        return juli = data.total_risalah
                    } 
                    if (data.bulan == 'Agustus') {
                        // console.log('agustus')
                        agustus = data.total_risalah
                    } 
                    if (data.bulan == 'September') {
                        // console.log('september')
                        return september = data.total_risalah
                    }
                    if (data.bulan == 'Oktober') {
                        // console.log('oktober')
                        return oktober = data.total_risalah
                    } 
                    if (data.bulan == 'November') {
                        // console.log('november')
                        return november = data.total_risalah
                    }    
                    if (data.bulan == 'Desember') {
                        // console.log('desember')
                        return desember = data.total_risalah
                    }
                })

                // console.log('value januari', januari)

                // console.log('statistik materi', this.statistikMateri)

                let options : any = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        left: '1%',
                        right: '1%',
                        bottom: '2%',
                        containLabel: true
                    },
                    xAxis: {
                        show: true,
                        axisTick:{
                            show : true
                        },
                        axisLabel:{
                          show : true
                      }, 
                        type: 'category',                    
                        data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                      },
                      yAxis: {
                        type: 'value',
                      },
                    series: [
                      {
                        // data: [user1, user2, user3, user4, user5 ],
                        barWidth: "40px",
                        data: [
                            {
                                value: januari,
                                itemStyle: {color: '#4ea8de'},
                            },
                            {
                                value: februari,
                                itemStyle: {color: '#FFFF00'},
                            },
                            {
                                value: maret,
                                itemStyle: {color: '#90EE90'},
                            },
                            {
                                value: april,
                                itemStyle: {color: '#ADD8E6'},
                            },
                            {
                                value: mei,
                                itemStyle: {color: '#D3D3D3'},
                            },
                            {
                                value: juni,
                                itemStyle: {color: '#40E0D0'},
                            },
                            {
                                value: juli,
                                itemStyle: {color: '#FFA500'},
                            },
                            {
                                value: agustus,
                                itemStyle: {color: '#BDB76B'},
                            },
                            {
                                value: september,
                                itemStyle: {color: '#F4A460'},
                            },
                            {
                                value: oktober,
                                itemStyle: {color: '#E9967A'},
                            },
                            {
                                value: november,
                                itemStyle: {color: '#e26d5c'},
                            },
                            {
                                value: desember,
                                itemStyle: {color: '#D2691E'},
                            }
                        ],
    
                        type: 'bar',
                      },
                    ],
                };
                return options;
            }

            this.statistikRisalah.forEach(function (getRisalah) {
                if (getRisalah.bulan == 'Januari') {
                    januari = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Februari') {
                    februari = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Maret') {
                    maret = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'April') {
                    april = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Mei') {
                    mei = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Juni') {
                    juni = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Juli') {
                    juli = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'Agustus') {
                    agustus = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'September') {
                    september = getRisalah.total_risalah
                }
                if (getRisalah.bulan == 'Oktober') {
                    oktober = getRisalah.total_risalah
                } 
                if (getRisalah.bulan == 'November') {
                    november = getRisalah.total_risalah
                }    
                if (getRisalah.bulan == 'Desember') {
                    desember = getRisalah.total_risalah
                }             
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                xAxis: {
                    show: true,
                    axisTick:{
                        show : true
                    },
                    axisLabel:{
                      show : true
                  }, 
                    type: 'category',                    
                    data: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                  },
                  yAxis: {
                    type: 'value',
                  },
                series: [
                  {
                    // data: [user1, user2, user3, user4, user5 ],
                    barWidth: "40px",
                    data: [
                        {
                            value: januari,
                            itemStyle: {color: '#4ea8de'},
                        },
                        {
                            value: februari,
                            itemStyle: {color: '#FFFF00'},
                        },
                        {
                            value: maret,
                            itemStyle: {color: '#90EE90'},
                        },
                        {
                            value: april,
                            itemStyle: {color: '#ADD8E6'},
                        },
                        {
                            value: mei,
                            itemStyle: {color: '#D3D3D3'},
                        },
                        {
                            value: juni,
                            itemStyle: {color: '#40E0D0'},
                        },
                        {
                            value: juli,
                            itemStyle: {color: '#FFA500'},
                        },
                        {
                            value: agustus,
                            itemStyle: {color: '#BDB76B'},
                        },
                        {
                            value: september,
                            itemStyle: {color: '#F4A460'},
                        },
                        {
                            value: oktober,
                            itemStyle: {color: '#E9967A'},
                        },
                        {
                            value: november,
                            itemStyle: {color: '#e26d5c'},
                        },
                        {
                            value: desember,
                            itemStyle: {color: '#D2691E'},
                        }
                    ],

                    type: 'bar',
                  },
                ],
              };
            return options;
        }

        if(this.judul === 'Statistik Sifat Materi') {
            
            let Umum:string;
            let Khusus:string;
            let Rahasia:string;

            // console.log('statistik sifat materi', this.statistikSifatMateri)

            this.statistikSifatMateri.forEach(function (getSifatMateri) {
                if (getSifatMateri.sifat_materi == 'umum') {
                    Umum = getSifatMateri.jumlah
                }   
                if (getSifatMateri.sifat_materi == 'khusus') {
                    Khusus = getSifatMateri.jumlah
                } 
                if (getSifatMateri.sifat_materi == 'rahasia') {
                    Rahasia = getSifatMateri.jumlah
                }              
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                color: ["#e26d5c", "#32e0c4", "#4ea8de"],
			    title: {
                    x: 'center',
                    textStyle: {
                    color: "#C2C2C2"
                    }
			    },
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                legend: {
                    orient: 'horizontal',
                    left: 'left',
                    data: ['Umum','Khusus','Rahasia'],
                    textStyle: {
                        color: "#C2C2C2"
                    }
                },
                series: [
                    {
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            { value: Umum, name: 'Umum' },
                            { value: Khusus, name: 'Khusus' },
                            { value: Rahasia, name: 'Rahasia'}
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ],
              };
            return options;
        }       

        if(this.judul === 'Statistik Sifat Pengantar Presiden') {

            // console.log('statistik sifat risalah', this.statistikSifatRisalah)

            let distribusi:string;
            let tidak_distribusi:string;
            this.statistikSifatRisalah.forEach(function (getSifatRisalah) {
                if (getSifatRisalah.keterangan == 'Terdistribusi') {
                    distribusi = getSifatRisalah.jumlah
                } 
                if (getSifatRisalah.keterangan == 'Tidak Terdistribusi') {
                    tidak_distribusi = getSifatRisalah.jumlah
                }                
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                color: ["lightcoral", "lightgreen"],
			    title: {
                    x: 'center',
                    textStyle: {
                    color: "#C2C2C2"
                    }
			    },
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                legend: {
                    orient: 'horizontal',
                    left: 'left',
                    data: ['Terdistribusi','Tidak Terdistribusi'],
                    textStyle: {
                        color: "#C2C2C2"
                    }
                },
                series: [
                    {
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            { value: distribusi, name: 'Terdistribusi' },
                            { value: tidak_distribusi, name: 'Tidak Terdistribusi' }
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ],
              };
            return options;
        }
        
        if(this.judul === 'Statistik Cetak dan Unduh Pengantar Presiden') {

            // console.log('statistik counter risalah', this.statistikCounterRisalah)

            let Download:string;
            let Print:string;
            this.statistikCounterRisalah.forEach(function (getCounterRisalah) {
                if (getCounterRisalah.keterangan == 'download') {
                    Download = getCounterRisalah.total
                } 
                if (getCounterRisalah.keterangan == 'print') {
                    Print = getCounterRisalah.total
                }                
            });
            // console.log('data',superAdmin)
            // console.log('data',adminSidang)
            let options : any = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '2%',
                    containLabel: true
                },
                xAxis: {
                    type: 'value',
                  },
                  yAxis: {
                    show: true,
                    axisTick:{
                        show : true
                    },
                    axisLabel:{
                      show : true
                  }, 
                    type: 'category',                    
                    data: ['Unduh','Cetak']
                  },
                series: [
                  {
                    // data: [user1, user2, user3, user4, user5 ],
                    barWidth: "40px",
                    data: [
                        {
                            value: Download,
                            itemStyle: {color: '#fddb3a'},
                        },
                        {
                            value: Print,
                            itemStyle: {color: '#4ea8de'},
                        }
                    ],

                    type: 'bar',
                  },
                ],
              };
            return options;
        }
    }
}