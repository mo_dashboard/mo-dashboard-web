import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule} from '@angular/forms'
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
// import { FullCalendarModule } from 'ng-fullcalendar';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgxGalleryModule } from 'ngx-gallery';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuardService } from "./guards/auth-guard.service";
import { AuthService } from './guards/auth.service';
import { NgSelect2Module } from 'ng-select2';
// import { NgxPaginationModule } from 'ngx-pagination';
// import { Ng2SearchPipeModule } from 'ng2-search-filter';

// import { DatePipe } from '@angular/common';
import * as $ from 'jquery';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SafePipe } from './safe.pipe';
import { VconModule } from './vcon/vcon.module';
import { JawabanSurveyModule } from './jawaban-survey/jawaban-survey.module';
import { UploadMateriModule } from './upload-materi/upload-materi.module';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { PageNotFoundComponent } from './authentication/page-not-found/page-not-found.component';
// import { ListUndanganUserComponent } from './list/list-undangan-user/list-undangan-user.component';

// import { MomentModule } from 'ngx-moment';
@NgModule({
    declarations: [
        AppComponent,
        SafePipe,
        PageNotFoundComponent,
        // ListUndanganUserComponent
    ],
    imports: [ LazyLoadImageModule,
        BrowserModule,
        routing,
        NgbModule,
        FormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        RichTextEditorAllModule,
        // FullCalendarModule,
        NgMultiSelectDropDownModule.forRoot(),
        LeafletModule.forRoot(),
        NgxGalleryModule,
        HttpClientModule,
        NgSelect2Module,
        VconModule,
        JawabanSurveyModule,
        NgxSpinnerModule,
        UploadMateriModule
        // NgxPaginationModule,
        // Ng2SearchPipeModule,
        // MomentModule,
    ],
    exports:[PageNotFoundComponent],
    providers: [AuthGuardService, AuthService, NgxSpinnerService],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
