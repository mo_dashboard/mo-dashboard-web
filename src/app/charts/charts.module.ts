import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartEchartComponent } from './chart-echart/chart-echart.component';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';

import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
	declarations: [ChartEchartComponent],
	imports: [ LazyLoadImageModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule
	]
})
export class ChartsModule { }
