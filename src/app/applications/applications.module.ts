import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
// import { FullCalendarModule } from 'ng-fullcalendar';
import { RouterModule } from '@angular/router';

import { ApplicationsComponent } from './applications/applications.component';
import { AppChatComponent } from './app-chat/app-chat.component';



import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  declarations: [
    ApplicationsComponent,
    AppChatComponent
  ],
  imports: [ LazyLoadImageModule,
    CommonModule,
    NgxEchartsModule,
    NgbModule,
    RichTextEditorAllModule,
    // FullCalendarModule,
    RouterModule
  ]
})
export class ApplicationsModule { }
