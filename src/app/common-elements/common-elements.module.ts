import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertMessageComponent } from './alert-message/alert-message.component';
import { CollepsibleComponent } from './collepsible/collepsible.component';
import { AccordionComponent } from './accordion/accordion.component';


import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [ LazyLoadImageModule,
    CommonModule,
  ],
  declarations: [AlertMessageComponent, CollepsibleComponent, AccordionComponent],
    exports: [AlertMessageComponent, CollepsibleComponent, AccordionComponent]
})
export class CommonElementsModule { }
