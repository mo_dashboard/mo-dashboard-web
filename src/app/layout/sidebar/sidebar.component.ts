import { Component, Input, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ThemeService } from '../../services/theme.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService } from '../../guards/auth.service';
import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponents implements OnDestroy {
	@ViewChild('sidebar', {static: false}) sidebar: SidebarComponent
	@Input() sidebarVisible: boolean = false;
	@Input() navTab: string = "menu";
	@Input() currentActiveMenu;
	@Input() currentActiveSubMenu;
	@Output() changeNavTabEvent = new EventEmitter();
	@Output() activeInactiveMenuEvent = new EventEmitter();
	@Input() showNotifMenu: boolean = false;
	@Input() showToggleMenu: boolean = true;
	@Output() toggleSettingDropMenuEvent = new EventEmitter();
	@Output() toggleNotificationDropMenuEvent = new EventEmitter();

    public themeClass: string = "theme-cyan";
	public darkClass: string = "";
	public nama_role : string = localStorage.getItem('nama_role');
	public id_role : string = localStorage.getItem('id_role')
	private ngUnsubscribe = new Subject();
	public widthSize : number = 250;

	constructor(private themeService: ThemeService, private authservice : AuthService, private router : Router) {
        this.themeService.themeClassChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe(themeClass => {
			this.themeClass = themeClass;
        });
        this.themeService.darkClassChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe(darkClass => {
            this.darkClass = darkClass;
		});
		// console.log(this.nama_role)
		// console.log(this.id_role)
    }
    
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

	changeNavTab(tab: string) {
		this.navTab = tab;
	}

	activeInactiveMenu(menuItem: string) {
		this.activeInactiveMenuEvent.emit({ 'item': menuItem });
	}

	changeTheme(theme:string){
		this.themeService.themeChange(theme);
    }
    
    changeDarkMode(darkClass: string) {
        this.themeService.changeDarkMode(darkClass);
	}
	
	keluar() {
		this.authservice.logoutUser()
	}

	toggleClick() {
        this.sidebar.toggle();
    }
    closeClick() {
        this.themeService.showHideMenu();
    }
}
