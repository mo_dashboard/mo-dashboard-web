import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThemeService } from '../../services/theme.service';
import { AuthService } from '../../guards/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
	providers: [NgbDropdownConfig]
})

export class HeaderComponent implements OnInit {

	// Properties
	public timerInterval : any;
	public nama_role : string = localStorage.getItem('nama_role');

	@Input() showNotifMenu: boolean = false;
	@Input() showToggleMenu: boolean = false;
	@Input() darkClass: string = "";
	@Output() toggleSettingDropMenuEvent = new EventEmitter();
	@Output() toggleNotificationDropMenuEvent = new EventEmitter();

	constructor(private config: NgbDropdownConfig, private themeService: ThemeService, private authservice: AuthService) {
		config.placement = 'bottom-right';
	}

	ngOnInit() {
	}

	toggleSettingDropMenu() {
		this.toggleSettingDropMenuEvent.emit();
	}

	toggleNotificationDropMenu() {
		this.toggleNotificationDropMenuEvent.emit();
	}

	toggleSideMenu() {
		this.themeService.showHideMenu();
	}

	keluar() {
		this.authservice.logoutUser()
	}

	opensweetalertcst(){
		// console.log('tertekan')
		Swal.fire({
			width: '25rem',
			text: "Apakah anda ingin keluar?",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, keluar',
			cancelButtonText: 'Batal'
		  }).then((result) => {
			if (result.value) {
				Swal.fire({
					text: 'Anda Berhasil Keluar',
					timer: 1000,
					timerProgressBar: true,
					onBeforeOpen: () => {
						Swal.showLoading()
						this.timerInterval = setInterval(() => {
						const content = Swal.getContent()
						// if (content) {
						// 	const b = content.querySelector('b')
						// 	if (b) {
						// 	b.textContent = Swal.getTimerLeft()
						// 	}
						// }
						}, 100)
					},
					onClose: () => {
						clearInterval(this.timerInterval)
					}
				})
				this.keluar()
			}
		  })
	  }

	@HostListener('window:scroll', ['$event'])
	onWindowScroll(e) {
		// console.log('Bos, On Scroll Iniii')
		let element = document.querySelector('.navbar');
		// if (window.pageYOffset > element.clientHeight) {
		if (window.pageYOffset > 300) {
			element.classList.add('navbar-inverse');
		} else {
			element.classList.remove('navbar-inverse');
		}
	}

}
