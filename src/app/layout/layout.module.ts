import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SidebarComponents } from './sidebar/sidebar.component';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { SidebarModule } from '@syncfusion/ej2-angular-navigations';

import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
	imports: [ LazyLoadImageModule,
		CommonModule,
		NgbModule,
		RouterModule,
		SidebarModule
	],
	declarations: [HeaderComponent, SidebarComponents, PageLoaderComponent],
	exports: [HeaderComponent, SidebarComponents, PageLoaderComponent]
})
export class LayoutModule { }
