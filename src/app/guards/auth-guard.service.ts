import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from './auth.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  routeURL: string;
  role: string;

  constructor(private authservice: AuthService, private router: Router) {
    this.routeURL = this.router.url;
   }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // console.log(this.authservice.isUserLoggedIn())
    // if (this.authservice.isUserLoggedIn() === true) {
    //   return true;
    // } else {
    //   this.router.navigate(['/authentication/page-login']);
    //   return false;
    // }
    this.role = localStorage.getItem('nama_role')
    // console.log('next', next)
    // console.log('state', state.url.replace('/admin/dashboard/index','/admin/list/list-undangan-user'))
    // console.log('state change', state)

    let expectedRole = ''
    expectedRole = this.role;
    // console.log('expected role', expectedRole)
  
    if (!this.authservice.isUserLoggedIn()){
      // if(this.role === "User Tipe 2") {
      //   this.router.navigate(['/admin/list/list-undangan-user'])
      // }
      this.routeURL = '/authentication/page-login';
      this.router.navigate(['/authentication/page-login'], {
        // note: this queryParams returns the current URL
        // that we can have in 'return' parameter,
        // so when the '/login' page opens,
        // this param tell us from where it comes
        queryParams: {
          return: state.url
        }
      });
      return false;
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'User Tipe 4' ||
      this.role === 'User Tipe 5' 
      ) {
      // console.log('masuk tipe 4 dan 5')
      if(
        // MENU DASHBOARD
        state.url === '/admin/dashboard/index' ||
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/forms/forms-tambah-undangan' ||
        state.url === '/admin/list/list-undangan' ||
        state.url === '/admin/list/list-tambah-responden-undangan' ||
        state.url === '/admin/list/list-denah' ||
        // MENU MATERI
        state.url === '/admin/list/list-materi' ||
        state.url === '/admin/list/list-kirim-materi' ||
        state.url === '/admin/list/list-unggah-materi' ||
        state.url === '/admin/list/list-daftar-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah' ||
        state.url === '/admin/list/list-tambah-responden-risalah' ||
        state.url === '/admin/list/list-ubah-risalah' ||
        state.url === '/admin/list/list-risalah-user' ||
        state.url === '/admin/list/list-laporan-risalah' ||
        state.url === '/admin/list/list-unggah-kirim-risalah'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/list/list-undangan-user'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'User Tipe 2' ||
      this.role === 'User Tipe 3' 
      ) {
      // console.log('masuk tipe 2 dan 3')
      if(
        // MENU DASHBOARD
        state.url === '/admin/dashboard/index' ||
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/forms/forms-tambah-undangan' ||
        state.url === '/admin/list/list-undangan' ||
        state.url === '/admin/list/list-tambah-responden-undangan' ||
        state.url === '/admin/list/list-denah' ||
        // MENU MATERI
        state.url === '/admin/list/list-materi' ||
        state.url === '/admin/list/list-kirim-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah' ||
        state.url === '/admin/list/list-tambah-responden-risalah' ||
        state.url === '/admin/list/list-ubah-risalah' ||
        state.url === '/admin/list/list-laporan-risalah' ||
        state.url === '/admin/list/list-unggah-kirim-risalah'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/list/list-undangan-user'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'User Tipe 1'
      ) {
      // console.log('masuk tipe 1')
      if(
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/forms/forms-tambah-undangan' ||
        state.url === '/admin/list/list-undangan' ||
        state.url === '/admin/list/list-tambah-responden-undangan' ||
        state.url === '/admin/list/list-denah' ||
        // MENU MATERI
        state.url === '/admin/list/list-materi' ||
        state.url === '/admin/list/list-kirim-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah' ||
        state.url === '/admin/list/list-tambah-responden-risalah' ||
        state.url === '/admin/list/list-ubah-risalah' ||
        state.url === '/admin/list/list-unggah-kirim-risalah'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/dashboard/index'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'Admin Sidang'
      ) {
      // console.log('masuk admin sidang')
      if(
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/list/list-undangan-user' ||
        // MENU MATERI
        state.url === '/admin/list/list-daftar-materi' ||
        state.url === '/admin/list/list-materi' ||
        state.url === '/admin/list/list-unggah-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah-user' ||
        state.url === '/admin/list/list-risalah' ||
        state.url === '/admin/list/list-tambah-responden-risalah' ||
        state.url === '/admin/list/list-ubah-risalah' ||
        state.url === '/admin/list/list-unggah-kirim-risalah' ||
        state.url === '/admin/list/list-laporan-risalah'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/dashboard/index'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'Admin Materi'
      ) {
      // console.log('masuk admin materi')
      if(
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/forms/forms-tambah-undangan' ||
        state.url === '/admin/list/list-undangan' ||
        state.url === '/admin/list/list-tambah-responden-undangan' ||
        state.url === '/admin/list/list-denah' ||
        state.url === '/admin/list/list-undangan-user' ||
        // MENU MATERI
        state.url === '/admin/list/list-daftar-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah-user' ||
        state.url === '/admin/list/list-risalah' ||
        state.url === '/admin/list/list-tambah-responden-risalah' ||
        state.url === '/admin/list/list-ubah-risalah' ||
        state.url === '/admin/list/list-unggah-kirim-risalah' ||
        state.url === '/admin/list/list-laporan-risalah'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/dashboard/index'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'Admin Risalah'
      ) {
      // console.log('masuk admin risalah')
      if(
        // MENU PENGGUNA
        state.url === '/admin/list/list-pengguna' ||
        state.url === '/admin/list/list-ubah-data-pengguna' ||
        state.url === '/admin/forms/forms-tambah-pengguna' ||
        // MENU UNDANGAN
        state.url === '/admin/forms/forms-tambah-undangan' ||
        state.url === '/admin/list/list-undangan' ||
        state.url === '/admin/list/list-tambah-responden-undangan' ||
        state.url === '/admin/list/list-denah' ||
        state.url === '/admin/list/list-undangan-user' ||
        // MENU MATERI
        state.url === '/admin/list/list-daftar-materi' ||
        state.url === '/admin/list/list-materi' ||
        state.url === '/admin/list/list-unggah-materi' ||
        state.url === '/admin/list/list-kirim-materi' ||
        // MENU SURVEY
        state.url === '/admin/list/list-survey' ||
        state.url === '/admin/list/list-kirim-survey' ||
        // MASTER DATA
        state.url === '/admin/list/list-master-data' ||
        state.url === '/admin/list/list-lokasi' ||
        state.url === '/admin/list/list-group' ||
        state.url === '/admin/list/list-jenis-sidang' ||
        state.url === '/admin/list/list-jabatan' ||
        // MENU RISALAH
        state.url === '/admin/list/list-risalah-user'
        ){
        // console.log('cek benar')
        this.router.navigate(['/admin/dashboard/index'])
      }
      return true
    } 
    else if (
      this.authservice.isUserLoggedIn() && 
      this.role === 'Super Admin'
    ){
      if(
        // state.url === '/admin/pages/page-profile' ||
        state.url === '/admin/list/list-undangan-user' || 
        state.url === '/admin/list/list-risalah-user' ||
        state.url === '/admin/list/list-daftar-materi'
        ){
          this.router.navigate(['/admin/dashboard/index'])
        }
      // console.log('loggedin')
      // re-assign current route URL to 'routeURL'
      // when the user is logged in
      // this.routeURL = this.router.url+'admin/list/list-undangan-user';
      // this.router.navigate(['/admin/list/list-undangan-user'])
      // just return true - if user is logged i
      return true;
    } 
    
  }
}
