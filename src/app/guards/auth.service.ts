import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

    private isloggedIn: boolean;
    private userName: string;

    constructor(private router : Router) {
        this.isloggedIn = JSON.parse(localStorage.getItem('isloggedIn'));
        // console.log(this.isloggedIn)
    }

    changeIsLoggedIn(nilai : boolean) {
        // console.log("changeIsLoggedIn() di eksekusi")
        localStorage.setItem('isloggedIn', nilai.toString());
        this.isloggedIn = JSON.parse(localStorage.getItem('isloggedIn'));
        // console.log(this.isloggedIn)
    }

    isUserLoggedIn(): boolean {
        const role = localStorage.getItem('nama_role')
        if (this.isloggedIn === true) {
            // console.log('role', role)
            // if(role === "User Tipe 2") {
            //     console.log('masuk tipe 2')
            //     this.router.navigate(['/admin/list/list-undangan-user'])
            //     return false
            // }
            return true
        } else {
            return false
        }
    }

    isAdminUser(): boolean {
        if (this.userName == 'Admin') {
            return true;
        }
        return false;
    }

    logoutUser(): void {
        // console.log("logoutUser() dieksekusi")
        this.changeIsLoggedIn(false)
        this.router.navigate(['/authentication/page-login']);
        localStorage.clear();
    }

} 