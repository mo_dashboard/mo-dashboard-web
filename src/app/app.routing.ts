﻿import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuardService } from './guards/auth-guard.service';
import { MeetingComponent } from './vcon/meeting/meeting.component';
import { PageNotFoundComponent } from './authentication/page-not-found/page-not-found.component';

export const routes: Routes = [
    { path: '', redirectTo: 'admin', pathMatch: 'full' },
    {
        path:
            'admin',
        //canActivate: [AuthGuardService],
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
    },
    { path: 'authentication', loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule) },
    {
        path: 'vcon',
        //canActivate: [AuthGuardService], // disable this for testing only
        children: [
            { path: '', redirectTo: 'meeting', pathMatch: 'full' },
            {
                path: 'meeting', component: MeetingComponent, data: {

                    title: 'e-kabinet'
                }
            },
        ]
    },
    { path: 'survey', loadChildren: () => import('./jawaban-survey/jawaban-survey.module').then(m => m.JawabanSurveyModule) },
    { path: 'upload-materi', loadChildren: () => import('./upload-materi/upload-materi.module').then(m => m.UploadMateriModule) },
    { path: '**', component: PageNotFoundComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });