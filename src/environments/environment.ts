// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  tel: "Kv+De1Us1FP8p1UNad4deY8nlOWMXdndMOXlybr1jacloLMO142H7uILPh7e0RtODgp+DD6IwfY4h+7CzbsdDtPw/wL9jzN7C16I4RdaNZE=",
  api_base_url : "localhost:3000/api/"
};
